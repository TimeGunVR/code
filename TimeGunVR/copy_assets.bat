@echo off
REM Copy Assets
mkdir "%~dp0\exe\Debug\Assets\"
mkdir "%~dp0\exe\Release\Assets\"
pushd "%~dp0\..\..\assets\"
for /r %%d in (*.gltf, *.bin, *.png, *.dds) do (
  xcopy /y "%%d" "%~dp0\exe\Debug\Assets\"
  xcopy /y "%%d" "%~dp0\exe\Release\Assets\"
  echo.
)
popd