#include "AssetManager.h"


#include "objects/base/BaseObject.h"
#include "objects/base/GrabObject.h"
#include "objects/base/InteractableObject.h"
#include "objects/base/TimeObject.h"

#include "objects/special/Lamp.h"
#include "objects/special/Safe.h"
#include "objects/special/FloppyDisk.h"
#include "objects/special/Door.h"
#include "objects/special/Teleporter.h"
#include "objects/special/Keypad.h"
#include "objects/special/DigitScreen.h"
#include "objects/special/Computer.h"
#include "objects/special/ComputerScreen.h"
#include "objects/special/Seed.h"
#include "objects/special/Flower.h"
#include "objects/special/Platform.h"

#include <Magnum/Trade/TextureData.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/Trade/SceneData.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Trade/ObjectData3D.h>

#include <Magnum/GL/TextureFormat.h>
#include <MagnumPlugins/DdsImporter/DdsImporter.h>
#include <MagnumPlugins/TinyGltfImporter/TinyGltfImporter.h>

#include "Helper.h"

#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NO_STB_IMAGE
#define TINYGLTF_NO_STB_IMAGE_WRITE

//#define CAVE_BUILD

#ifdef CAVE_BUILD
#include "tiny_gltf/tiny_gltf.h"
#else
#include <MagnumExternal/TinyGltf/tiny_gltf.h>
#endif

tinygltf::Model model;

void* AssetManager::loadScene(Scene3D &scene, Magnum::SceneGraph::DrawableGroup3D &drawables, Magnum::SceneGraph::AnimableGroup3D &animables, int id)
{
	Magnum::PluginManager::Manager<Magnum::Trade::AbstractImporter> manager;

	// Initialize TinyGltfImporter
	std::unique_ptr<Magnum::Trade::AbstractImporter> importer
		= manager.loadAndInstantiate("TinyGltfImporter");
	CORRADE_ASSERT_OUTPUT(importer, "[TinyGltfImporter] Loading/initializing failed", nullptr);

	// Import File
    CORRADE_ASSERT_OUTPUT(importer->openFile(_assetPath + Constant::SCENE_FILE), "[TinyGltfImporter] Opening file failed", nullptr);

	if (id == -1)
		id = importer->defaultScene();

	CORRADE_ASSERT_OUTPUT(id < importer->sceneCount() && id >= 0, "[AssetManager] loadScene: id out of range", nullptr);
	currentScene = id;

	if(!texturesLoaded)
		loadTextures(*importer);

	// Load JSON representation of scene for finding PBR textures correctly
#ifdef CAVE_BUILD
	tinygltf::TinyGLTF loader;
	std::string err;
	std::string warn;
	loader.LoadASCIIFromFile(&model, &err, &warn, _assetPath + Constant::SCENE_FILE);
	//loader.LoadBinaryFromFile(&model, &err, &warn, argv[1]); // for binary glTF(.glb) 
#else
	model = *((tinygltf::Model*)importer->importerState());
#endif

	std::vector<Magnum::UnsignedInt> children3D = importer->scene(id)->children3D();
	for (Magnum::UnsignedInt objectID : children3D)
		loadObject(*importer, scene, drawables, animables, objectID);

	BaseObject::onSceneLoaded();

	return nullptr;
}

void AssetManager::loadObject(Magnum::Trade::AbstractImporter &importer, Object3D &parent, 
	Magnum::SceneGraph::DrawableGroup3D &drawables, Magnum::SceneGraph::AnimableGroup3D &animables, Magnum::UnsignedInt objectID)
{
	MaterialPBR matPBR = extractMaterial(importer, objectID);
	std::string name = importer.object3DName(objectID);
	std::string shortname = Helper::getModelShortname(name);
	std::string partialShortName = Helper::getModelPartialShortname(shortname);

	// Teleporter
	if (shortname == Constant::SN_TELEPORTER)
		new Teleporter(shortname, parent, drawables, importer, objectID, matPBR);

	// Safe
	else if (shortname == Constant::SN_SAFE)
		new Safe(shortname, parent, drawables, importer, objectID, matPBR);
	
	// Lamp
	else if (shortname == Constant::SN_LAMP)
	{
		if(currentScene == 0)
			new Lamp(shortname, parent, drawables, animables, importer, objectID, matPBR);
		else
			new BaseObject(name, parent, drawables, importer, objectID, matPBR, 0);
	}

	// FloppyDisk
	else if (shortname == Constant::SN_FLOPPY)
		new FloppyDisk(shortname, parent, drawables, importer, objectID, matPBR);

	// Door
	else if (shortname == Constant::SN_DOOR) 
		new Door(shortname, parent, drawables, importer, objectID, matPBR);

	// Keypad
	else if (shortname == Constant::SN_KEYPAD)
		new Keypad(shortname, parent, drawables, importer, objectID, matPBR);

	// Computer
	else if (shortname == Constant::SN_COMPUTER)
		new Computer(shortname, parent, drawables, importer, objectID, matPBR);

	// Computer Screen
	else if (shortname == Constant::SN_COMPUTER_SCREEN)
		new ComputerScreen(shortname, parent, drawables, animables, importer, objectID, matPBR);

	// Magic Bean
	else if (shortname == Constant::SN_MAGICBEAN)
		new Seed(shortname, parent, drawables, importer, objectID, matPBR, true);

	// Seed
	else if (partialShortName == Constant::PS_SEED)
		new Seed(shortname, parent, drawables, importer, objectID, matPBR, false);

	// Keypad Screen Parts
	else if (partialShortName == Constant::PS_DIGITSCREEN)
		new DigitScreen(shortname, parent, drawables, importer, objectID, matPBR);

	// FLower
	else if (partialShortName == Constant::PS_FLOWER) {
		matPBR.setAlphaCutoff(true);
		new Flower(shortname, parent, drawables, importer, objectID, matPBR);
	}

	// Platform
	else if (shortname == Constant::SN_PLATFORM) {
		new Platform(shortname, parent, drawables, importer, objectID, matPBR);
	}

	// BaseObject without special purpose
	else
		new BaseObject(shortname, parent, drawables, importer, objectID, matPBR, 0);
}

void AssetManager::loadTextures(Magnum::Trade::AbstractImporter &importer)
{
	textures = Magnum::Containers::Array<Magnum::GL::Texture2D>(importer.image2DCount());
	for (Magnum::UnsignedInt i = 0; i < importer.image2DCount(); i++)
	{
		Magnum::Containers::Optional<Magnum::Trade::ImageData2D> imgData = importer.image2D(i);
		Magnum::Containers::Optional<Magnum::Trade::TextureData> texData = importer.texture(i);

		textures[i] = Magnum::GL::Texture2D();
		textures[i]
			.setWrapping(texData->wrapping().xy())
			.setMagnificationFilter(texData->magnificationFilter())
			.setMinificationFilter(texData->minificationFilter())
			.setMaxAnisotropy(Magnum::GL::Sampler::maxMaxAnisotropy())
			.setStorage(Magnum::Math::log2(imgData->size().x()) + 1, Magnum::TextureFormat::RGBA8, imgData->size())
			.setSubImage(0, {}, *imgData)
			.generateMipmap();
	}

	texturesLoaded = true;
}

MaterialPBR AssetManager::extractMaterial(Magnum::Trade::AbstractImporter &importer, Magnum::UnsignedInt objectID)
{
	int meshID = model.nodes.at(objectID).mesh;
	int materialID = model.meshes.at(meshID).primitives.at(0).material;

	int texID_base = model.materials.at(materialID).values.at("baseColorTexture").TextureIndex();
	int texID_occRoughMetal= model.materials.at(materialID).values.at("metallicRoughnessTexture").TextureIndex();
	int texID_normal = model.materials.at(materialID).additionalValues.at("normalTexture").TextureIndex();

	int imgID_base = importer.texture(texID_base)->image();
	int imgID_occRoughMetal = importer.texture(texID_occRoughMetal)->image();
	int imgID_normal = importer.texture(texID_normal)->image();

	Magnum::Texture2D* baseColorTexture = &textures[imgID_base];
	Magnum::Texture2D* occRoughMetalTexture = &textures[imgID_occRoughMetal];
	Magnum::Texture2D* normalTexture = &textures[imgID_normal];
	Magnum::Texture2D* emissiveTexture = nullptr;

	if (model.materials.at(materialID).additionalValues.count("emissiveTexture"))
	{
		int texID_emission = model.materials.at(materialID).additionalValues.at("emissiveTexture").TextureIndex();
		int imgID_emission = importer.texture(texID_emission)->image();
		emissiveTexture = &textures[imgID_emission];
	}

	return MaterialPBR(baseColorTexture, normalTexture, occRoughMetalTexture, emissiveTexture);
}

Magnum::GL::Texture2D AssetManager::loadTextureDDS(std::string name)
{
	Magnum::PluginManager::Manager<Magnum::Trade::AbstractImporter> manager;
	std::unique_ptr<Magnum::Trade::AbstractImporter> importer
		= manager.loadAndInstantiate("DdsImporter");

	Magnum::GL::Texture2D texture;
	
	CORRADE_ASSERT_OUTPUT(importer, "[DdsImporter] Loading/initializing failed", texture);
	CORRADE_ASSERT_OUTPUT(importer->openFile(_assetPath + name + ".dds"),
		"[DdsImporter] Opening file failed (" + name + ".dds)", texture);

	texture
		.setWrapping(Magnum::SamplerWrapping::Repeat)
		.setMagnificationFilter(Magnum::SamplerFilter::Linear)
		.setMinificationFilter(Magnum::SamplerFilter::Linear)
		.setMaxAnisotropy(Magnum::GL::Sampler::maxMaxAnisotropy())
		.setStorage(0, Magnum::GL::TextureFormat::RGBA8, importer->image2D(0)->size())
		.setCompressedImage(0, *importer->image2D(0));

	return texture;
}

Magnum::Containers::Array<Magnum::GL::Texture2D> AssetManager::textures;
bool AssetManager::texturesLoaded = false;
std::string AssetManager::_assetPath;

int AssetManager::currentScene = 0;