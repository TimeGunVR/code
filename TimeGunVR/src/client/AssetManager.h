#pragma once
#include "stdafx.h"

#include "drawing/MaterialPBR.h"

#include <iostream>

#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/Trade/AbstractImporter.h>

// AssetManager
//
// class for loading scenes & storing textures from .gltf files
//
class AssetManager
{
public:
	// @scene: structure to store loaded scene
	// @drawables: drawable group for loaded objects
	// @sceneID: id of the scene in the scenes file (-1 for default)
	static void* loadScene(Scene3D &scene, Magnum::SceneGraph::DrawableGroup3D &drawables, Magnum::SceneGraph::AnimableGroup3D &animables, int id);

	static Magnum::GL::Texture2D loadTextureDDS(std::string name);

	static std::string _assetPath;
private:
	// recursively loads object hierachy
	static void loadObject(Magnum::Trade::AbstractImporter &importer, Object3D &parent,
		Magnum::SceneGraph::DrawableGroup3D &drawables, Magnum::SceneGraph::AnimableGroup3D &animables, Magnum::UnsignedInt objectID);

	// loads all textures and saves to class member std::map @textures
	static void loadTextures(Magnum::Trade::AbstractImporter &importer);

	static MaterialPBR extractMaterial(Magnum::Trade::AbstractImporter &importer, Magnum::UnsignedInt objectID);

	// <name, texture>
	static Magnum::Containers::Array<Magnum::GL::Texture2D> textures;
	static bool texturesLoaded;

	static int currentScene;
};