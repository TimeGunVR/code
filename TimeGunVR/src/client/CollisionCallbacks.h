#pragma once
#include "stdafx.h"
#include "Input.h"
#include "Helper.h"
#include "objects/base/BaseObject.h"
#include "objects/base/GrabObject.h"
#include "objects/special/FloppyDisk.h"
#include "objects/special/Seed.h"

//#define CAVE_BUILD

namespace CollisionCallbacks 
{
	struct ContactSensorCallback : public btCollisionWorld::ContactResultCallback {
		// Constructor, pass whatever context you want to have available when processing contacts
		/*! You may also want to set m_collisionFilterGroup and m_collisionFilterMask
		 *  (supplied by the superclass) for needsCollision() */
		ContactSensorCallback() : btCollisionWorld::ContactResultCallback(), body(nullptr), input(nullptr) { }

		ContactSensorCallback(btRigidBody& tgtBody, Input& inp)
			: btCollisionWorld::ContactResultCallback(), body(&tgtBody), input(&inp) { }

		btRigidBody* body; //!< The body the sensor is monitoring
		Input* input;

		//! If you don't want to consider collisions where the bodies are joined by a constraint, override needsCollision:
		virtual bool needsCollision(btBroadphaseProxy* proxy) const {
			// superclass will check m_collisionFilterGroup and m_collisionFilterMask
			if (!btCollisionWorld::ContactResultCallback::needsCollision(proxy))
				return false;
			// if passed filters, may also want to avoid contacts between constraints
			return body->checkCollideWithOverride(static_cast<btCollisionObject*>(proxy->m_clientObject));
		}

		//! Called with each contact for your own processing (e.g. test if contacts fall in within sensor parameters)
		virtual btScalar addSingleResult(btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0, int partId0, int index0,
			const btCollisionObjectWrapper* colObj1, int partId1, int index1) {

			const btCollisionObject* cObj;
			btVector3 ptBody, ptOther; // will be set to point of collision relative to body
			if (colObj0->m_collisionObject == body) {
				ptBody = cp.m_localPointA;
				ptOther = cp.m_localPointB;
				cObj = colObj1->getCollisionObject();
			}
			else {
				assert(colObj1->m_collisionObject == body && "body does not match either collision object");
				ptBody = cp.m_localPointB;
				ptOther = cp.m_localPointA;
				cObj = colObj0->getCollisionObject();
			}

			if (cObj->getInternalType() == 4) {
				return 0;
			}

			bool grabObject;
#ifdef CAVE_BUILD
			grabObject = input->getKeyDown(KeyCode::WandTrigger);
#else
			grabObject = input->getKeyDown(KeyCode::Space);
#endif // !CAVE_BUILD

			if (grabObject)
			{
				BaseObject* bObj = ((BaseObject*)cObj->getUserPointer());
				if (bObj->isGrabObject())
					dynamic_cast<GrabObject*>(bObj)->grab(body, const_cast<btRigidBody*>(btRigidBody::upcast(cObj)), btTransform(btQuaternion::getIdentity(), ptBody), btTransform(btQuaternion::getIdentity(), ptOther), true);
			}

			return 0; // There was a planned purpose for the return value of addSingleResult, but it is not used so you can ignore it.
		}
	};

	struct FloppySensorCallback : public btCollisionWorld::ContactResultCallback {
		FloppySensorCallback(btRigidBody& tgtBody)
			: btCollisionWorld::ContactResultCallback(), body(&tgtBody) { }

		btRigidBody* body; 

		virtual bool needsCollision(btBroadphaseProxy* proxy) const {
			if (!btCollisionWorld::ContactResultCallback::needsCollision(proxy))
				return false;
			return body->checkCollideWithOverride(static_cast<btCollisionObject*>(proxy->m_clientObject));
		}

		virtual btScalar addSingleResult(btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0, int partId0, int index0,
			const btCollisionObjectWrapper* colObj1, int partId1, int index1) {

			const btCollisionObject* cObj;
			if (colObj0->m_collisionObject == body) {
				cObj = colObj1->getCollisionObject();
			}
			else {
				assert(colObj1->m_collisionObject == body && "body does not match either collision object");
				cObj = colObj0->getCollisionObject();
			}

			if (cObj->getInternalType() == 4 || cObj->getUserPointer() == nullptr) {
				return 0;
			}

			if (!((FloppyDisk*)body->getUserPointer())->getCanBeGrabed())
				return 0;

			BaseObject* bObj = ((BaseObject*)cObj->getUserPointer());
			if (bObj->getName() == "Computer" && cObj->getUserIndex() == 1)
				((FloppyDisk*)body->getUserPointer())->fixFloppy(cObj);

			return 0; 
		}
	};

	struct PlatformSensorCallback : public btCollisionWorld::ContactResultCallback {
		PlatformSensorCallback(btRigidBody& tgtBody)
			: btCollisionWorld::ContactResultCallback(), body(&tgtBody) { }

		btRigidBody* body; 

		virtual bool needsCollision(btBroadphaseProxy* proxy) const {
			if (!btCollisionWorld::ContactResultCallback::needsCollision(proxy))
				return false;
			return body->checkCollideWithOverride(static_cast<btCollisionObject*>(proxy->m_clientObject));
		}

		virtual btScalar addSingleResult(btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0, int partId0, int index0,
			const btCollisionObjectWrapper* colObj1, int partId1, int index1) {

			const btCollisionObject* cObj;
			btVector3 ptBody, ptOther; 
			if (colObj0->m_collisionObject == body) {
				ptBody = cp.m_localPointA;
				ptOther = cp.m_localPointB;
				cObj = colObj1->getCollisionObject();
			}
			else {
				assert(colObj1->m_collisionObject == body && "body does not match either collision object");
				ptBody = cp.m_localPointB;
				ptOther = cp.m_localPointA;
				cObj = colObj0->getCollisionObject();
			}

			if (cObj->getInternalType() == 4 || cObj->getUserPointer() == nullptr) {
				return 0;
			}

			BaseObject* bObj = ((BaseObject*)cObj->getUserPointer());
			std::string partialName = Helper::getModelPartialShortname(bObj->getName());

			if (partialName == "Seed") {
				((Seed*)bObj)->implantSeed(body, ptBody);
			}
			else if (bObj->getName() == "MagicBean") {
				((Seed*)bObj)->implantSeed(body, ptBody);
			}

			return 0;
		}
	};
}
