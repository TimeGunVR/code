#pragma once

#include <iostream>

namespace Constant
{
	const std::string SCENE_FILE = "Scene.gltf";

#pragma region Environment 
	const std::string TEX_ENV_DIFFUSE = "Diffuse";
	const std::string TEX_ENV_SPECULAR = "Specular";
	const std::string TEX_ENV_BRDFLUT = "Brdf";
#pragma endregion

#pragma region Vectors, Floats, ets.
    const Magnum::Vector3 LIGHT_DIRECTION{ 0, 245, 0 };
	const Magnum::Vector3 LIGHT_COLOR{ 1.0f, 1.0f, 1.0f };

	const Magnum::Color4 COL_CLEAR{ 0.6f, 0.6f, 0.6f, 1.0f };
#pragma endregion

#pragma region Object Names
	// SN := ShortName
	// N : =Name
	// PS := PartialShortname (for objects with underscore + number))

	const std::string SN_TELEPORTER = "Teleporter";

	const std::string SN_LAMP = "Lamp";
	const std::string SN_KEYPAD = "Keypad";
	const std::string PS_KEYPAD_BTN = "btn";
	const std::string PS_DIGITSCREEN = "Screen";

	const std::string SN_SAFE = "Safe";
	const std::string SN_FLOPPY = "FloppyDisk";
	const std::string SN_COMPUTER = "Computer";
	const std::string SN_COMPUTER_SCREEN = "MonitorScreen";

	const std::string SN_BED = "Bed";
	const std::string SN_DOOR = "Door";
	const std::string SN_PLATFORM = "Platform";
	const std::string SN_FLOORTHICK = "FloorThick";
	const std::string PS_SEED = "Seed";
	const std::string PS_FLOWER = "Flower";
	const std::string SN_MAGICBEAN = "MagicBean";
#pragma endregion

#pragma region Gameplay Settings
	const int ROOM_1_KEYPAD_CODE = 8364;
#pragma endregion
}
