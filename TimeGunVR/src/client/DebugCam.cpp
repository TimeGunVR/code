#include "DebugCam.h"

#include <boost/algorithm/clamp.hpp>

DebugCam::DebugCam(Object3D &cameraObject)
{
	cam = &cameraObject;
}

void DebugCam::update(Input input, float deltaTime)
{
	float speed = speedNorm;
	if (input.getKeyDown(KeyCode::L_Shift))
		speed = speedFast;
	else if (input.getKeyDown(KeyCode::L_Ctrl))
		speed = speedSlow;

	if (input.getKeyDown(KeyCode::W)) {
		cam->translateLocal(Magnum::Vector3::zAxis(-1.) * speed);
	}
	if (input.getKeyDown(KeyCode::A)) {
		cam->translateLocal(Magnum::Vector3::xAxis(-1.) * speed);
	}
	if (input.getKeyDown(KeyCode::S)) {
		cam->translateLocal(Magnum::Vector3::zAxis(1.) * speed);
	}
	if (input.getKeyDown(KeyCode::D)) {
		cam->translateLocal(Magnum::Vector3::xAxis(1.) * speed);
	}
	if (input.getKeyDown(KeyCode::E)) {
		cam->translate(Magnum::Vector3::yAxis(1.) * speed);
	}
	if (input.getKeyDown(KeyCode::Q)) {
		cam->translate(Magnum::Vector3::yAxis(-1.) * speed);
	}

	rotX += -input.getMouseMotion().y() * sensitivity * deltaTime;
	rotY += -input.getMouseMotion().x() * sensitivity * deltaTime;

	rotX = boost::algorithm::clamp(rotX, -89.f, 89.f);


	Magnum::Vector3 pos = cam->transformationMatrix().translation();
	Magnum::Vector3 dir = Magnum::Vector3(cos(glm::radians(rotX)) * cos(glm::radians(-rotY)),
		sin(glm::radians(rotX)),
		cos(glm::radians(rotX)) * sin(glm::radians(-rotY)));

	Magnum::Matrix4 mat = Magnum::Matrix4::lookAt(pos, dir + pos, Magnum::Vector3(0, 1, 0));

	cam->setTransformation(mat);
}