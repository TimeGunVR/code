#pragma once
#include "stdafx.h"
#include "Input.h"

// DebugCam
//
// flycam that can be controlled using mouse 
// & keyboard keys W,A,S,D,Q,E
//
class DebugCam
{
public:
	DebugCam(Object3D &cameraObject);
	void update(Input input, float deltaTime);

private:
	Object3D* cam;
	float rotX = 0.f;
	float rotY = 0.f;

	float sensitivity = 45.f;

	float speedNorm = 10.f;
	float speedSlow = 1.f;
	float speedFast = 25.f;
};