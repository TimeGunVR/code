#pragma once

#include "stdafx.h"
#include <iostream>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

// Helper
//
// wraps various static helper functions
//
class Helper
{
public:
	static std::string getModelShortname(std::string name)
	{
		return getStringBeforeSign(name, ".");
	}

	static std::string getModelPartialShortname(std::string name)
	{
		return getStringBeforeSign(name, "_");
	}

	// From: https://stackoverflow.com/a/5289624
	static float randomFloat(float a, float b) {
		float random = ((float)rand()) / (float)RAND_MAX;
		float diff = b - a;
		float r = random * diff;
		return a + r;
	}

	static Magnum::Matrix4 glm2magnum(const glm::mat4& input)
	{
		return Magnum::Matrix4 (
			Magnum::Vector4(input[0][0], input[0][1], input[0][2], input[0][3]),
			Magnum::Vector4(input[1][0], input[1][1], input[1][2], input[1][3]),
			Magnum::Vector4(input[2][0], input[2][1], input[2][2], input[2][3]),
			Magnum::Vector4(input[3][0], input[3][1], input[3][2], input[3][3]));
	}

	static float DegToRad(float deg) {
		return (deg * Magnum::Constants::pi()) / 180.0f;
	}

	static float RadToDeg(float rad) {
		return (rad * 180.0f) / Magnum::Constants::pi();
	}
private:
	static std::string getStringBeforeSign(std::string string, std::string sign)
	{
		std::vector<std::string> split;
		boost::split(split, string, boost::is_any_of(sign));
		return split[0];
	}
};