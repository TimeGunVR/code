#pragma once
#include "stdafx.h"
#include <Magnum/Platform/GlfwApplication.h>

typedef Magnum::Platform::Application::KeyEvent KeyEvent;
typedef Magnum::Platform::Application::MouseMoveEvent MouseMoveEvent;
typedef Magnum::Platform::Application::MouseEvent MouseEvent;

enum KeyCode : short
{
	A,
	D,
	E,
	Q,
	S,
	W,
	L_Shift,
	L_Ctrl,
	Space,

	MouseLeft,
	MouseRight,

	WandLeft,
	WandMiddle,
	WandRight,
	WandTrigger,

	Num_1,
	Num_2,
	Num_3,
	Num_4,
	Num_5,
	Num_6,
	Num_7,
	Num_8,
	Num_9,

	ALWAYS_LAST_NO_KEYCODE
};

// Input
//
// stores current keyboard, mouse & wand button state
//
class Input
{
private:
	bool keyDown[KeyCode::ALWAYS_LAST_NO_KEYCODE];
	Magnum::Vector2 mouseMotion;
	Magnum::Vector2 mousePressPosition;
	bool mouseMoved;

public:
	bool getKeyDown(KeyCode code) { return keyDown[code]; }

	Magnum::Vector2 getMouseMotion() { return mouseMotion; }
	void resetMouseMotion() { mouseMotion = Magnum::Vector2{ 0.f, 0.f }; }

	bool getMouseMoved() { return mouseMoved; }

	void keyPressEvent(KeyEvent &event)
	{
		switch (event.key())
		{
		case KeyEvent::Key::W: keyDown[KeyCode::W] = true; break;
		case KeyEvent::Key::A: keyDown[KeyCode::A] = true; break;
		case KeyEvent::Key::S: keyDown[KeyCode::S] = true; break;
		case KeyEvent::Key::D: keyDown[KeyCode::D] = true; break;
		case KeyEvent::Key::Q: keyDown[KeyCode::Q] = true; break;
		case KeyEvent::Key::E: keyDown[KeyCode::E] = true; break;
		case KeyEvent::Key::LeftShift: keyDown[KeyCode::L_Shift] = true; break;
		case KeyEvent::Key::LeftCtrl:  keyDown[KeyCode::L_Ctrl] = true; break;
		case KeyEvent::Key::Space:  keyDown[KeyCode::Space] = true; break;
		case KeyEvent::Key::NumOne:		keyDown[KeyCode::Num_1] = true; break;
		case KeyEvent::Key::NumTwo:		keyDown[KeyCode::Num_2] = true; break;
		case KeyEvent::Key::NumThree:	keyDown[KeyCode::Num_3] = true; break;
		case KeyEvent::Key::NumFour:	keyDown[KeyCode::Num_4] = true; break;
		case KeyEvent::Key::NumFive:	keyDown[KeyCode::Num_5] = true; break;
		case KeyEvent::Key::NumSix:		keyDown[KeyCode::Num_6] = true; break;
		case KeyEvent::Key::NumSeven:	keyDown[KeyCode::Num_7] = true; break;
		case KeyEvent::Key::NumEight:	keyDown[KeyCode::Num_8] = true; break;
		case KeyEvent::Key::NumNine:	keyDown[KeyCode::Num_9] = true; break;
		default: break;
		}
	}

	void keyReleaseEvent(KeyEvent &event)
	{
		switch (event.key())
		{
		case KeyEvent::Key::W: keyDown[KeyCode::W] = false; break;
		case KeyEvent::Key::A: keyDown[KeyCode::A] = false; break;
		case KeyEvent::Key::S: keyDown[KeyCode::S] = false; break;
		case KeyEvent::Key::D: keyDown[KeyCode::D] = false; break;
		case KeyEvent::Key::Q: keyDown[KeyCode::Q] = false; break;
		case KeyEvent::Key::E: keyDown[KeyCode::E] = false; break;
		case KeyEvent::Key::LeftShift: keyDown[KeyCode::L_Shift] = false; break;
		case KeyEvent::Key::LeftCtrl:  keyDown[KeyCode::L_Ctrl] = false; break;
		case KeyEvent::Key::Space:  keyDown[KeyCode::Space] = false; break;
		case KeyEvent::Key::NumOne:		keyDown[KeyCode::Num_1] = false; break;
		case KeyEvent::Key::NumTwo:		keyDown[KeyCode::Num_2] = false; break;
		case KeyEvent::Key::NumThree:	keyDown[KeyCode::Num_3] = false; break;
		case KeyEvent::Key::NumFour:	keyDown[KeyCode::Num_4] = false; break;
		case KeyEvent::Key::NumFive:	keyDown[KeyCode::Num_5] = false; break;
		case KeyEvent::Key::NumSix:		keyDown[KeyCode::Num_6] = false; break;
		case KeyEvent::Key::NumSeven:	keyDown[KeyCode::Num_7] = false; break;
		case KeyEvent::Key::NumEight:	keyDown[KeyCode::Num_8] = false; break;
		case KeyEvent::Key::NumNine:	keyDown[KeyCode::Num_9] = false; break;
		default: break;
		}
	}

	void wandButtonEvent(KeyCode btn, bool isPressed)
	{
		keyDown[btn] = isPressed;
	}

	void mouseMoveEvent(MouseMoveEvent &event)
	{
		mouseMotion = Magnum::Vector2(event.position());
		if (keyDown[KeyCode::MouseLeft] || keyDown[KeyCode::MouseRight])
			mouseMoved = true;
		event.setAccepted();
	}

	void mousePressEvent(MouseEvent &event)
	{
		if (event.button() != MouseEvent::Button::Left && event.button() != MouseEvent::Button::Right) return;

		switch (event.button())
		{
		case MouseEvent::Button::Left: keyDown[KeyCode::MouseLeft] = true; break;
		case MouseEvent::Button::Right: keyDown[KeyCode::MouseRight] = true; break;
		}

		mousePressPosition = Magnum::Vector2(event.position());

		event.setAccepted();
	}

	void mouseReleaseEvent(MouseEvent &event)
	{
		if (event.button() != MouseEvent::Button::Left && event.button() != MouseEvent::Button::Right) return;

		switch (event.button())
		{
		case MouseEvent::Button::Left: keyDown[KeyCode::MouseLeft] = false; break;
		case MouseEvent::Button::Right: keyDown[KeyCode::MouseRight] = false; break;
		}

		mouseMoved = false;

		event.setAccepted();
	}
};