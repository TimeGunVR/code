#include "MyProject.h"

#include "Helper.h"
#include "AssetManager.h"

#include <Magnum/GL/Renderer.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/GL/DefaultFramebuffer.h>

#include <algorithm>
#include <boost/filesystem/path.hpp>

#include "objects/base/GrabObject.h"
#include "objects/base/InteractableObject.h"
#include "objects/base/TimeObject.h"
#include "objects/special/Teleporter.h"
#include "objects/special/Platform.h"
#include "objects/special/Safe.h"

//#define CAVE_BUILD
//#define DEBUG_DRAW

MyProject::MyProject(const Arguments& arguments) :
	Magnum::Platform::Application{ arguments, Configuration{}
		.setTitle("Time Gun VR")
		.setCursorMode(Configuration::CursorMode::Disabled)
#ifdef CAVE_BUILD
	.setSize(Magnum::Vector2i(1920,1200))
	.setFlags(GLConfiguration::Flag::Stereo)
	.setCursorMode(Configuration::CursorMode::Hidden)
#else
	.setWindowFlags(Configuration::WindowFlag::Resizable)
#endif
}
{
	if (arguments.argc < 3) {
		std::cout << "Not enough input arguments. Exiting now" << std::endl;
		this->exit();
		return;
	}

	boost::filesystem::path p(arguments.argv[0]);
	boost::filesystem::path dir = p.parent_path();
	AssetManager::_assetPath = dir.string() + "/Assets/";

	rNode = std::make_shared<synchlib::renderNode>(arguments.argv[2], arguments.argc, arguments.argv);

	timeSyncher = synchlib::udpSynchObject<long long>::create();
	wandButtonSyncher = synchlib::udpSynchObject<glm::bvec4 >::create();

	rNode->addSynchObject(timeSyncher, synchlib::renderNode::RECEIVER, 0);
	rNode->addSynchObject(wandButtonSyncher, synchlib::renderNode::RECEIVER, 0);
	rNode->init();
	rNode->startSynching();
	// synchlib stuff end

	/* Every scene needs a camera */
	_cameraObject.setParent(&_scene);
	(*(_camera = std::make_shared<Magnum::SceneGraph::Camera3D>(_cameraObject)))
#ifndef CAVE_BUILD
		.setAspectRatioPolicy(Magnum::SceneGraph::AspectRatioPolicy::Extend)
		.setProjectionMatrix(Magnum::Matrix4::perspectiveProjection(Magnum::Deg(60.f), 1.0f, 0.01f, 100000.0f))
#endif
		.setViewport(Magnum::GL::defaultFramebuffer.viewport().size());

	/* Setup renderer and shader defaults */
	Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::DepthTest);
	Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::FaceCulling);

	DrawablePBR::init("Panorama");

	Magnum::GL::Renderer::setClearColor(Constant::COL_CLEAR);

	/* Bullet setup */
	auto* broadphase = new btDbvtBroadphase;
	broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
	auto* collisionConfiguration = new btDefaultCollisionConfiguration;
	auto* dispatcher = new btCollisionDispatcher{ collisionConfiguration };
	auto* solver = new btSequentialImpulseConstraintSolver;
	_bWorld = new btDiscreteDynamicsWorld{ dispatcher, broadphase, solver, collisionConfiguration };
	_bWorld->setGravity({ 0.0f, -1000.0f, 0.0f });
	_bWorld->setForceUpdateAllAabbs(false);

	_debugDraw = Magnum::BulletIntegration::DebugDraw{};
	_debugDraw.setMode(Magnum::BulletIntegration::DebugDraw::Mode::DrawWireframe | Magnum::BulletIntegration::DebugDraw::Mode::DrawConstraints | Magnum::BulletIntegration::DebugDraw::Mode::DrawConstraintLimits);
	_bWorld->setDebugDrawer(&_debugDraw);

	btTransform t;
	t.setIdentity();
	t.setOrigin(btVector3(_cameraObject.transformationMatrix().translation()));
	btSphereShape* cameraShape = new btSphereShape(1);
	btSphereShape* cameraShape2 = new btSphereShape(8);

	_handGhostObject = new btGhostObject();
	_handGhostObject->setCollisionShape(cameraShape);
	_handGhostObject->setWorldTransform(t);
	_handGhostObject->setCollisionFlags(6);
	_bWorld->addCollisionObject(_handGhostObject);

	_handObject.setParent(&_scene);
	auto* motionState = new Magnum::BulletIntegration::MotionState{ _handObject };
	_handRigidBody = new btRigidBody{ btRigidBody::btRigidBodyConstructionInfo{
		10, &motionState->btMotionState(), cameraShape2, {0.0f,0.0f,0.0f}} };
	_handRigidBody->setCollisionFlags(6);
	_handRigidBody->setWorldTransform(t);
	_bWorld->addRigidBody(_handRigidBody);

	BaseObject::world = _bWorld;
	GrabObject::world = _bWorld;

	// set user pointer to nullptr to filter it out 
	// while checking ghost object's overlapping objects
	_handRigidBody->setUserPointer(nullptr);

	callback = CollisionCallbacks::ContactSensorCallback( *_handRigidBody, input );

	loadScene(currentScene);

	timeRay = std::unique_ptr<TimeRay>(new TimeRay(_scene, _drawables));
}

void MyProject::loadScene(int sceneID)
{
	for (int i = _bWorld->getNumConstraints() - 1; i >= 0; i--) {
		_bWorld->removeConstraint(_bWorld->getConstraint(i));
	}

	BaseObject::destroyAllObjects();
	GrabObject::clearAllObjects();
	TimeObject::clearAllObjects();
	InteractableObject::clearAllObjects();

	_drawables = Magnum::SceneGraph::DrawableGroup3D();
	_animables = Magnum::SceneGraph::AnimableGroup3D();

	AssetManager::loadScene(_scene, _drawables, _animables, sceneID);
}

void MyProject::drawEvent()
{
#ifdef CAVE_BUILD
	deltaTime = FIXED_DELTA;
	m_lastTime += FIXED_DELTA;
#else
	// Synch time & calculate last frame duration
	float curTime = timeSyncher->getData() / 1000.f;
	deltaTime = curTime - m_lastTime;
	m_lastTime = curTime;
#endif // CAVE_BUILD

	// update animables
	_animables.step(m_lastTime, deltaTime);
	// update animation of current time gun target
	TimeObject::stepAnimation(deltaTime);

	// step physics simulation
#ifdef CAVE_BUILD
	_bWorld->stepSimulation(1.0f / 60.f);
#else
	_bWorld->stepSimulation(deltaTime);
#endif

	updateHandAndHeadTrafo();
	processInput();

	draw();

	glFinish();
	if (!rNode->synchFrame()) {
		std::cout << "Could not sync frame" << std::endl;
		this->exit();
	}

	swapBuffers();
	redraw();

	// if teleporter is active and player stands on it, 
	// load next level after TELPORTATION_TIME has passed 
	Teleporter* teleporter = (Teleporter*)BaseObject::getObject(Constant::SN_TELEPORTER);
	if (teleporter->isActive()) {
		Magnum::Vector2 tele2D(teleporter->transformationMatrix().translation().x(), teleporter->transformationMatrix().translation().z());
		Magnum::Vector2 head2D(DrawablePBR::headPosition.x(), DrawablePBR::headPosition.z());

		if ((tele2D - head2D).length() <= 35.f)
		{
			telportationTime += deltaTime;
			if (telportationTime > TELPORTATION_TIME) {
				currentScene++;
				loadScene(currentScene);
			}
		}
		else
			telportationTime = 0;
	}
}

void MyProject::viewportEvent(const Magnum::Vector2i& size)
{
	Magnum::GL::defaultFramebuffer.setViewport({ {}, size });
	_camera->setViewport(size);
}

void MyProject::draw()
{
	glm::mat4 projMatL, projMatR, viewMat;
	rNode->getProjectionMatrices(projMatL, projMatR);
	rNode->getSceneTrafo(viewMat);
	Magnum::Matrix4 mMatL = Helper::glm2magnum(projMatL);
	Magnum::Matrix4 mMatR = Helper::glm2magnum(projMatR);
	Magnum::Matrix4 mview = Helper::glm2magnum(viewMat);

	//Default famebuffer
	Magnum::GL::defaultFramebuffer.mapForDraw(Magnum::GL::DefaultFramebuffer::DrawAttachment::BackLeft);
	Magnum::GL::defaultFramebuffer.clear(Magnum::GL::FramebufferClear::Color | Magnum::GL::FramebufferClear::Depth);

#ifdef CAVE_BUILD
	_camera->setProjectionMatrix(mMatL*mview);
#endif
	_camera->draw(_drawables);

#ifdef DEBUG_DRAW
	_debugDraw.setTransformationProjectionMatrix(
		_camera->projectionMatrix()*_camera->cameraMatrix());
	_bWorld->debugDrawWorld();
#endif

#ifdef CAVE_BUILD
	Magnum::GL::defaultFramebuffer.mapForDraw(Magnum::GL::DefaultFramebuffer::DrawAttachment::BackRight);
	Magnum::GL::defaultFramebuffer.clear(Magnum::GL::FramebufferClear::Color | Magnum::GL::FramebufferClear::Depth);
	_camera->setProjectionMatrix(mMatR*mview);
	_camera->draw(_drawables);

#ifdef DEBUG_DRAW
	_debugDraw.setTransformationProjectionMatrix(
		_camera->projectionMatrix()*_camera->cameraMatrix());
	_bWorld->debugDrawWorld();
#endif
#endif
}

#pragma region Input

void MyProject::keyPressEvent(KeyEvent &event) { input.keyPressEvent(event); }

void MyProject::keyReleaseEvent(KeyEvent &event) { input.keyReleaseEvent(event); }

void MyProject::mouseMoveEvent(MouseMoveEvent &event) { input.mouseMoveEvent(event); }

void MyProject::mousePressEvent(MouseEvent &event) { input.mousePressEvent(event); }

void MyProject::mouseReleaseEvent(MouseEvent &event) { input.mouseReleaseEvent(event); }

void MyProject::updateHandAndHeadTrafo()
{
	btTransform t;
	t.setIdentity();

#ifdef CAVE_BUILD
	// get head position for correct shading
	glm::mat4 userTrafo;
	rNode->getUserTrafo(userTrafo);
	Magnum::Matrix4 userMatrix = Helper::glm2magnum(userTrafo);
	DrawablePBR::headPosition = userMatrix.translation();

	// get hand transformation from wand
	glm::mat4 wandTrafo = rNode->getWandTrafo();
	handTrafo = Helper::glm2magnum(wandTrafo);

	// translate it a bit so its in front of wand instead of inside
	t.setOrigin(btVector3(handTrafo.translation() - handTrafo.backward() * 10.f));
#else
	// update camera position according to input
	debugCam.update(input, deltaTime);

	// get hand transformation from debug cam
	handTrafo = _cameraObject.transformationMatrix();
	DrawablePBR::headPosition = _cameraObject.transformationMatrix().translation();
	t.setOrigin(btVector3(handTrafo.translation()));

	Object3D obj;
	obj.setTransformation(handTrafo);
	obj.translate(-handTrafo.backward() * 20.f - handTrafo.up() * 40.f);
	handTrafo = obj.transformation();
#endif // CAVE_BUILD

	// move hand ghost object & rigidbody to hand position
	// and additonally set rotation for rigidbody 
	//
	_handGhostObject->setWorldTransform(t);

	Magnum::Quaternion quatMagnum = Magnum::Quaternion::fromMatrix(handTrafo.rotation());
	btQuaternion quat(btVector3(quatMagnum.axis()), (float)quatMagnum.angle());
	t.setRotation(quat);
	_handRigidBody->setWorldTransform(t);
}

void MyProject::processInput()
{
#ifdef CAVE_BUILD
	// Synch wand button presses
	if (wandButtonSyncher->hasChanged())
	{
		glm::bvec4 buttonPressed = wandButtonSyncher->getData();
		input.wandButtonEvent(KeyCode::WandTrigger, buttonPressed[0]);
		input.wandButtonEvent(KeyCode::WandRight, buttonPressed[1]);
		input.wandButtonEvent(KeyCode::WandMiddle, buttonPressed[2]);
		input.wandButtonEvent(KeyCode::WandLeft, buttonPressed[3]);
	}
#else
	// center mouse cursor in screen
	glfwSetCursorPos(window(), 0., 0.);
	input.resetMouseMotion();
#endif

	bool shootTimeGun, grabObject;
#ifdef CAVE_BUILD
	shootTimeGun = input.getKeyDown(KeyCode::WandLeft) || input.getKeyDown(KeyCode::WandRight);
	grabObject = input.getKeyDown(KeyCode::WandTrigger);
#else
	shootTimeGun = input.getKeyDown(KeyCode::MouseLeft) || input.getKeyDown(KeyCode::MouseRight);
	grabObject = input.getKeyDown(KeyCode::Space);
#endif

	// Shooting TimeGun
	//

	// if player presses time gun butons, shoot a ray to set the currently targeted TimeObject correctly
	if (shootTimeGun)
	{
		// forward or reverse?
		bool forward;
#ifdef CAVE_BUILD
		forward = input.getKeyDown(KeyCode::WandLeft);
#else
		forward = input.getKeyDown(KeyCode::MouseLeft);
#endif
		TimeObject::mode = forward ? TimeGunMode::Forward : TimeGunMode::Reverse;

		timeRay->setActive(true);
		BaseObject* obj = castRay();
		if (obj && obj->isTimeObject())
		{
			TimeObject::setTarget(dynamic_cast<TimeObject*>(obj));
		}
		else // object is no TimeObject
			TimeObject::setTarget(nullptr);
	}
	else // player is not shooting
	{
		TimeObject::setTarget(nullptr);
		timeRay->setActive(false);
	}


	// Grabbing
	//

	// Custom Collision Callbacks for the Scenes
	if (currentScene == 1) {
		btRigidBody* floppy = BaseObject::getObject(Constant::SN_FLOPPY)->getRigidBodies().at(Constant::SN_FLOPPY);
		CollisionCallbacks::FloppySensorCallback callbackFloppy(*floppy);
		_bWorld->contactTest(floppy, callbackFloppy);

		Safe* safe = (Safe*)BaseObject::getObject(Constant::SN_SAFE);
		safe->checkDial();
	}
	else if (currentScene == 2) {
		btRigidBody* bed = BaseObject::getObject(Constant::SN_BED)->getRigidBodies().at(Constant::SN_BED);
		CollisionCallbacks::PlatformSensorCallback callbackPlatform(*bed);
		_bWorld->contactTest(bed, callbackPlatform);

		// Check if player is standing on platform, drop to 0 if not
		Platform* platform = (Platform*)BaseObject::getAllObjects().at(Constant::SN_PLATFORM);
		platform->update(DrawablePBR::headPosition);
	}
	
	// check if player wants to grab an object
	//CollisionCallbacks::ContactSensorCallback callback(*_handRigidBody, input);
	_bWorld->contactTest(_handRigidBody, callback);

	// check if player wants to drop currently grabbed object
	if (!grabObject)
	{
		for (std::pair<std::string, GrabObject*> kvp : GrabObject::getAllObjects())
			kvp.second->ungrab();
	}


	// Interacting (without button press)
	//

	// find all InteractableObjects that currently overlap 
	// with the hand ghost object and set them triggered
	std::vector<InteractableObject*> triggeredObjects;
	for (int i = 0; i < _handGhostObject->getNumOverlappingObjects(); i++)
	{
		btCollisionObject* obj = _handGhostObject->getOverlappingObject(i);
		if (obj && obj->getUserPointer())
		{
			BaseObject* bObj = (BaseObject*)obj->getUserPointer();
			if (bObj->isInteractableObject())
			{
				InteractableObject* iObj = dynamic_cast<InteractableObject*>(bObj);
				iObj->setTriggered(true, _handGhostObject->getOverlappingObject(i));
				triggeredObjects.push_back(iObj);
			}
		}
	}

	// set all objects that aren't overlapping to not triggered
	for (auto kvp : InteractableObject::getAllObjects())
	{
		auto it = std::find(triggeredObjects.begin(), triggeredObjects.end(), kvp.second);
		if (it == triggeredObjects.end())
			kvp.second->setTriggered(false, nullptr);
	}
}
#pragma endregion

BaseObject* MyProject::castRay() {
	btVector3 rayFrom = _handRigidBody->getWorldTransform().getOrigin();
	btVector3 rayTo;
	Magnum::Matrix4 rayTrafo;

#ifdef CAVE_BUILD
	rayTo = btVector3(handTrafo.translation() - handTrafo.backward() * 600.0f);
	rayTrafo = handTrafo;
#else
	Magnum::Matrix4 camTrafo = _cameraObject.transformationMatrix();
	rayTo = btVector3(camTrafo.translation() - camTrafo.backward() * 600.0f);
#endif

	btCollisionWorld::ClosestRayResultCallback rayCallback(rayFrom, rayTo);
	_bWorld->rayTest(rayFrom, rayTo, rayCallback);

	if (rayCallback.hasHit())
	{
		btVector3 pickPos = rayCallback.m_hitPointWorld;

#ifndef CAVE_BUILD
		rayTrafo = Magnum::Matrix4::lookAt(handTrafo.translation(), Magnum::Vector3(pickPos), Magnum::Vector3(0, 1, 0));
#endif

		timeRay->recalculateMesh(handTrafo.translation(), Magnum::Vector3(pickPos), rayTrafo, deltaTime);

		btRigidBody* body = (btRigidBody*)btRigidBody::upcast(rayCallback.m_collisionObject);
		if (body)
		{
			BaseObject* obj = (BaseObject*)body->getUserPointer();
			return obj;
		}
	}
	return nullptr;
}

MAGNUM_APPLICATION_MAIN(MyProject)
