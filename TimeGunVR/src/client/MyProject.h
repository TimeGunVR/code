#pragma once
#include "stdafx.h"

#include "Input.h"
#include "DebugCam.h"
#include "drawing/TimeRay.h"
#include "CollisionCallbacks.h"

#include "objects/base/BaseObject.h"

#include <Magnum/Platform/GlfwApplication.h>
#include <Magnum/SceneGraph/AnimableGroup.h>

//using namespace Magnum;
//using namespace Math::Literals;

class MyProject : public Magnum::Platform::GlfwApplication
{
public:
	explicit MyProject(const Arguments& arguments);
	~MyProject() { rNode->stopSynching(); }

private:
	void loadScene(int sceneID = -1); /// -1 for default scene

	void drawEvent() override;
	void viewportEvent(const Magnum::Vector2i& size) override;
	void draw();

#pragma region Input

	void keyPressEvent(KeyEvent& event)override;
	void keyReleaseEvent(KeyEvent& event)override;
	void mouseMoveEvent(MouseMoveEvent &event) override;
	void mousePressEvent(MouseEvent &event) override;
	void mouseReleaseEvent(MouseEvent& event) override;

	// update hand ghost object & rigidbody from either
	// wand & head transformation (CAVE_BUILD) or the 
	// mouse input & debug cam
	void updateHandAndHeadTrafo();
	// process input from either wand (CAVE_BUILD) or mouse
	void processInput();

	Input input{};

#pragma endregion

	BaseObject* castRay();

	Scene3D _scene;
	Object3D _cameraObject;
	Object3D _handObject;
	btGhostObject* _handGhostObject;
	btRigidBody* _handRigidBody;

	Magnum::Matrix4 handTrafo;

	int currentScene = 0;

	DebugCam debugCam{ _cameraObject };
	std::shared_ptr<Magnum::SceneGraph::Camera3D> _camera;

	Magnum::SceneGraph::DrawableGroup3D _drawables;
	Magnum::SceneGraph::AnimableGroup3D _animables;

	float telportationTime;
	const float TELPORTATION_TIME = 3.0f;

	std::unique_ptr<TimeRay> timeRay;

	btDiscreteDynamicsWorld* _bWorld;
	Magnum::BulletIntegration::DebugDraw _debugDraw{ Magnum::NoCreate };

	CollisionCallbacks::ContactSensorCallback callback{};

	// synchlib stuff
	std::shared_ptr<synchlib::renderNode> rNode;
	std::shared_ptr<synchlib::udpSynchObject<long long>> timeSyncher;
	std::shared_ptr< synchlib::udpSynchObject<glm::bvec4>> wandButtonSyncher;
	synchlib::caveConfig m_conf;

	float m_lastTime = 0.f;
	float deltaTime = 0;

	const float FIXED_DELTA = 1.f / 60.f;
};