#include "DrawablePBR.h"

#include <Magnum/GL/Mesh.h>

//#define CAVE_BUILD

DrawablePBR::DrawablePBR(Object3D& object, Magnum::GL::Mesh& mesh, Magnum::SceneGraph::DrawableGroup3D* drawables, MaterialPBR& material)
	: Magnum::SceneGraph::Drawable3D{ object, drawables }, mesh{ mesh }, material{ material }
{
}

PBR_Shader* DrawablePBR::shader_opaque;
PBR_Shader* DrawablePBR::shader_cutoff;
EnvironmentMap* DrawablePBR::envMap;

Magnum::Vector3 DrawablePBR::lightColor;
float DrawablePBR::lightIntensityModifier;

Magnum::Vector3 DrawablePBR::headPosition;

void DrawablePBR::init(std::string environmentMapName)
{
	if (shader_opaque == nullptr)
	{
		shader_opaque = new PBR_Shader{
			PBR_Shader::Flag::HAS_NORMALS |
			PBR_Shader::Flag::HAS_UV |
			PBR_Shader::Flag::HAS_BASECOLORMAP |
			PBR_Shader::Flag::HAS_NORMALMAP |
			PBR_Shader::Flag::HAS_OCCROUGHMETALMAP |
			PBR_Shader::Flag::USE_IBL |
			PBR_Shader::Flag::HAS_EMISSIVEMAP
		};
	}


	if (shader_cutoff == nullptr)
	{
		shader_cutoff = new PBR_Shader{
			PBR_Shader::Flag::HAS_NORMALS |
			PBR_Shader::Flag::HAS_UV |
			PBR_Shader::Flag::HAS_BASECOLORMAP |
			PBR_Shader::Flag::HAS_NORMALMAP |
			PBR_Shader::Flag::HAS_OCCROUGHMETALMAP |
			PBR_Shader::Flag::USE_IBL |
			PBR_Shader::Flag::HAS_EMISSIVEMAP |
			PBR_Shader::Flag::HAS_ALPHA
		};
	}

	if (envMap == nullptr)
		envMap = new EnvironmentMap(environmentMapName);

	lightColor = Constant::LIGHT_COLOR;
	lightIntensityModifier = 1.0;
}

void DrawablePBR::draw(const Magnum::Matrix4 & transformationMatrix, Magnum::SceneGraph::Camera3D & camera)
{
	Magnum::Matrix4 inverseCamMat = camera.cameraMatrix().inverted();
	Magnum::Matrix4 modelMatrix = inverseCamMat * transformationMatrix;

	PBR_Shader* shader;

	if (material.alphaCutoff())
		shader = shader_cutoff;
	else
		shader = shader_opaque;

	(*shader)
		/// Matrices
		.setModelViewProjectionMatrix(camera.projectionMatrix() * transformationMatrix)
		.setModelMatrix(modelMatrix)
		.setNormalMatrix(modelMatrix.rotation())
		.setCameraPosition(headPosition)
		.setUVOffset(material.uvOffset())
		/// Light
		.setLightDirection(Constant::LIGHT_DIRECTION)
		.setLightColor(lightColor)
		.setLightIntensityModifier(lightIntensityModifier)
		/// Textures
		.setBaseColorTexture(*(material.baseColorMap()))
		.setNormalMap(*(material.normalMap()))
		.setOcclusionRoughnessMetallicMap(*(material.occRoughMetalMap()))
		.setDiffuseEnvTexture(envMap->diffuse())
		.setSpecularEnvTexture(envMap->specular())
		.setBrdfLookUpTable(envMap->brdfLUT())
		.setEmissiveFactor(material.emissiveFactor());

	if (material.emissiveMap())
		shader->setEmissiveMap(*(material.emissiveMap()));
	else
		shader->clearEmissiveMap();

	mesh.draw(*shader);
}
