#pragma once
#include "../stdafx.h"
#include "PBR_Shader.h"
#include "EnvironmentMap.h"
#include "MaterialPBR.h"

#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>

// DrawablePBR
//
// drawable with a PBR (Phyiscally Based Rendering) shader (opaque or alpha cutoff);
// used for almost all objects in the game
//
class DrawablePBR : public Magnum::SceneGraph::Drawable3D
{
public:
	explicit DrawablePBR(Object3D& object, Magnum::GL::Mesh& mesh, Magnum::SceneGraph::DrawableGroup3D* drawables, MaterialPBR& material);

	static void init(std::string environmentMapName);

	static Magnum::Vector3 lightColor;
	static float lightIntensityModifier;
	static Magnum::Vector3 headPosition;

private:
	void draw(const Magnum::Matrix4& transformationMatrix, Magnum::SceneGraph::Camera3D& camera) override;

	Magnum::GL::Mesh& mesh;
	MaterialPBR& material;

	static PBR_Shader* shader_opaque; // standard shader
	static PBR_Shader* shader_cutoff; // discards fragments with base color alpha < 0.5
	static EnvironmentMap* envMap;
};