#include "EnvironmentMap.h"

#include <MagnumPlugins/DdsImporter/DdsImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/GL/TextureFormat.h>
#include "../AssetManager.h"

EnvironmentMap::EnvironmentMap(std::string name)
{
	Magnum::PluginManager::Manager<Magnum::Trade::AbstractImporter> manager;

	// Initialize DdsImporter
	std::unique_ptr<Magnum::Trade::AbstractImporter> importer
		= manager.loadAndInstantiate("DdsImporter");

	if (!importer) {
		Magnum::Error{} << "[DdsImporter] Loading/initializing failed";
		return;
	}

	_diffuse = loadCubeTexture(*importer, name + Constant::TEX_ENV_DIFFUSE);
	_specular = loadCubeTexture(*importer, name + Constant::TEX_ENV_SPECULAR);
	_brdfLUT = loadTexture(*importer, name + Constant::TEX_ENV_BRDFLUT);
}

Magnum::GL::CubeMapTexture EnvironmentMap::loadCubeTexture(Magnum::Trade::AbstractImporter& importer, std::string imagename)
{
	Magnum::GL::CubeMapTexture texture;
	CORRADE_ASSERT_OUTPUT(importer.openFile(AssetManager::_assetPath + imagename + ".dds"),
		"[DdsImporter] Opening file failed (" + imagename + ".dds)", texture);

	int mipCount = importer.image2DCount() / 6;

	texture
		.setWrapping(Magnum::SamplerWrapping::Repeat)
		.setMagnificationFilter(Magnum::SamplerFilter::Linear)
		.setMinificationFilter(Magnum::SamplerFilter::Linear)
		.setMaxAnisotropy(Magnum::GL::Sampler::maxMaxAnisotropy())
		.setStorage(mipCount, Magnum::GL::TextureFormat::RGB8, importer.image2D(0)->size());

	for (int i = 0; i < mipCount; i++)
	{
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::PositiveX, i, {}, *importer.image2D(mipCount * 0 + i));
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::NegativeX, i, {}, *importer.image2D(mipCount * 1 + i));
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::PositiveY, i, {}, *importer.image2D(mipCount * 2 + i));
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::NegativeY, i, {}, *importer.image2D(mipCount * 3 + i));
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::PositiveZ, i, {}, *importer.image2D(mipCount * 4 + i));
		texture.setSubImage(Magnum::GL::CubeMapCoordinate::NegativeZ, i, {}, *importer.image2D(mipCount * 5 + i));
	}

	return texture;
}

Magnum::GL::Texture2D EnvironmentMap::loadTexture(Magnum::Trade::AbstractImporter& importer, std::string imagename)
{
	Magnum::GL::Texture2D texture;

	CORRADE_ASSERT_OUTPUT(importer.openFile(AssetManager::_assetPath + imagename + ".dds"),
		"[DdsImporter] Opening file failed (" + imagename + ".dds)", texture);

	texture
		.setWrapping(Magnum::SamplerWrapping::Repeat)
		.setMagnificationFilter(Magnum::SamplerFilter::Linear)
		.setMinificationFilter(Magnum::SamplerFilter::Linear)
		.setMaxAnisotropy(Magnum::GL::Sampler::maxMaxAnisotropy())
		.setStorage(0, Magnum::GL::TextureFormat::RGB8, importer.image2D(0)->size())
		.setCompressedImage(0, *importer.image2D(0));

	return texture;
}

