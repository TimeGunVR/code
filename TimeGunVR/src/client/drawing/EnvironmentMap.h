#pragma once
#include "../stdafx.h"
#include <Magnum/Texture.h>
#include <Magnum/CubeMapTexture.h>

#include <Magnum/Trade/AbstractImporter.h>

#include <iostream>

// EnvironmentMap
//
// class for loading & storing environment maps for BPR shading
//
class EnvironmentMap
{
public:

	// @name: prefix used to load textures from disc
	EnvironmentMap(std::string name);

	Magnum::GL::CubeMapTexture& diffuse() { return _diffuse; }
	Magnum::GL::CubeMapTexture& specular() { return _specular; }
	Magnum::GL::Texture2D& brdfLUT() { return _brdfLUT; }

private:
	Magnum::GL::CubeMapTexture loadCubeTexture(Magnum::Trade::AbstractImporter& importer, std::string imagename);
	Magnum::GL::Texture2D loadTexture(Magnum::Trade::AbstractImporter & importer, std::string imagename);

	Magnum::GL::CubeMapTexture _diffuse;
	Magnum::GL::CubeMapTexture _specular;
	Magnum::GL::Texture2D _brdfLUT;
};

