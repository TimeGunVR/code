#pragma once
#include "../stdafx.h"
#include <Magnum/GL/Texture.h>

// MaterialPBR
//
// class for storing textures & material attributes for PBR shading
//
class MaterialPBR
{
public:
	MaterialPBR(Magnum::Texture2D * baseColorTexture, Magnum::Texture2D * normalTexture,
		Magnum::Texture2D * occRoughMetalTexture)
		: baseColorTexture{ baseColorTexture }, normalTexture{ normalTexture }, 
		occRoughMetalTexture{ occRoughMetalTexture }
	{
	}
	MaterialPBR(Magnum::Texture2D * baseColorTexture, Magnum::Texture2D * normalTexture,
		Magnum::Texture2D * occRoughMetalTexture, Magnum::Texture2D * emissiveTexture)
		: baseColorTexture{ baseColorTexture }, normalTexture{ normalTexture }, 
		occRoughMetalTexture{ occRoughMetalTexture }, emissiveTexture{ emissiveTexture }
	{
	}

	Magnum::Texture2D* baseColorMap() { return baseColorTexture; }
	Magnum::Texture2D* normalMap() { return normalTexture; }
	Magnum::Texture2D* occRoughMetalMap() { return occRoughMetalTexture; }
	Magnum::Texture2D* emissiveMap() { return emissiveTexture; }

	Magnum::Vector3 emissiveFactor() { return _emissiveFactor; }
	void setEmissiveFactor(Magnum::Vector3 color) { _emissiveFactor = color; }

	Magnum::Vector2 uvOffset() { return _uvOffset; }
	void setUVOffset(Magnum::Vector2 offset) { _uvOffset = offset; }

	bool alphaCutoff() { return _alphaCutoff; }
	void setAlphaCutoff(bool alphaCutoff) { _alphaCutoff = alphaCutoff; }

private:
	Magnum::Texture2D* baseColorTexture;
	Magnum::Texture2D* normalTexture;
	Magnum::Texture2D* occRoughMetalTexture; /// Occlusion(R), Roughness(G), Metallic(B)

	Magnum::Texture2D* emissiveTexture;
	Magnum::Vector3 _emissiveFactor{ 1.f };

	Magnum::Vector2 _uvOffset{ 0, 0 };

	bool _alphaCutoff = false;
};