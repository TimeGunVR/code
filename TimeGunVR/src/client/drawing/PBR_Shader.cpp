#include "PBR_Shader.h"

#include <Corrade/Utility/Resource.h>
#include <Magnum/GL/Context.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Version.h>

PBR_Shader::PBR_Shader(Flags flags) : flags(flags)
{
	const Magnum::GL::Version version{ Magnum::GL::Version::GL430 };

	MAGNUM_ASSERT_GL_VERSION_SUPPORTED(version);

	const Magnum::Utility::Resource rs{ "ShaderResources" };

	Magnum::GL::Shader vert{ version, Magnum::GL::Shader::Type::Vertex };
	Magnum::GL::Shader frag{ version, Magnum::GL::Shader::Type::Fragment };

	vert.addSource(flags & Flag::HAS_NORMALS ? "#define HAS_NORMALS\n" : "")
		.addSource(flags & Flag::HAS_ALPHA ? "#define HAS_ALPHA\n" : "")
		.addSource(flags & Flag::HAS_UV ? "#define HAS_UV\n" : "")
		.addSource(rs.get("PBR_Shader.vert"));
	frag.addSource(flags & Flag::HAS_NORMALS ? "#define HAS_NORMALS\n" : "")
		.addSource(flags & Flag::HAS_ALPHA ? "#define HAS_ALPHA\n" : "")
		.addSource(flags & Flag::USE_IBL ? "#define USE_IBL\n" : "")
		.addSource(flags & Flag::HAS_BASECOLORMAP ? "#define HAS_BASECOLORMAP\n" : "")
		.addSource(flags & Flag::HAS_NORMALMAP ? "#define HAS_NORMALMAP\n" : "")
		.addSource(flags & Flag::HAS_EMISSIVEMAP ? "#define HAS_EMISSIVEMAP\n" : "")
		.addSource(flags & Flag::HAS_OCCROUGHMETALMAP ? "#define HAS_OCCROUGHMETALMAP\n" : "")
		.addSource("#define MANUAL_SRGB\n")
		.addSource("#define USE_TEX_LOD\n")
		.addSource(rs.get("PBR_Shader.frag"));

	CORRADE_INTERNAL_ASSERT_OUTPUT(Magnum::GL::Shader::compile({ vert,frag }));

	attachShaders({ vert, frag });

	CORRADE_INTERNAL_ASSERT_OUTPUT(link());
}

PBR_Shader& PBR_Shader::setModelViewProjectionMatrix(const Magnum::Matrix4& matrix)
{
	setUniform(Uniforms::u_MVPMatrix, matrix);
	return *this;
}

PBR_Shader& PBR_Shader::setModelMatrix(const Magnum::Matrix4& matrix)
{
	setUniform(Uniforms::u_ModelMatrix, matrix);
	return *this;
}

PBR_Shader& PBR_Shader::setNormalMatrix(const Magnum::Matrix3x3& matrix)
{
	setUniform(Uniforms::u_NormalMatrix, matrix);
	return *this;
}

PBR_Shader & PBR_Shader::setUVOffset(const Magnum::Vector2 & offset)
{
	setUniform(Uniforms::u_UVOffset, offset);
	return *this;
}

PBR_Shader& PBR_Shader::setLightDirection(const Magnum::Vector3& lightDir)
{
	setUniform(Uniforms::u_LightDirection, lightDir);
	return *this;
}

PBR_Shader& PBR_Shader::setLightColor(const Magnum::Vector3& color)
{
	setUniform(Uniforms::u_LightColor, color);
	return *this;
}

PBR_Shader& PBR_Shader::setLightIntensityModifier(Magnum::Float  intensity) {
	setUniform(Uniforms::u_LightIntensityModifierUniform, intensity);
	return *this;
}

PBR_Shader& PBR_Shader::setDiffuseEnvTexture(Magnum::GL::CubeMapTexture& texture)
{
	CORRADE_ASSERT(flags & Flag::USE_IBL,
		"PBR_Shader::setDiffuseEnvTexture(): image based lighting NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_DiffuseEnvSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setSpecularEnvTexture(Magnum::GL::CubeMapTexture& texture)
{
	CORRADE_ASSERT(flags & Flag::USE_IBL,
		"PBR_Shader::setSpecularEnvTexture(): image based lighting NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_SpecularEnvSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setBrdfLookUpTable(Magnum::GL::Texture2D& texture)
{
	CORRADE_ASSERT(flags & Flag::USE_IBL,
		"PBR_Shader::setBrdfLookUpTable(): image based lighting NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_brdfLUT);
	return *this;
}

PBR_Shader& PBR_Shader::setBaseColorTexture(Magnum::GL::Texture2D& texture)
{
	CORRADE_ASSERT(flags & Flag::HAS_BASECOLORMAP,
		"PBR_Shader::setBaseColorTexture(): base color textures NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_BaseColorSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setNormalMap(Magnum::GL::Texture2D& texture)
{
	CORRADE_ASSERT(flags & Flag::HAS_NORMALMAP,
		"PBR_Shader::setNormalMap(): normal maps NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_NormalSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setNormalScale(const Magnum::Float& normScale)
{
	CORRADE_ASSERT(flags & Flag::HAS_NORMALMAP,
		"PBR_Shader::setNormalScale(): normal maps NOT enabled (set respective flag when creating shader)", *this);
	setUniform(Uniforms::u_NormalScale, normScale);
	return *this;
}

PBR_Shader& PBR_Shader::setEmissiveMap(Magnum::GL::Texture2D& texture)
{
	CORRADE_ASSERT(flags & Flag::HAS_EMISSIVEMAP,
		"PBR_Shader::setEmissiveMap(): emissive maps NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_EmissiveSampler);
	return *this;
}

PBR_Shader& PBR_Shader::clearEmissiveMap()
{
	CORRADE_ASSERT(flags & Flag::HAS_EMISSIVEMAP,
		"PBR_Shader::clearEmissiveMap(): emissive maps NOT enabled (set respective flag when creating shader)", *this);
	Magnum::Texture2D::unbind(TextureLayer::u_EmissiveSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setEmissiveFactor(const Magnum::Vector3& emFactor)
{
	CORRADE_ASSERT(flags & Flag::HAS_EMISSIVEMAP,
		"PBR_Shader::setEmissiveFactor(): emissive maps NOT enabled (set respective flag when creating shader)", *this);
	setUniform(Uniforms::u_EmissiveFactor, emFactor);
	return *this;
}

PBR_Shader& PBR_Shader::setOcclusionRoughnessMetallicMap(Magnum::GL::Texture2D& texture)
{
	CORRADE_ASSERT(flags & Flag::HAS_OCCROUGHMETALMAP,
		"PBR_Shader::setOcclusionRoughnessMetallicMap(): occlusion/roughness/metal maps NOT enabled (set respective flag when creating shader)", *this);
	texture.bind(TextureLayer::u_OcclusionRoughnessMetallicSampler);
	return *this;
}

PBR_Shader& PBR_Shader::setOcclusionStrength(const Magnum::Float& occStrenght)
{
	CORRADE_ASSERT(flags & Flag::HAS_OCCROUGHMETALMAP,
		"PBR_Shader::setOcclusionStrength(): occlusion/roughness/metal maps NOT enabled (set respective flag when creating shader)", *this);
	setUniform(Uniforms::u_OcclusionStrength, occStrenght);
	return *this;
}

PBR_Shader& PBR_Shader::setMetallicRoughnessValues(const Magnum::Vector2& metalRoughVal)
{
	setUniform(Uniforms::u_MetallicRoughnessValues, metalRoughVal);
	return *this;
}

PBR_Shader& PBR_Shader::setBaseColorFactor(const Magnum::Vector4& baseCol)
{
	setUniform(Uniforms::u_BaseColorFactor, baseCol);
	return *this;
}

PBR_Shader& PBR_Shader::setCameraPosition(const Magnum::Vector3& camPos)
{
	setUniform(Uniforms::u_Camera, camPos);
	return *this;
}

PBR_Shader& PBR_Shader::setScaleDiffBaseMR(const Magnum::Vector4& diffBaseMRScale)
{
	setUniform(Uniforms::u_ScaleDiffBaseMR, diffBaseMRScale);
	return *this;
}

PBR_Shader& PBR_Shader::setScaleFGDSpec(const Magnum::Vector4& FGDSpecScale)
{
	setUniform(Uniforms::u_ScaleFGDSpec, FGDSpecScale);
	return *this;
}

PBR_Shader& PBR_Shader::setScaleIBLAmbient(const Magnum::Vector4& IBLAmbientScale)
{
	setUniform(Uniforms::u_ScaleIBLAmbient, IBLAmbientScale);
	return *this;
}
