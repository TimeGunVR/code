#pragma once

#include <Magnum/GL/AbstractShaderProgram.h>

#include <Magnum/GL/Texture.h>
#include <Magnum/GL/CubeMapTexture.h>

#include <Magnum/Math/Matrix4.h>

#include <Magnum/Shaders/Generic.h>

// PBR_Shader
//
// class for loading PBR shaders @PBR_Shader.vert & @PBR_Shader.frag
// and setting their attributes & uniforms; used by @DrawablePBR
//
class PBR_Shader : public Magnum::GL::AbstractShaderProgram
{

public:
	/// Attributes
	typedef Magnum::Shaders::Generic3D::Position Position;
	typedef Magnum::Shaders::Generic3D::Normal Normal;
	typedef Magnum::Shaders::Generic3D::TextureCoordinates TextureCoordinates;

	enum class Flag : Magnum::UnsignedByte {

		HAS_NORMALS = 0b00000001,
		HAS_ALPHA	= 0b00000010,
		HAS_UV = 0b00000100,
		USE_IBL = 0b00001000,
		HAS_BASECOLORMAP = 0b00010000,
		HAS_NORMALMAP = 0b00100000,
		HAS_EMISSIVEMAP = 0b01000000,
		HAS_OCCROUGHMETALMAP = 0b10000000
	};
	typedef Magnum::Containers::EnumSet<Flag> Flags;
	Flags getFlags() const { return flags; }

	explicit PBR_Shader(Flags flags = {});

	/// Matrices
	PBR_Shader& setModelViewProjectionMatrix(const Magnum::Matrix4& matrix);
	PBR_Shader& setModelMatrix(const Magnum::Matrix4& matrix);
	PBR_Shader& setNormalMatrix(const Magnum::Matrix3x3& matrix);

	PBR_Shader& setUVOffset(const Magnum::Vector2& offset);

	/// Light
	PBR_Shader& setLightDirection(const Magnum::Vector3& lightDir);
	PBR_Shader& setLightColor(const Magnum::Vector3& color);
	PBR_Shader& setLightIntensityModifier(Magnum::Float intensity);

	/// Environment
	PBR_Shader& setDiffuseEnvTexture(Magnum::GL::CubeMapTexture& texture);
	PBR_Shader& setSpecularEnvTexture(Magnum::GL::CubeMapTexture& texture);
	PBR_Shader& setBrdfLookUpTable(Magnum::GL::Texture2D& brdfLUT);

	/// BaseColor Texture
	PBR_Shader& setBaseColorTexture(Magnum::GL::Texture2D& texture);

	/// Normal Map
	PBR_Shader& setNormalMap(Magnum::GL::Texture2D& texture);
	PBR_Shader& setNormalScale(const Magnum::Float& normScale);

	/// Emissive 
	PBR_Shader& setEmissiveMap(Magnum::GL::Texture2D& texture);
	PBR_Shader& clearEmissiveMap();
	PBR_Shader& setEmissiveFactor(const Magnum::Vector3& emFactor);

	/// Occlusion(R) Rough(G) Metal(B) Texture
	PBR_Shader& setOcclusionRoughnessMetallicMap(Magnum::GL::Texture2D& texture);
	PBR_Shader& setOcclusionStrength(const Magnum::Float& occStrenght);

	/// BaseColor & MetalRough Modifiers
	PBR_Shader& setMetallicRoughnessValues(const Magnum::Vector2& metalRoughVal);
	PBR_Shader& setBaseColorFactor(const Magnum::Vector4& baseCol);

	/// Camera Position
	PBR_Shader& setCameraPosition(const Magnum::Vector3& camPos);

	/// debugging flags used for shader output of intermediate PBR variables
	PBR_Shader& setScaleDiffBaseMR(const Magnum::Vector4& diffBaseMRScale);
	PBR_Shader& setScaleFGDSpec(const Magnum::Vector4& FGDSpecScale);
	PBR_Shader& setScaleIBLAmbient(const Magnum::Vector4& IBLAmbientScale);


private:
	Flags flags;

	enum TextureLayer : Magnum::Int // DO NOT CHANGE ORDER!!! INSERT NEW VALUES AT BOTTOM!!!
	{
		u_DiffuseEnvSampler,
		u_SpecularEnvSampler,
		u_brdfLUT,
		u_BaseColorSampler,
		u_NormalSampler,
		u_EmissiveSampler,
		u_OcclusionRoughnessMetallicSampler
	};

	enum Uniforms : Magnum::Int // DO NOT CHANGE ORDER!!! INSERT NEW VALUES AT BOTTOM!!!
	{
		u_MVPMatrix,
		u_ModelMatrix,
		u_NormalMatrix,
		u_LightDirection,
		u_LightColor,
		u_NormalScale,
		u_EmissiveFactor,
		u_OcclusionStrength,
		u_MetallicRoughnessValues,
		u_BaseColorFactor,
		u_Camera,
		u_ScaleDiffBaseMR,
		u_ScaleFGDSpec,
		u_ScaleIBLAmbient,
		u_LightIntensityModifierUniform,
		u_UVOffset
	};
};

CORRADE_ENUMSET_OPERATORS(PBR_Shader::Flags);