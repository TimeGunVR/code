#include "PolyboardShader.h"

#include <Corrade/Utility/Resource.h>
#include <Magnum/GL/Context.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Version.h>

PolyboardShader::PolyboardShader()
{
	const Magnum::GL::Version version{ Magnum::GL::Version::GL430 };

	MAGNUM_ASSERT_GL_VERSION_SUPPORTED(version);

	const Magnum::Utility::Resource rs{ "ShaderResources" };

	Magnum::GL::Shader vert{ version, Magnum::GL::Shader::Type::Vertex };
	Magnum::GL::Shader geom{ version, Magnum::GL::Shader::Type::Geometry };
	Magnum::GL::Shader frag{ version, Magnum::GL::Shader::Type::Fragment };

	vert.addSource(rs.get("Polyboard.vert"));
	geom.addSource(rs.get("Polyboard.geom"));
	frag.addSource(rs.get("Polyboard.frag"));

	CORRADE_INTERNAL_ASSERT_OUTPUT(Magnum::GL::Shader::compile({ vert, geom, frag }));

	attachShaders({ vert, geom, frag });

	CORRADE_INTERNAL_ASSERT_OUTPUT(link());
}

PolyboardShader& PolyboardShader::setModelViewProjectionMatrix(const Magnum::Matrix4& matrix)
{
	setUniform(U_MVPMATRIX, matrix);
	return *this;
}

PolyboardShader & PolyboardShader::setModelMatrix(const Magnum::Matrix4 & matrix)
{
	setUniform(U_MODELMATRIX, matrix);
	return *this;
}

PolyboardShader & PolyboardShader::setCameraPosition(const Magnum::Vector3 & position)
{
	setUniform(U_CAMERA, position);
	return *this;
}

PolyboardShader & PolyboardShader::setRadius(const Magnum::Float radius)
{
	setUniform(U_RADIUS, radius);
	return *this;
}

PolyboardShader& PolyboardShader::setTexture(Magnum::GL::Texture2D& texture)
{
	texture.bind(TEX_LAYER);
	return *this;
}
