#pragma once
#include <Magnum/GL/Texture.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Shaders/Generic.h>
#include <Magnum/GL/AbstractShaderProgram.h>

// PolyboardShader
//
// class for loading polyboard shaders @Polyboard.vert, @Polyboard.geom & @Polyboard.vert
// and setting their attributes & uniforms; used by @TimeRay for rendering the timegun ray
//
class PolyboardShader : public Magnum::GL::AbstractShaderProgram
{
public:
	typedef Magnum::Shaders::Generic3D::Position Position;
	typedef Magnum::Shaders::Generic2D::TextureCoordinates TextureCoordinates;

	explicit PolyboardShader();

	PolyboardShader& setModelViewProjectionMatrix(const Magnum::Matrix4& matrix);
	PolyboardShader& setModelMatrix(const Magnum::Matrix4& matrix);
	PolyboardShader& setCameraPosition(const Magnum::Vector3& position);
	PolyboardShader& setRadius(const Magnum::Float radius);

	PolyboardShader& setTexture(Magnum::GL::Texture2D& texture);

private:
	enum Uniforms : Magnum::Int
	{
		U_MVPMATRIX,
		U_MODELMATRIX,
		U_CAMERA,
		U_RADIUS
	};
	
	const int TEX_LAYER = 0;

};