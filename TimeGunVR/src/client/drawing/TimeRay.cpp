#include "TimeRay.h"

#include "../AssetManager.h"
#include <Magnum/Shaders/Generic.h>
#include <Magnum/MeshTools/Interleave.h>
#include "../objects/base/TimeObject.h"

#include <boost/algorithm/clamp.hpp>
#include <math.h>

TimeRay::TimeRay(Object3D& scene, Magnum::SceneGraph::DrawableGroup3D& drawables)
	: Object3D(&scene), Magnum::SceneGraph::Drawable3D{ *this, nullptr }
{
	this->drawables = &drawables;

	texture = AssetManager::loadTextureDDS("timeray");
	shader.setTexture(texture)
		.setRadius(RADIUS);
}

void TimeRay::setActive(bool active)
{
	if (active == isActive)
		return;

	if (active)
		drawables->add(*this);
	else
		drawables->remove(*this);

	isActive = active;
}

Magnum::Vector3 TimeRay::interpolate(Magnum::Vector3 v1, Magnum::Vector3 v2, float delta)
{
	return v1 + ((v2 - v1) * delta);
}

float offset_midpoint = 0.0f;
void TimeRay::recalculateMesh(Magnum::Vector3 startPoint, Magnum::Vector3 endPoint, Magnum::Matrix4 rayTrafo, Magnum::Float deltaTime)
{
	setTransformation(rayTrafo);

	Magnum::Vector3 ray = endPoint - startPoint;
	float rayLenght = ray.length();
	ray = Magnum::Vector3::zAxis(-rayLenght);

	float amplitude = AMPLITUDE + (AMPLITUDE * 0.1) * std::sin(offset_midpoint);
	Magnum::Vector3 midPoint_1 = ray * 0.2f  + Magnum::Vector3::xAxis(1.f) * amplitude;
	Magnum::Vector3 midPoint_2 = ray * 0.8f + Magnum::Vector3::xAxis(1.f) * amplitude / 32 + Magnum::Vector3::yAxis(1.f) * AMPLITUDE;

	offset_midpoint += 10 * deltaTime;
	if (offset_midpoint >= 2.f * Magnum::Constants::pi())
		offset_midpoint = 0.0f;

	std::vector<Magnum::Vector3> positions;
	std::vector<Magnum::Vector2> textureCoordinates;

	int vertexCount = ceilf(rayLenght/ RESOLUTION) + 1;

	float vertexSpacing = rayLenght / vertexCount;

	float offset_z = 0.f;

	for (int i = 0; i <= vertexCount; i++)
	{
		float delta = 1.f - vertexSpacing * i / rayLenght;

		// calculate vertex position via cubic bezier
		//
		Magnum::Vector3 i1 = interpolate(ray, midPoint_2, delta);
		Magnum::Vector3 i2 = interpolate(midPoint_2, midPoint_1, delta);
		Magnum::Vector3 i3 = interpolate(midPoint_1, Magnum::Vector3(0.f, 0.f, 0.f), delta);

		Magnum::Vector3 pos = interpolate(
			interpolate(i1, i2, delta),
			interpolate(i2, i3, delta),
			delta);
		
		if (positions.size() > 0)
			offset_z += (positions.back() - pos).length();

		pos += Magnum::Vector3(R * std::cos(offset_z), R * std::sin(offset_z), 0);

		Magnum::Vector2 texCoords{ 0.f, (offset_z + uv_offset) * UV_SCALE};

		positions.push_back(pos);
		textureCoordinates.push_back(texCoords);
		if (i == 0 || i == vertexCount)
		{
			positions.push_back(pos);
			textureCoordinates.push_back(texCoords);
		}
	}

	vertices = Magnum::GL::Buffer();
	vertices.setData(Magnum::MeshTools::interleave(positions, textureCoordinates), Magnum::GL::BufferUsage::StaticDraw);

	mesh = Magnum::GL::Mesh();
	mesh.setPrimitive(Magnum::GL::MeshPrimitive::LineStripAdjacency)
		.setCount(positions.size())
		.addVertexBuffer(vertices, 0,
			Magnum::Shaders::Generic3D::Position{},
			Magnum::Shaders::Generic2D::TextureCoordinates{}
		);

	uv_offset += deltaTime * UV_OFFSET_SPEED * (TimeObject::mode == TimeGunMode::Forward ? 1.f : -1.f);
}

void TimeRay::draw(const Magnum::Matrix4& transformationMatrix, Magnum::SceneGraph::Camera3D& camera)
{
	shader.setRadius(RADIUS);

	Magnum::Matrix4 inverseCamMat = camera.cameraMatrix().inverted();

	Magnum::Matrix4 trafo = transformationMatrix;
	trafo = trafo * Magnum::Matrix4::rotationZ(Magnum::Deg(45));

	shader.setModelViewProjectionMatrix(camera.projectionMatrix() * trafo)
		.setCameraPosition(trafo.inverted().transformPoint(DrawablePBR::headPosition));
	mesh.draw(shader);

	trafo = trafo * Magnum::Matrix4::rotationZ(Magnum::Deg(60));

	shader.setModelViewProjectionMatrix(camera.projectionMatrix() * trafo)
		.setCameraPosition(trafo.inverted().transformPoint(DrawablePBR::headPosition));
	mesh.draw(shader);

	trafo = trafo * Magnum::Matrix4::rotationZ(Magnum::Deg(120));

	shader.setModelViewProjectionMatrix(camera.projectionMatrix() * trafo)
		.setCameraPosition(trafo.inverted().transformPoint(DrawablePBR::headPosition));
	mesh.draw(shader);

	trafo = trafo * Magnum::Matrix4::rotationZ(Magnum::Deg(70));

	shader.setModelViewProjectionMatrix(camera.projectionMatrix() * trafo)
		.setCameraPosition(trafo.inverted().transformPoint(DrawablePBR::headPosition));
	mesh.draw(shader);
}

