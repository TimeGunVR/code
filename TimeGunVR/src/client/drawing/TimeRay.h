#pragma once

#include "../stdafx.h"
#include "PolyboardShader.h"

#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>

#include "DrawablePBR.h"
#include "../Input.h"

// TimeRay
//
// generates vertices along a ray & draws them via @PolyboardShader
//
class TimeRay : public Object3D, public Magnum::SceneGraph::Drawable3D
{
public:
	explicit TimeRay(Object3D& scene, Magnum::SceneGraph::DrawableGroup3D& drawables);

	void setActive(bool active);

	// @startPoint & @endPoint in world space
	void recalculateMesh(Magnum::Vector3 startPoint, Magnum::Vector3 endPoint, Magnum::Matrix4 rayTrafo, Magnum::Float deltaTime);

	void debugInputProcessing(Input& input)
	{
		if(input.getKeyDown(KeyCode::Num_1))
			AMPLITUDE--;				   
		if(input.getKeyDown(KeyCode::Num_2))
			AMPLITUDE++;
		if (input.getKeyDown(KeyCode::Num_3))
			UV_SCALE -= 0.005f;
		if(input.getKeyDown(KeyCode::Num_4))
			R -= 0.25f;
		if(input.getKeyDown(KeyCode::Num_5))
			R += 0.25f;
		if (input.getKeyDown(KeyCode::Num_6))
			UV_SCALE += 0.005f;
		if(input.getKeyDown(KeyCode::Num_7))
			RADIUS--;
		if(input.getKeyDown(KeyCode::Num_8))
			RADIUS++;
	}

private:
	void draw(const Magnum::Matrix4& transformationMatrix, Magnum::SceneGraph::Camera3D& camera) override;
	Magnum::Vector3 interpolate(Magnum::Vector3 v1, Magnum::Vector3 v2, float delta);

	Magnum::SceneGraph::DrawableGroup3D* drawables;

	Magnum::GL::Mesh mesh;
	Magnum::GL::Buffer vertices;
	PolyboardShader shader{};

	Magnum::GL::Texture2D texture;

	bool isActive = false;

	float uv_offset = 0.f;

	int RESOLUTION = 1;
	int AMPLITUDE = 20;
	float R = 1.25f;
	float RADIUS = 6.f;
	float UV_OFFSET_SPEED = -150.f;
	float UV_SCALE = 0.005f;
};