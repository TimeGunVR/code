#include "BaseObject.h"
#include "TimeObject.h"
#include "GrabObject.h"
#include "InteractableObject.h"

#include <Magnum/Trade/MeshObjectData3D.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Trade/ObjectData3D.h>

#include <Magnum/MeshTools/Compile.h>

#include "../../Helper.h"
#include "boost/algorithm/string/predicate.hpp"

//#define CAVE_BUILD
//#define PHYSICS_DEBUG

using namespace Magnum::Math::Literals;

BaseObject::BaseObject(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables, 
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material, float mass, 
	std::initializer_list<std::string> names)
	: Object3D{ &parent }, material{ material }
{
	_id = counter;
	counter++;

	this->name = name;

	if (meshes.size() == 0)
		meshes = Magnum::Containers::Array<Magnum::Containers::Optional<Magnum::GL::Mesh>>(importer.mesh3DCount());
	loadObject(parent, drawables, importer, objectID, true);

	btTransform t;
	t.setIdentity();
	createRigidBody(*this, mass, names, t);

	allObjects.insert(std::pair<std::string, BaseObject*>(this->name, this));
}

std::map<std::string, BaseObject*> BaseObject::allObjects;
BaseObject * BaseObject::getObject(std::string name)
{
	return allObjects.at(name);
}

std::map<std::string, BaseObject*> BaseObject::getAllObjects()
{
	return allObjects;
}

void BaseObject::destroyAllObjects()
{
	for (auto it = allObjects.begin(); it != allObjects.end(); it++)
	{
		if (it->second != nullptr)
		{
			it->second->removeRigidBodies();
			delete it->second;
		}
	}
	allObjects.clear();
}

void BaseObject::onSceneLoaded()
{
	for (std::pair<std::string, BaseObject*> kvp : allObjects)
	{
		BaseObject* bObj = kvp.second;

		bObj->_isTimeObject = dynamic_cast<TimeObject*>(bObj) != nullptr;
		bObj->_isGrabObject = dynamic_cast<GrabObject*>(bObj) != nullptr;
		bObj->_isInteractableObject = dynamic_cast<InteractableObject*>(bObj) != nullptr;

		bObj->findObjectDependencies(allObjects);
	}
}

void BaseObject::loadObject(Object3D & parent, Magnum::SceneGraph::DrawableGroup3D & group,
	Magnum::Trade::AbstractImporter & importer, Magnum::UnsignedInt objectID, bool first)
{
	Object3D* object;

	if (first)
		object = this;
	else
		object = new Object3D{ &parent };

	std::unique_ptr<Magnum::Trade::ObjectData3D> objectData = importer.object3D(objectID);
	object->setTransformation(objectData->transformation());
	std::string shortname = Helper::getModelShortname(importer.object3DName(objectID));

	// load mesh & create drawable
	if (objectData->instanceType() == Magnum::Trade::ObjectInstanceType3D::Mesh && objectData->instance() != -1)
	{
		auto& meshObjectData = static_cast<Magnum::Trade::MeshObjectData3D&>(*objectData);

		if (meshObjectData.instance() >= 0)
		{

			meshData.push_back(importer.mesh3D(meshObjectData.instance()));
			if (meshData.back())
			{
				meshes[meshObjectData.instance()] = (Magnum::MeshTools::compile(*meshData.back()));

				btIndexedMesh bulletMesh;
				bulletMesh.m_numTriangles = meshData.back()->indices().size() / 3;
				bulletMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char *>(meshData.back()->indices().data());
				bulletMesh.m_triangleIndexStride = 3 * sizeof(Magnum::UnsignedInt);
				bulletMesh.m_numVertices = meshData.back()->positions(0).size();
				bulletMesh.m_vertexBase = reinterpret_cast<const unsigned char *>(meshData.back()->positions(0).data());
				bulletMesh.m_vertexStride = sizeof(Magnum::Vector3);
				bulletMesh.m_indexType = PHY_INTEGER;
				bulletMesh.m_vertexType = PHY_FLOAT;
				bulletMeshes.insert(std::pair<Object3D*, btIndexedMesh>(object, bulletMesh));

				drawable = new DrawablePBR{ *object, *meshes[meshObjectData.instance()], &group, material };
			}
		}
	}

	objects.insert(std::pair<std::string, Object3D*>(shortname, object));
	names.insert(std::pair<Object3D*, std::string>(object, shortname));

	for (Magnum::UnsignedInt id : objectData->children())
		loadObject(*object, group, importer, id);
}

Magnum::Containers::Array<Magnum::Containers::Optional<Magnum::GL::Mesh>> BaseObject::meshes;

void BaseObject::createRigidBody(Object3D& object, float mass, std::initializer_list<std::string> names, btTransform startTrans) 
{
	btCompoundShape* compoundShape = new btCompoundShape();
	Magnum::Vector3 translation = object.transformationMatrix().translation();
	createShape(object, compoundShape, names, true, btVector3(translation), btVector3(translation) + startTrans.getOrigin());

	btTransform t;
	t.setIdentity();
	t.setOrigin(btVector3(translation) + startTrans.getOrigin());

	/* Calculate inertia so the object reacts as it should with rotation and everything */
	btVector3 bInertia(0.0f, 0.0f, 0.0f);
	if (mass != 0.0f) {
		compoundShape->calculateLocalInertia(mass, bInertia);
	}


	/* Bullet rigid body setup */
	auto* motionState = new Magnum::BulletIntegration::MotionState{ object };
	auto* bRigidBody = new btRigidBody{ btRigidBody::btRigidBodyConstructionInfo{
		mass, &motionState->btMotionState(), compoundShape, bInertia} };
	bRigidBody->setUserPointer(this);

	bRigidBody->setWorldTransform(t);
	bRigidBody->getMotionState()->setWorldTransform(t);

#ifndef CAVE_BUILD
	bRigidBody->setUserIndex2(_id);
#endif // !CAVE_BUILD
	
	short collisionFilterGroup;
	short collisionFilterMask;

	if (mass == 0.0f) {
#ifdef PHYSICS_DEBUG
		bRigidBody->setCollisionFlags(2); //2
		bRigidBody->forceActivationState(DISABLE_DEACTIVATION);
#else
		bRigidBody->setCollisionFlags(1); //2
		bRigidBody->forceActivationState(ISLAND_SLEEPING);
#endif
		collisionFilterGroup = short(btBroadphaseProxy::StaticFilter);
		collisionFilterMask = short(btBroadphaseProxy::AllFilter ^ btBroadphaseProxy::StaticFilter);
	}
	else {
		bRigidBody->forceActivationState(DISABLE_DEACTIVATION);
		collisionFilterGroup = short(btBroadphaseProxy::DefaultFilter);
		collisionFilterMask = short(btBroadphaseProxy::AllFilter);
	}

#ifdef PHYSICS_DEBUG
	world->addRigidBody(bRigidBody, collisionFilterGroup, collisionFilterMask);
#else
	world->addRigidBody(bRigidBody);
#endif

	rigidBodies.insert(std::pair<std::string, btRigidBody*>(this->names.at(&object), bRigidBody));
}

void BaseObject::createShape(Object3D& object, btCompoundShape* compound, std::initializer_list<std::string> names, bool first, btVector3 baseShapePosition, btVector3 baseBodyPosition) {
	btTransform t;
	t.setIdentity();

	if (!first) {
		Magnum::Vector3 translation = object.absoluteTransformationMatrix().translation();
		t.setOrigin(btVector3(translation) - baseShapePosition);
	}
	else {
		btCollisionShape* shape = nullptr;
		if (this->name == "Floor") {
			name  += std::to_string(_id);
			shape = new btBoxShape(btVector3(135.0f, 50.0f, 135.0f));
			t.setOrigin(btVector3(0.f, -50.0f, -135.0f));
		}
		else if (this->name == "Ceiling") {
			name += std::to_string(_id);
			shape = new btBoxShape(btVector3(135.0f, 50.0f, 135.0f));
			t.setOrigin(btVector3(0.f, 50.0f, -135.0f));
		}
		else if (this->name == "WallN") {
			name += std::to_string(_id);
			shape = new btBoxShape(btVector3(135.0f, 135.0f, 50.0f));
			t.setOrigin(t.getOrigin() + btVector3(0.f, 135.0f, -50.0f));
		}
		else if (this->name == "WallE") {
			name += std::to_string(_id);
			shape = new btBoxShape(btVector3(50.0f, 135.0f, 135.0f));
			t.setOrigin(t.getOrigin() + btVector3(50.0f, 135.0f, 0.f));
		}
		else if (this->name == "WallS") {
			name += std::to_string(_id);
			shape = new btBoxShape(btVector3(135.0f, 135.0f, 50.0f));
			t.setOrigin(t.getOrigin() + btVector3(0.f, 135.0f, 50.0f));
		}
		else if (this->name == "WallW") {
			name += std::to_string(_id);
			shape = new btBoxShape(btVector3(50.0f, 135.0f, 135.0f));
			t.setOrigin(t.getOrigin() + btVector3(-50.0f, 135.0f, 0.f));
		}
		else if (this->name == "FloorThick")
			name += std::to_string(_id);

		if (shape)
			compound->addChildShape(t, shape);
		t.setIdentity();
	}

	btCollisionShape* shape = nullptr;
	btTriangleIndexVertexArray* tivArray = new btTriangleIndexVertexArray();
	tivArray->addIndexedMesh(bulletMeshes.at(&object), PHY_INTEGER);
	if (this->name == Constant::SN_SAFE || this->name == Constant::SN_COMPUTER
		|| this->name == Constant::SN_PLATFORM || this->name == Constant::SN_BED
		|| this->name == Constant::SN_FLOORTHICK + std::to_string(_id))
	{
		// exact shape, but worse performance
		shape = new btBvhTriangleMeshShape(tivArray, true);
	}
	else {
		// convex hull, but better performance
		shape = new btConvexTriangleMeshShape(tivArray, true);
	} // btConvexHullShape can be even more performant

	compound->addChildShape(t, shape);

	Object3D* child = object.children().first();
	while (child)
	{
		Object3D* sibling = child->nextSibling();
		auto l = { this->names.at(child) };
		if (boost::algorithm::contains(names, l)) {
			t.setIdentity();
			t.setOrigin(baseBodyPosition);
			child->setParent(this->parent());
			createRigidBody(*child, 10, names, t);
		}
		else {
			createShape(*child, compound, names, false, baseShapePosition, baseBodyPosition);
		}
		child = sibling;
	}
}

void BaseObject::removeRigidBodies()
{
	for (std::map<std::string, btRigidBody*>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++)
	{
		world->removeRigidBody(it->second);
		delete it->second;
	}
	rigidBodies.clear();
}

btDiscreteDynamicsWorld* BaseObject::world;
Magnum::UnsignedInt BaseObject::counter = 1;