#pragma once
#include "../../stdafx.h"

#include "../../drawing/DrawablePBR.h"
#include "../../drawing/PBR_Shader.h"
#include "../../drawing/MaterialPBR.h"

#include <Magnum/GL/Mesh.h>
#include <Magnum/Trade/AbstractImporter.h>

#include <Magnum/Trade/MeshData3D.h>

#include <iostream>

// BaseObject
//
// Base class for all objects in the game
//
class BaseObject : public Object3D
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	// @mass: mass of the object (for RigidBody)
	// @names: names of the child object which have to get a additional RigidBody
	explicit BaseObject(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables, Magnum::Trade::AbstractImporter& importer,
		Magnum::UnsignedInt objectID, MaterialPBR material, float mass, std::initializer_list<std::string> names = {});

	bool isTimeObject() { return _isTimeObject; }
	bool isGrabObject() { return _isGrabObject; }
	bool isInteractableObject() { return _isInteractableObject; }

	std::string getName() { return name; };
	Magnum::UnsignedInt getId() { return _id; };
	DrawablePBR& getDrawable() { return *drawable; }
	std::map<std::string, btRigidBody*> getRigidBodies() { return rigidBodies; }

	void removeRigidBodies();

	// Called in @AssetManager::loadScene() after loading all objects 
	// to find dependencies between objects & set type flags
	static void onSceneLoaded();

	// find a BaseObject by name
	static BaseObject* getObject(std::string name);
	// get map of all loaded BaseObjects
	static std::map<std::string, BaseObject*> getAllObjects();
	// destroy all loaded BaseObjects
	static void destroyAllObjects();

	static btDiscreteDynamicsWorld* world;

protected:
	// Loads the model of the object
	void loadObject(Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& group,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, bool first = false);

	// Creates the RigidBody of the object
	void createRigidBody(Object3D& object, float mass, std::initializer_list<std::string> names, btTransform startTrans);

	// Called in createRigidBody() and creates the shape for the object according to its model
	// recursive function (shapes are stored in a compoundShape)
	void createShape(Object3D& object, btCompoundShape* compound, std::initializer_list<std::string> name, bool first, btVector3 basePosition, btVector3 baseBodyPosition);

	// Called in AssetManager after loading all objects via static @BaseObject::onSceneLoaded; 
	// can be overriden in implementations for specific objects to find other objects in the scene
	virtual void findObjectDependencies(std::map<std::string, BaseObject*> &objects) {}

	// type flags
	bool _isTimeObject = false;   // is TimeObject
	bool _isGrabObject = false;	  // is GrabObject
	bool _isInteractableObject = false; // is InteractableObject

	std::map<Object3D*, btIndexedMesh> bulletMeshes;
	std::map<std::string, btRigidBody*> rigidBodies;
	std::map<std::string, Object3D*> objects;
	std::map<Object3D*, std::string> names;

	std::vector<Magnum::Containers::Optional<Magnum::Trade::MeshData3D>> meshData;
	MaterialPBR material;
	DrawablePBR* drawable;

	std::string name;
	Magnum::UnsignedInt _id = 0;

	static Magnum::UnsignedInt counter;
	// all currently loaded base objects
	static std::map<std::string, BaseObject*> allObjects;
	static Magnum::Containers::Array<Magnum::Containers::Optional<Magnum::GL::Mesh>> meshes;
};