#include "GrabObject.h"

//#define CAVE_BUILD

GrabObject::GrabObject(std::string name)
{
	_grabbed = false;
	constraintsWorld = world;

	allObjects.insert(std::pair<std::string, GrabObject*>(name, this));
}

std::map<std::string, GrabObject*> GrabObject::allObjects;
GrabObject * GrabObject::getObject(std::string name)
{
	return allObjects.at(name);
}

std::map<std::string, GrabObject*> GrabObject::getAllObjects()
{
	return allObjects;
}

void GrabObject::clearAllObjects()
{
	allObjects.clear();
}

void GrabObject::grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed)
{
	if (somethingIsGrabbed)
		return;

	if (!_grabbed) {
		_grabbed = true;
		somethingIsGrabbed = true;

		if(!fixed){
			constraint = new btPoint2PointConstraint(*hand, *object, transformHand.getOrigin(), transformObj.getOrigin());
		}
		else {
			constraint = new btFixedConstraint(*hand, *object, transformHand, transformObj);
		}

		constraintsWorld->addConstraint(constraint, true);
		constraint->setDbgDrawSize(0.2f);

	}
}

void GrabObject::ungrab() 
{
	if (_grabbed) {
		_grabbed = false;
		somethingIsGrabbed = false;
		constraintsWorld->removeConstraint(constraint);
	}
}

btDiscreteDynamicsWorld* GrabObject::world;
bool GrabObject::somethingIsGrabbed = false;