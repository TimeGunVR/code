#pragma once
#include "../../stdafx.h"

#include <iostream>

// GrabObject
//
// objects that can be picked up or moved by player
//
class GrabObject
{
public:
	explicit GrabObject(std::string name);

	// find a GrabObject by name
	static GrabObject* getObject(std::string name);
	// get map of all loaded GrabObjects
	static std::map<std::string, GrabObject*> getAllObjects();
	// only clears the object map, call BaseObject::destroyAllObjects for deletion
	static void clearAllObjects();

	// generell function for the grab interaction
	// creates a constraint for the physics world
	virtual void grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed);
	// ungrab function removes the constraint 
	void ungrab();

	static btDiscreteDynamicsWorld* world;

private:
	bool _grabbed;
	btDiscreteDynamicsWorld* constraintsWorld;
	btTypedConstraint* constraint;

	// all currently loaded grabable objects
	static std::map<std::string, GrabObject*> allObjects;
	static bool somethingIsGrabbed;
};