#include "InteractableObject.h"

InteractableObject::InteractableObject(std::string name)
{
	_triggered = false;

	allObjects.insert(std::pair<std::string, InteractableObject*>(name, this));
}

std::map<std::string, InteractableObject*> InteractableObject::allObjects;
InteractableObject * InteractableObject::getObject(std::string name)
{
	return allObjects.at(name);
}

std::map<std::string, InteractableObject*> InteractableObject::getAllObjects()
{
	return allObjects;
}

void InteractableObject::clearAllObjects()
{
	allObjects.clear();
}

void InteractableObject::setTriggered(bool triggered, const btCollisionObject* collision) {
	if (!_triggered && triggered)
		this->onTriggerEnter(collision);
	else if (_triggered && !triggered)
		this->onTriggerExit();

	_triggered = triggered;
}

bool InteractableObject::getTriggered() {
	return _triggered;
}

void InteractableObject::onTriggerEnter(const btCollisionObject* collision) {}

void InteractableObject::onTriggerExit() {}