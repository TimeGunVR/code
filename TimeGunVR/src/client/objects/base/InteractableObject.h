#pragma once
#include "../../stdafx.h"

#include <iostream>

// InteractableObject
//
// objects the player can interact with by touching them
//
class InteractableObject
{
public:
	explicit InteractableObject(std::string name);

	// find a InteractableObject by name
	static InteractableObject* getObject(std::string name);
	// get map of all loaded InteractableObjects
	static std::map<std::string, InteractableObject*> getAllObjects();
	// only clears the object map, call BaseObject::destroyAllObjects for deletion
	static void clearAllObjects();

	void setTriggered(bool triggered, const btCollisionObject* collision);
	bool getTriggered();

protected:
	// called if triggered changed from false to true (in setTriggered())
	virtual void onTriggerEnter(const btCollisionObject* collision);
	// called if triggered changed from true to false (in setTriggered())
	virtual void onTriggerExit();

private:
	bool _triggered;

	// all currently loaded interactable objects
	static std::map<std::string, InteractableObject*> allObjects;
};