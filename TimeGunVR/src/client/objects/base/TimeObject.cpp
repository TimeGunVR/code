#include "TimeObject.h"
#include <boost/algorithm/clamp.hpp>

TimeObject::TimeObject(std::string name)
{
	_aimed = false;
	allObjects.insert(std::pair<std::string, TimeObject*>(name, this));
}

std::map<std::string, TimeObject*> TimeObject::allObjects;
TimeObject * TimeObject::getObject(std::string name)
{
	return allObjects.at(name);
}

std::map<std::string, TimeObject*> TimeObject::getAllObjects()
{
	return allObjects;
}

void TimeObject::clearAllObjects()
{
	allObjects.clear();
}


TimeObject* TimeObject::currentTarget;
void TimeObject::setTarget(TimeObject * target)
{
	if (target != currentTarget)
	{
		if (target)
			target->onTargeted();
		if (currentTarget)
			currentTarget->onUntargeted();

		currentTarget = target;
	}
}

void TimeObject::_stepAnimation(Magnum::Float delta)
{
	if (mode == TimeGunMode::Reverse)
		delta *= -1;

	animationTime += delta;

	if (animationTime < 0 || animationTime > animationDuration)
		animationTime = boost::algorithm::clamp(animationTime, 0.f, animationDuration);

	updateTimeAnimation();
}
TimeGunMode TimeObject::mode;
