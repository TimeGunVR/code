#pragma once
#include "../../stdafx.h"

#include <iostream>
#include <Magnum/Trade/AbstractImporter.h>

enum TimeGunMode { Forward, Reverse };

// TimeObject
//
// objects that should be affected by the time gun;
// animation can run forward or reveresed depending on @TimeObject::mode
//
class TimeObject
{
public:
	explicit TimeObject(std::string name);

	// find a TimeObject by name
	static TimeObject* getObject(std::string name);	
	// get map of all loaded TimeObjects
	static std::map<std::string, TimeObject*> getAllObjects();
	// only clears the object map, call BaseObject::destroyAllObjects for deletion
	static void clearAllObjects();

	// set the time object which is currently targeted by the time gun;
	// if no object is targeted pass nullptr as @target
	static void setTarget(TimeObject* target);

	// updates animation of the object currently targeted by the time gun;
	// @delta last frame time
	static void stepAnimation(Magnum::Float delta) 
	{ 
		if (currentTarget) 
			currentTarget->_stepAnimation(delta); };

	static TimeGunMode mode;

protected:
	bool _aimed = false;
	float animationTime = 0.0f;
	float animationDuration = 1.0f;

	bool isTarget() { return this == currentTarget; }

	// called in @animationStep() 
	// implement in subclasses to update animation state according to @animationTime & @animationDuration
	virtual void updateTimeAnimation() = 0;

	// called once when object is target by time gun
	virtual void onTargeted(){}
	// called once when object is not target anymore by time gun
	virtual void onUntargeted() {}

private:
	void _stepAnimation(Magnum::Float delta);

	// object that is currently targeted by the time gun
	static TimeObject* currentTarget;
	// all currently loaded time objects
	static std::map<std::string, TimeObject*> allObjects;
};