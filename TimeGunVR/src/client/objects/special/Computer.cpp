#include "Computer.h"
#include "../../Constants.h"

Computer::Computer(std::string name, Object3D & parent, Magnum::SceneGraph::DrawableGroup3D & drawables,
	Magnum::Trade::AbstractImporter & importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 , { "FloppyDrive" } }, TimeObject{ name }
{
	animationDuration = 5.0f;
	this->material.setEmissiveFactor(EMISSIVE_COLOR_NO_FLOPPY);

	btRigidBody* computer = rigidBodies.at("Computer");
	computer->setUserIndex(0);
	btRigidBody* drive = rigidBodies.at("FloppyDrive");
	drive->setUserIndex(1);
	drive->setCollisionFlags(4);
	btFixedConstraint *constraint = new btFixedConstraint(
		*computer, *drive,
		btTransform(btQuaternion::getIdentity(), drive->getCenterOfMassTransform().getOrigin() - computer->getCenterOfMassTransform().getOrigin()),
		btTransform(btQuaternion::getIdentity(), { 0.0f,  0.0f, 0.0f })
	);
	world->addConstraint(constraint, true);
	constraint->setDbgDrawSize(0.2f);

}

void Computer::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	teleporter = (Teleporter*)objects.at(Constant::SN_TELEPORTER);
	screen = (ComputerScreen*)objects.at(Constant::SN_COMPUTER_SCREEN);
}

void Computer::onFloppyInserted()
{
	material.setEmissiveFactor(EMISSIVE_COLOR_WITH_FLOPPY);
	screen->nextState();
}

void Computer::updateTimeAnimation()
{
	if (screen->getState() != ComputerScreen::State::ACTIVATING_TELEPORTER)
	{
		animationTime = 0.f;
		return;
	}

	if (animationDuration == animationTime) {
		screen->nextState();
		teleporter->activate();
	}
}

void Computer::onTargeted()
{
	if(mode == TimeGunMode::Forward)
		screen->animationSpeed = SCREEN_ANIMATION_SPEED_UP;
}

void Computer::onUntargeted()
{
	screen->animationSpeed = 1.0f;
}
