#pragma once

#include "../../stdafx.h"
#include "../base/BaseObject.h"
#include "../base/TimeObject.h"
#include "Teleporter.h"
#include "ComputerScreen.h"

// Computer with screen
//
// @TimeObject: speeds up the loading process of the teleporter activation process
//
// when the loading in done, the teleporter is activated
//
class Computer : public BaseObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	explicit Computer(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;

	// Called if FloppyDisk is inserted into the Computer (called in FloppyDisk)
	void onFloppyInserted();

protected:
	void onTargeted() override;
	void onUntargeted() override;

private:
	Teleporter * teleporter;
	ComputerScreen* screen;

	virtual void updateTimeAnimation() override;

	const float SCREEN_ANIMATION_SPEED_UP = 32.f;

	const Magnum::Vector3 EMISSIVE_COLOR_WITH_FLOPPY { 0, 1, 0 };
	const Magnum::Vector3 EMISSIVE_COLOR_NO_FLOPPY { 1, 0, 0 };
};