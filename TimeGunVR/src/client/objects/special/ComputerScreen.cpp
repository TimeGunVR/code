#include "ComputerScreen.h"
#include "../../Constants.h"

using AnimationState = Magnum::SceneGraph::AnimationState;

ComputerScreen::ComputerScreen(std::string name, Object3D & parent, Magnum::SceneGraph::DrawableGroup3D & drawables, Magnum::SceneGraph::AnimableGroup3D & animables,
	Magnum::Trade::AbstractImporter & importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }, Magnum::SceneGraph::Animable3D{ *this, &animables }
{
	setState(AnimationState::Running);
}

void ComputerScreen::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	// set user pointer to computer, so time gun works also when pointed at screen
	rigidBodies.begin()->second->setUserPointer(objects.at(Constant::SN_COMPUTER));
}

void ComputerScreen::animationStep(Magnum::Float time, Magnum::Float delta)
{
	frameTime += delta;

	if (frameTime > timePerFrame / animationSpeed)
	{
		frameTime = 0.f;
		Magnum::Vector2 offset = material.uvOffset();

		switch (state)
		{
			// show texture with error message & loading sign (4 frames, repeating)
			case MISSING_PROTOCOL:
				if (offset.x() == 0.75f)
					material.setUVOffset(Magnum::Vector2(0.f, 0.0f));
				else
					material.setUVOffset(Magnum::Vector2(offset.x() + 0.25f, 0.0f));
				break;

			// show texture with some loading stuff (7 frames, once)
			case LOADING_PROTOCOL:
				if (offset.x() == 0.5f)
				{
					if (offset.y() == -0.5f)
						nextState();
					else
						material.setUVOffset(Magnum::Vector2{ offset.x() + 0.25f, offset.y() });
				}
				else if(offset.x() == 0.75f)
					material.setUVOffset(Magnum::Vector2{ 0.f, -0.5f });
				else
					material.setUVOffset(Magnum::Vector2{ offset.x() + 0.25f, offset.y() });
				break;

			// show texture with stuck loading screen (4 frames, repeating)
			case ACTIVATING_TELEPORTER:
				if(offset.y() == -0.5f)
					material.setUVOffset(Magnum::Vector2{ 0.f, -0.75f });
				else if (offset.x() == 0.5f)
					material.setUVOffset(Magnum::Vector2{ 0.75f, -0.5f });
				else
					material.setUVOffset(Magnum::Vector2{ offset.x() + 0.25f, offset.y() });
				break;

			// show last texture with success message (1 frame, staying forever)
			case ACTIVATION_FINISHED:
				setState(AnimationState::Stopped);
				break;
		}
	}

}
