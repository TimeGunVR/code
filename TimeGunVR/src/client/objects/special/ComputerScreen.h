#pragma once

#include "../../stdafx.h"
#include "../base/BaseObject.h"
#include <Magnum/SceneGraph/Animable.h>

// Computer screen
//
class ComputerScreen : public BaseObject, public Magnum::SceneGraph::Animable3D
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @animables: animable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	explicit ComputerScreen(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables, Magnum::SceneGraph::AnimableGroup3D& animables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	enum State { MISSING_PROTOCOL, LOADING_PROTOCOL, ACTIVATING_TELEPORTER, ACTIVATION_FINISHED };

	void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;

	State getState() { return state; }
	void nextState() 
	{ 
		if (state == ACTIVATION_FINISHED)
			return;

		state = (State)(((int)state) + 1);
		frameTime = 0.f;

		if (state == LOADING_PROTOCOL)
			material.setUVOffset(Magnum::Vector2{0.f, -0.25f});
		else if (state == ACTIVATING_TELEPORTER)
			material.setUVOffset(Magnum::Vector2{ 0.75f, -0.5f });
		else if (state == ACTIVATION_FINISHED)
			material.setUVOffset(Magnum::Vector2{ 0.75f, -0.75f });
	}
	float animationSpeed = 1.f;

private:
	State state = MISSING_PROTOCOL;
	void animationStep(Magnum::Float time, Magnum::Float delta) override;

	float frameTime = 0.f;
	const float timePerFrame = 1.f; // amount of time that each animation frame is shown
};