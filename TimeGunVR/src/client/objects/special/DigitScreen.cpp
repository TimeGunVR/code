#include "DigitScreen.h"

DigitScreen::DigitScreen(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID,
	MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }
{
}

void DigitScreen::setDigit(int digit)
{
	material.setUVOffset(Magnum::Vector2{ (digit % 4) / 4.f, -(digit / 4) / 4.f });
}
