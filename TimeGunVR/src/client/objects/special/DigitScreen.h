#pragma once

#include "../../stdafx.h"
#include "../base/BaseObject.h"

// Digit screen of the Keypad
//
class DigitScreen : public BaseObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	DigitScreen(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	// set the currently displayed digit 
	// @digit: [1-9] displays correspoding digit, 0 is empty screen
	void setDigit(int digit);

	void setEmissiveFactor(Magnum::Vector3 factor) { material.setEmissiveFactor(factor); }
};

