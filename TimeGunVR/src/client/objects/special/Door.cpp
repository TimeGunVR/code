#include "Door.h"

Door::Door(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID,
	MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }, TimeObject{ name }
{
	lock();

	animationDuration = 12.0f;
	animationTime = animationDuration;
}

void Door::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	door1 = objects.at("door1")->getRigidBodies().begin()->second;
	door2 = objects.at("door2")->getRigidBodies().begin()->second;

	door1->setCollisionFlags(6);
	door2->setCollisionFlags(6);
}

void Door::lock()
{
	material.setEmissiveFactor(Magnum::Color3::red());
}

void Door::unlock()
{
	material.setEmissiveFactor(Magnum::Color3::green());
}

void Door::updateTimeAnimation()
{
	if (animationTime > 10.0f)
		lock();
	else 
	{
		unlock();

		float xOffset = Magnum::Math::lerp(slideLength, 0.f, animationTime / 10.0f);
		offsetRigidBody(door1, -xOffset);
		offsetRigidBody(door2, xOffset);
	}
}

void Door::offsetRigidBody(btRigidBody* rb, float xOffset)
{
	btVector3 origin = rb->getWorldTransform().getOrigin();
	origin.setX(xOffset);

	btTransform t;
	t.setIdentity();
	t.setOrigin(origin);
	rb->setWorldTransform(t);
	rb->getMotionState()->setWorldTransform(t);

	BaseObject::world->removeRigidBody(rb);
	BaseObject::world->addRigidBody(rb);
}
