#pragma 

#include "../../stdafx.h"
#include "../base/BaseObject.h"
#include "../base/TimeObject.h"

// Slide Door
//
// @TimeObject: door unlocks and opens
//
class Door : public BaseObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	Door(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;

private:
	// sets the emission color to red
	void lock();
	// sets the emission color to green
	void unlock();

	const float slideLength = 45.0f;

	btRigidBody *door1, *door2;

	// Inherited via TimeObject
	virtual void updateTimeAnimation() override;
	void offsetRigidBody(btRigidBody * rb, float xOffset);
};

