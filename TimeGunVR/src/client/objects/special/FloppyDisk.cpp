#include "FloppyDisk.h"
#include "../../Constants.h"

FloppyDisk::FloppyDisk(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0.005f}, GrabObject{ name }
{
}

void FloppyDisk::findObjectDependencies(std::map<std::string, BaseObject*> &objects)
{
	computer = (Computer*)objects.at(Constant::SN_COMPUTER);
}

void FloppyDisk::grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) {
	if (!canBeGrabed)
		return;
	GrabObject::grab(hand, rigidBodies.at("FloppyDisk"), btTransform(btQuaternion::getIdentity(), { 0.0f, 0.0f, 0.0f }), btTransform(btQuaternion::getIdentity(), { 0.0f, 0.0f, 0.0f }), true);
}

void FloppyDisk::fixFloppy(const btCollisionObject* drive) {
	GrabObject::ungrab();
	canBeGrabed = false;
	btRigidBody* computer = ((BaseObject*)(drive->getUserPointer()))->getRigidBodies().at("Computer");
	btRigidBody* floppy = rigidBodies.at("FloppyDisk");
	floppy->setCollisionFlags(4);
	btVector3 anchor = btVector3(-1.1842f, 3.5f, 9.4788f);
	btFixedConstraint *constraint = new btFixedConstraint(
		*computer, *floppy,
		btTransform(btQuaternion::getIdentity(), anchor),
		btTransform(btQuaternion::getIdentity(), { 0.0f,  0.0f, 0.0f })
	);
	BaseObject::world->addConstraint(constraint, true);
	constraint->setDbgDrawSize(0.5f);

	this->computer->onFloppyInserted();
}

