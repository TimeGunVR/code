#pragma once
#include "../../stdafx.h"
#include "../base/GrabObject.h"
#include "../base/BaseObject.h"
#include "../../drawing/MaterialPBR.h"
#include "Computer.h"

// Floppy Disk
//
// @GrabObject: can be grabed and should be inserted into the Computer
//
// when inerted the Computer starts loading
//
class FloppyDisk : public BaseObject, public GrabObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	explicit FloppyDisk(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void findObjectDependencies(std::map<std::string, BaseObject*>& objects);

	virtual void grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) override;

	// fixes the floppy to a fix position in the Computer with a btFixedConstraint
	void fixFloppy(const btCollisionObject* drive);

	void setCanBeGrabed(bool can) { canBeGrabed = can; }
	bool getCanBeGrabed() { return canBeGrabed; }
private:
	bool canBeGrabed = false;

	Computer* computer;
};