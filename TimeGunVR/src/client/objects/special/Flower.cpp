#include "Flower.h"

Flower::Flower(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }, TimeObject { name }
{
	drawables.remove(*drawable);
	rigidBodies.begin()->second->setCollisionFlags(6);

	this->drawables = &drawables;
}

void Flower::plant()
{
	if (planted)
		return;

	planted = true;
	drawables->add(*drawable);
	refreshScale();
}

void Flower::refreshScale()
{
	if (!planted)
	{
		animationTime = 0.f;
		return;
	}

	float scale = Magnum::Math::lerp(MIN_FLOWER_SCALE, MAX_FLOWER_SCALE, animationTime / animationDuration);

	Magnum::Vector3 flwTrans = transformationMatrix().translation();
	resetTransformation();
	setTransformation(Magnum::Matrix4::scaling(Magnum::Vector3(scale)));
	translate(flwTrans);
}

void Flower::updateTimeAnimation()
{
	refreshScale();
}
