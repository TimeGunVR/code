#pragma once

#include "../../stdafx.h"
#include "../base/BaseObject.h"
#include "../base/TimeObject.h"

// Flower
//
// @TimeObject: "grows" flower by scaling 
//
class Flower : public BaseObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	Flower(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void plant();
	void refreshScale();

private:
	bool planted = false;
	Magnum::SceneGraph::DrawableGroup3D* drawables;

	const float MIN_FLOWER_SCALE = 0.1f;
	const float MAX_FLOWER_SCALE = 1.f;

	virtual void updateTimeAnimation() override;
};

