#include "Keypad.h"
#include "../../Constants.h"

Keypad::Keypad(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0,
		{ Constant::PS_KEYPAD_BTN + "_1", Constant::PS_KEYPAD_BTN + "_2", Constant::PS_KEYPAD_BTN + "_3",
		  Constant::PS_KEYPAD_BTN + "_4", Constant::PS_KEYPAD_BTN + "_5", Constant::PS_KEYPAD_BTN + "_6",
		  Constant::PS_KEYPAD_BTN + "_7", Constant::PS_KEYPAD_BTN + "_8", Constant::PS_KEYPAD_BTN + "_9"}},
	InteractableObject{ name }, TimeObject{ name }
{
	btRigidBody* keypad = rigidBodies.at(Constant::SN_KEYPAD);
	keypad->setUserIndex(0);

	for (int i = 1; i < 10; i++) {
		btRigidBody* bt = rigidBodies.at(Constant::PS_KEYPAD_BTN + "_" + std::to_string(i));
		bt->setCollisionFlags(4);
		bt->setUserIndex(i);
		btFixedConstraint *constraint = new btFixedConstraint(
			*keypad, *bt,
			btTransform(btQuaternion::getIdentity(), bt->getCenterOfMassTransform().getOrigin() - keypad->getCenterOfMassTransform().getOrigin()),
			btTransform(btQuaternion::getIdentity(), { 0.0f,  0.0f, 0.0f })
		);
		world->addConstraint(constraint, true);
		constraint->setDbgDrawSize(0.2f);
	}
	this->material.setEmissiveFactor(Magnum::Vector3{});
	animationDuration = 30.0f;
}

void Keypad::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	for (int i = 0; i < 4; i++) 
		screen[i] = (DigitScreen*)objects.at(Constant::PS_DIGITSCREEN + "_" + std::to_string(i+1));

	teleporter = (Teleporter*)objects.at(Constant::SN_TELEPORTER);

	setScreenEmissiveFactor(EMISSION_FACTOR_WRONG);
}

// enters according digit & checks if code is correct if 4 digits were entered
void Keypad::onTriggerEnter(const btCollisionObject* collision)
{
	if (collision->getUserIndex() == 0)
		return;

	if (digitIndex == 0)
	{
		screen[1]->setDigit(0);
		screen[2]->setDigit(0);
		screen[3]->setDigit(0);
		setScreenEmissiveFactor(EMISSION_FACTOR_NORMAL);
	}
	else if (digitIndex > 3)
		return;

	screen[digitIndex]->setDigit(collision->getUserIndex());
	code += std::pow(10, 3 - digitIndex) * collision->getUserIndex();
	digitIndex++;

	if (digitIndex > 3)
	{
		if (code == Constant::ROOM_1_KEYPAD_CODE)
		{
			setScreenEmissiveFactor(EMISSION_FACTOR_CORRECT);
			teleporter->activate();
		}
		else
		{
			setScreenEmissiveFactor(EMISSION_FACTOR_WRONG);
			digitIndex = 0;
			code = 0;
		}
	}
}

void Keypad::setScreenEmissiveFactor(Magnum::Vector3 factor)
{
	screen[0]->setEmissiveFactor(factor);
	screen[1]->setEmissiveFactor(factor);
	screen[2]->setEmissiveFactor(factor);
	screen[3]->setEmissiveFactor(factor);
}

void Keypad::updateTimeAnimation()
{
	material.setEmissiveFactor(Magnum::Math::lerp(Magnum::Vector3(0),
		Magnum::Vector3(1), animationTime / animationDuration));
}
