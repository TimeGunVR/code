#pragma once

#include "../../stdafx.h"
#include "../base/TimeObject.h"
#include "../base/InteractableObject.h"
#include "DigitScreen.h"
#include "Teleporter.h"

// Keypad with screen
//
// @InteractableObject: buttons can be pressed to enter a code
// @TimeObject: fingerprints appear on buttons to indicate correct digits
//
// when the correct key is entered, the teleporter is activated
//
class Keypad : public BaseObject, public InteractableObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	Keypad(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;

private:
	void onTriggerEnter(const btCollisionObject* collision) override;

	void setScreenEmissiveFactor(Magnum::Vector3 factor);

	// automatically called in base class
	virtual void updateTimeAnimation() override;

	DigitScreen* screen[4];
	// which digit must be set next
	int digitIndex = 0;

	// Code that has been entered
	int code = 0;

	Teleporter* teleporter;

	const Magnum::Vector3 EMISSION_FACTOR_NORMAL{ 1, 1, 1 };
	const Magnum::Vector3 EMISSION_FACTOR_CORRECT{ 0 , 2, 0 };
	const Magnum::Vector3 EMISSION_FACTOR_WRONG{ 3, 0, 0 };
};

