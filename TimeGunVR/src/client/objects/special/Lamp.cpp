#include "Lamp.h"
using AnimationState = Magnum::SceneGraph::AnimationState;

Lamp::Lamp(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables, Magnum::SceneGraph::AnimableGroup3D& animables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }, TimeObject{ name }
{
	animationDuration = 5.f;

	flickerObject = new FlickerObject(*this, animables);
	flickerObject->setState(AnimationState::Running);
	lampOnModifier = 1.f;
	lampOffModifier = animationDuration;

	animationTime = animationDuration;
}

void Lamp::updateTimeAnimation()
{
	if (animationTime == 0.f)
	{
		turnLightOn();
		flickerObject->setState(AnimationState::Paused);
	}
	else
		flickerObject->setState(AnimationState::Running);

	lampOnModifier = animationDuration - animationTime + 1.0f;
	lampOffModifier = animationTime;
}

void Lamp::turnLightOn()
{
	material.setEmissiveFactor(EMISSIVE_FACTOR_ON);
	DrawablePBR::lightIntensityModifier = 1.f;
	isOn = true;
}

void Lamp::turnLightOff()
{
	material.setEmissiveFactor(EMISSIVE_FACTOR_OFF);
	DrawablePBR::lightIntensityModifier = 0.0f;
	isOn = false;
}
