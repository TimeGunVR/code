#pragma once

#include "../../stdafx.h"
#include "../base/../base/BaseObject.h"
#include "../base/../base/TimeObject.h"
#include "../../Helper.h"

#include <Magnum/SceneGraph/Animable.h>

// Lamp
//
// @TimeObject: rewinding time to repair the lamp
//
// modifies @DrawablePBR::lightIntensityModifier 
// to simulate a flickering light source
//
class Lamp : public BaseObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @animables: animable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	Lamp(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables, Magnum::SceneGraph::AnimableGroup3D& animables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);
	~Lamp() { DrawablePBR::lightIntensityModifier = 1.0f; }

private:
	virtual void updateTimeAnimation() override;

	void turnLightOn();
	void turnLightOff();

	// when will be next flicker change?
	float flickerTime = 0.f;
	// modifier for how long lamp is on when flickering
	float lampOnModifier;
	// modifier for how long lamp is off when flickering
	float lampOffModifier;

	bool isOn = true;

	const Magnum::Vector3 EMISSIVE_FACTOR_ON { 1.f,1.f,1.f };
	const Magnum::Vector3 EMISSIVE_FACTOR_OFF { 0.03f,0.03f,0.03f };

	const float MIN_FLICKER_TIME = 0.02f;
	const float MAX_FLICKER_TIME = 0.2f;

	// Animable used to time the light flickering
	class FlickerObject : public Object3D, public Magnum::SceneGraph::Animable3D
	{
	public:
		FlickerObject(Lamp& lamp, Magnum::SceneGraph::AnimableGroup3D& animables)
			: Object3D{ &lamp }, Magnum::SceneGraph::Animable3D{ *this, &animables }
		{
			this->lamp = &lamp;
		}

	private:
		Lamp* lamp;

		void animationStep(Magnum::Float time, Magnum::Float delta)
		{
			lamp->flickerTime -= delta;
			if (lamp->flickerTime <= 0)
			{
				lamp->flickerTime = Helper::randomFloat(lamp->MIN_FLICKER_TIME, lamp->MAX_FLICKER_TIME);

				if (lamp->isOn) {
					lamp->turnLightOff();
					lamp->flickerTime *= lamp->lampOffModifier;
				}
				else {
					lamp->turnLightOn();
					lamp->flickerTime *= lamp->lampOnModifier;
				}
			}
		}
	};
	FlickerObject* flickerObject;
};