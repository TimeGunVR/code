#include "Platform.h"
#include "../../Constants.h"
#include "../../Helper.h"

Platform::Platform(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0 }, TimeObject{ name }
{
	animationDuration = 5.f;
}

void Platform::update(Magnum::Vector3 headPosition)
{
	Magnum::Vector2 plat2D(transformationMatrix().translation().x(), transformationMatrix().translation().z());
	Magnum::Vector2 head2D(headPosition.x(), headPosition.z());

	bool isOnPlatform = (plat2D - head2D).length() <= 45.f;
	
	switch (state)
	{
		case FLOOR1: 
			if (offsetY == 0.f && isOnPlatform)
				state = PLATFORM;
		break;	

		case PLATFORM: 
			if (!isOnPlatform)
			{
				if (offsetY == 270.f)
				{
					state = FLOOR2;
					offsetY = 0.f;
				}
				else
				{
					movePlatform(offsetY);
					moveWorld(-offsetY);
					state = FLOOR1;
				}
			}
		break;	

		case FLOOR2: 
			if (isOnPlatform)
			{
				float newOffsetY = 270.f + offsetY;
				moveWorld(offsetY);
				movePlatform(0.f);
				offsetY = newOffsetY;
				state = PLATFORM;
			}
		break;
	}
}

void Platform::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	magicBean = (Seed*)objects.at(Constant::SN_MAGICBEAN);
}

void Platform::updateTimeAnimation()
{
	if (!magicBean->isImplanted())
	{
		animationTime = 0;
		return;
	}

	float newOffsetY = 270.f * animationTime / animationDuration;

	switch (state)
	{
		case FLOOR1:
			movePlatform(newOffsetY);
			break;

		case PLATFORM:
			moveWorld(newOffsetY - offsetY);
			break;

		case FLOOR2:
			movePlatform(newOffsetY - 270.f);
	}
}

void Platform::moveWorld(float deltaY)
{
	for (auto kvp1 : BaseObject::getAllObjects())
	{
		if (kvp1.first == name)
			continue;

		for (auto kvp2 : kvp1.second->getRigidBodies())
		{
			btVector3 origin = kvp2.second->getWorldTransform().getOrigin();
			origin.setY(origin.y() - deltaY);

			btTransform t;
			t.setIdentity();
			t.setOrigin(origin);

			kvp2.second->setWorldTransform(t);
			kvp2.second->getMotionState()->setWorldTransform(t);

			BaseObject::world->removeRigidBody(kvp2.second);
			BaseObject::world->addRigidBody(kvp2.second);
		}

		if (Helper::getModelPartialShortname(kvp1.first) == Constant::PS_FLOWER)
			((Flower*)kvp1.second)->refreshScale();
	}

	offsetY += deltaY;
}

void Platform::movePlatform(float newOffsetY)
{
	btRigidBody* rb = rigidBodies.begin()->second;
	btVector3 origin = rb->getWorldTransform().getOrigin();
	origin.setY(newOffsetY);

	btTransform t;
	t.setIdentity();
	t.setOrigin(origin);
	rb->setWorldTransform(t);
	rb->getMotionState()->setWorldTransform(t);

	offsetY = newOffsetY;

	BaseObject::world->removeRigidBody(rb);
	BaseObject::world->addRigidBody(rb);
}
