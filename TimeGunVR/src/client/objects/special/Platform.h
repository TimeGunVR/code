#pragma once
#include "../../stdafx.h"
#include "../base/TimeObject.h"
#include "../base/BaseObject.h"
#include "../../drawing/MaterialPBR.h"
#include "Seed.h"

// Platform
//
// @TimeObject: rises the platform to get to the next floor
//
// if the magic bean is implanted, it grows a big tree trunk that rises the platform
//
class Platform : public BaseObject, public TimeObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	explicit Platform(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	// Update current @Platform::state according to player 
	// position to handle stepping on/falling off platform
	void update(Magnum::Vector3 headPosition);

private:
	enum State 
	{ 
		FLOOR1,		// player is on first floor
		PLATFORM,	// player is standing on platform
		FLOOR2		// player is on second floor
	};
	State state = FLOOR1;

	virtual void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;
	virtual void updateTimeAnimation() override;
	
	// moves world in negative y direction about @deltaY
	// & sets value for @Platform::offsetY accordingly
	void moveWorld(float deltaY);
	// moves platform to @offsetY & sets value for 
	// @Platform::offsetY accordingly
	void movePlatform(float offsetY);

	float offsetY = 0.f;

	Seed* magicBean;
};