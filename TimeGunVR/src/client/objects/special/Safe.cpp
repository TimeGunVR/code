#include "Safe.h"

#include "FloppyDisk.h"
#include "../../Helper.h"

#include <boost/algorithm/clamp.hpp>

Safe::Safe(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0, {"door", "dial"} }, TimeObject{ name }, GrabObject{ name }
{
	door = objects.at("door");
	dial = objects.at("dial");

	btRigidBody* safe = rigidBodies.at("Safe");
	btRigidBody* door = rigidBodies.at("door");
	btRigidBody* dial = rigidBodies.at("dial");

	safe->setCollisionFlags(safe->getCollisionFlags());
	safe->setUserIndex(0);
	door->setUserIndex(1);
	dial->setUserIndex(2);

	/*
	void btHingeConstraint::setLimit (
		btScalar    low,
		btScalar    high,
		btScalar    _softness = 0.9f,
		btScalar    _biasFactor = 0.3f,
		btScalar    _relaxationFactor = 1.0f
	)
	*/

	btVector3 axis(0.f, 1.f, 0.f);
	btVector3 anchorSafe = door->getCenterOfMassTransform().getOrigin() - safe->getCenterOfMassTransform().getOrigin();
	btVector3 anchorDoor(0.f, 0.f, 0.f);
	doorConstraint = new btHingeConstraint(*safe, *door, anchorSafe, anchorDoor, axis, axis);
	doorConstraint->setLimit(0, 0);
	doorConstraint->enableAngularMotor(true, 0.0f, 500.0f);
	BaseObject::world->addConstraint(doorConstraint, true);
	doorConstraint->setDbgDrawSize(0.5f);


	Magnum::Vector3 referencePoint = objects.at("handle")->absoluteTransformationMatrix().translation();
	Magnum::Vector3 temp = Magnum::Math::cross(objects.at("hinge4")->absoluteTransformationMatrix().translation() - referencePoint,
		objects.at("hinge1")->absoluteTransformationMatrix().translation() - referencePoint);
	temp = temp.normalized();
	rotationVectorDial = btVector3(temp);
	btVector3 anchorDoor2 = dial->getCenterOfMassTransform().getOrigin() - door->getCenterOfMassTransform().getOrigin();;
	btVector3 anchorDial(0.f, 0.f, 0.f);
	dialConstraint = new btHingeConstraint(*door, *dial, anchorDoor2, anchorDial, rotationVectorDial, rotationVectorDial);
	dialConstraint->setLimit(-Magnum::Constants::pi(), Magnum::Constants::pi());
	dialConstraint->enableAngularMotor(true, 0.0f, 200.0f);

	BaseObject::world->addConstraint(dialConstraint, true);
	dialConstraint->setDbgDrawSize(0.5f);

	rotationCounter = code[(sizeof(code) / sizeof(*code)) - 1];
	resetStartAngle = rotationCounter;
	codeCounter = (sizeof(code) / sizeof(*code)) - 1;
	turnToZero = true;
	dial->setCenterOfMassTransform(btTransform(btQuaternion(rotationVectorDial, -Helper::DegToRad(rotationCounter)), dial->getCenterOfMassPosition()));
	float currentAngle = Helper::RadToDeg(dialConstraint->getHingeAngle());
	currentAngle = (currentAngle < 0) ? 360.0f + currentAngle : currentAngle;
	prevDialRotation = currentAngle;

	codeTotalAngles = calculateRotationSum(code, 0, (sizeof(code) / sizeof(*code)) - 1);
	animationDuration = (codeTotalAngles) * 0.01f;
	animationTime = animationDuration;
}

void Safe::checkDial() {
	if (isDoorOpen || isTarget())
		return;

	float currentAngle = Helper::RadToDeg(dialConstraint->getHingeAngle());
	currentAngle = (currentAngle < 0) ? 360.0f + currentAngle : currentAngle;

	if (!((prevDialRotation > 340.0f && currentAngle < 20.0f) || (prevDialRotation < 20.0f && currentAngle > 340.0f))) {
		if (turnedDialLeft && prevDialRotation < currentAngle) {
			turnedDialLeft = false;
			resetStartAngle = rotationCounter;
			turnedDialLeftAngle = 0;
		}
		else if (!turnedDialLeft && currentAngle < prevDialRotation)
		{
			if(turnedDialLeftAngle >= TURN_LEFT_THRESHOLD)
				turnedDialLeft = true;
		}
	}
	calculateCurrentTotalRotation(currentAngle);

	if (rotationCounter > resetStartAngle + 1440.0f + (360.0f - fmod(resetStartAngle, 360.0f)))
	{
		rotationCounter = currentAngle;
		resetStartAngle = 0.0f;
		codeCounter = 1;
		blub = false;
		resetLoop = true;
		turnToZero = false;
	}

	if (turnToZero)
		return;

	if (resetLoop) {
		if (rotationCounter > 360.0f) {
			rotationCounter = currentAngle;
			codeCounter = 1;
			blub = false;
		}
		else if (rotationCounter < -360.0f) {
			rotationCounter = currentAngle - 360.0f;
			codeCounter = 1;
			blub = false;
		}
	}

	if (!blub && ((codeCounter % 2 == 1) ? rotationCounter > code[codeCounter] : rotationCounter < code[codeCounter])) {
		blub = true;
		if (codeCounter + 1 == (sizeof(code) / sizeof(*code)))
			openSafeDoor();
	}
	else if (blub) {
		if ((codeCounter % 2 == 1) ? (rotationCounter > code[codeCounter] + TOLERANCE) : (rotationCounter < code[codeCounter] - TOLERANCE)) {
			blub = false;
			turnToZero = true;
		}
		else if ((codeCounter % 2 == 1) ? rotationCounter < code[codeCounter] : rotationCounter > code[codeCounter]) {
			blub = false;
			resetLoop = false;
			codeCounter++;
		}
	}
}

void Safe::grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) {
	if (object->getUserIndex() == 0) return;

	if (object->getUserIndex() == 1 && isDoorOpen) {
		GrabObject::grab(hand, rigidBodies.at("door"), btTransform(btQuaternion::getIdentity(), transformHand.getOrigin()), btTransform(btQuaternion::getIdentity(), transformObj.getOrigin()), false);
	}
	else if (object->getUserIndex() == 2 && !isDoorOpen) {
		if (wasShot) {
			turnToZero = true;
			resetStartAngle = rotationCounter;
			wasShot = false;
		}

		GrabObject::grab(hand, rigidBodies.at("dial"), btTransform(rigidBodies.at("dial")->getCenterOfMassTransform().getRotation(), { 0.0f, 0.0f, 0.0f }), btTransform(btQuaternion::getIdentity(), { 0.0f, 0.0f, 0.0f }), true);
	}
}

void Safe::updateTimeAnimation() {
	float currentAnimationRotation = (animationTime / animationDuration) * codeTotalAngles;

	btRigidBody* dial = rigidBodies.at("dial");
	
	for (int i = 0; i < (sizeof(code) / sizeof(*code)) - 1; i++) {
		if (currentAnimationRotation >= calculateRotationSum(code, 0, i) && currentAnimationRotation < calculateRotationSum(code, 0, i+1)) {
			if(i % 2 == 0)
				rotationCounter = code[i] + ((abs(code[i + 1] - code[i])) - (calculateRotationSum(code, 0, i + 1) - currentAnimationRotation));
			else
				rotationCounter = code[i] - ((abs(code[i + 1] - code[i])) - (calculateRotationSum(code, 0, i + 1) - currentAnimationRotation));
			break;
		}
	}

	dial->setCenterOfMassTransform(btTransform(btQuaternion(rotationVectorDial, -Helper::DegToRad(rotationCounter)), dial->getCenterOfMassPosition()));
	float currentAngle = Helper::RadToDeg(dialConstraint->getHingeAngle());
	currentAngle = (currentAngle < 0) ? 360.0f + currentAngle : currentAngle;
	prevDialRotation = currentAngle;
}

void Safe::onTargeted() {
	rigidBodies.at("dial")->setAngularVelocity({ 0.0f, 0.0f, 0.0f });
	if (animationTime == animationDuration) {
		rotationCounter = code[(sizeof(code) / sizeof(*code)) - 1];
	}
	wasShot = true;
}

void Safe::openSafeDoor() {
	rigidBodies.at("dial")->setAngularVelocity({ 0.0f, 0.0f, 0.0f });
	doorConstraint->setLimit(-(200.0f*Magnum::Constants::pi()) / 180.0f, 0, 0.3f);
	isDoorOpen = true;
	((FloppyDisk*)GrabObject::getObject("FloppyDisk"))->setCanBeGrabed(true);
}

void Safe::calculateCurrentTotalRotation(float currentRelativAngle) {
	if (prevDialRotation > 340.0f && currentRelativAngle < 20.0f) {
		rotationCounter += (currentRelativAngle + (360.0f - prevDialRotation));
		turnedDialLeftAngle -= (currentRelativAngle + (360.0f - prevDialRotation));
	}
	else if (prevDialRotation < 20.0f && currentRelativAngle > 340.0f) {
		rotationCounter += (-prevDialRotation - (360.0f - currentRelativAngle));
		turnedDialLeftAngle -= (-prevDialRotation - (360.0f - currentRelativAngle));
	}
	else {
		rotationCounter += (currentRelativAngle - prevDialRotation);
		turnedDialLeftAngle -= (currentRelativAngle - prevDialRotation);
	}
	turnedDialLeftAngle = std::max(0.0f, turnedDialLeftAngle);
	prevDialRotation = currentRelativAngle;
}

float Safe::calculateRotationSum(float arr[], int from, int to) {
	float sum = 0;
	for (int i = from + 1; i <= to; i++)
		sum += abs(arr[i] - arr[i - 1]);
	return sum;
}


