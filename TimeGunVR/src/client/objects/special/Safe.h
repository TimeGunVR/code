#pragma once
#include "../../stdafx.h"
#include "../base/TimeObject.h"
#include "../base/GrabObject.h"
#include "../base/BaseObject.h"
#include "../../drawing/MaterialPBR.h"

// Safe (+ door, dial)
//
// @TimeObject: shows the combination for the dial
// @GrabObject: door and dial can be grabbed
//
// to open the safe the right combination/code has to be dialed
//
class Safe : public BaseObject, public TimeObject, public GrabObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	explicit Safe(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	// checks the rotation of the dial and if the code has been dialed
	void checkDial();

	virtual void grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) override;
	virtual void updateTimeAnimation() override;
	virtual void onTargeted() override;

private:
	// called as soon as the code has been dialed
	// opens the safe door
	void openSafeDoor();
	void calculateCurrentTotalRotation(float currentRelativAngle);
	float calculateRotationSum(float arr[], int from, int to);

	Object3D *door, *dial;
	float doorAngle = 0.f;
	float dialAngle = 0.f;

	bool isDoorOpen = false;
	btHingeConstraint* doorConstraint;
	btHingeConstraint* dialConstraint;

	const float TOLERANCE = 36.0f;

	// Degree:  0   36   72  108  144  180  216  252  288  324
	// Number:  0    9    8    7    6    5    4    3    2    1   

	// Degree:  0  -36  -72 -108 -144 -180 -216 -252 -288 -324
	// Number:  0    1    2    3    4    5    6    7    8    9 
	float code[5] = { 0.0f, 180.0f, -324.0f, -216.0f, -324.0f };
	float codeTotalAngles;

	float prevDialRotation;
	// start angle for the reset (reset is done by turning the dial clockwise 4 times)
	float resetStartAngle;
	float turnedDialLeftAngle = 0;
	const float TURN_LEFT_THRESHOLD = 20.0f;
	bool turnedDialLeft = true;
	// current total rotation
	float rotationCounter;
	// at which code number we are at
	int codeCounter;
	// to check if dial is overwinded
	bool blub = false;
	bool turnToZero = false;
	bool resetLoop = false;

	btVector3 rotationVectorDial;
	
	bool wasShot = false;
};