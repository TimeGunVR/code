#include "Seed.h"
#include "../../Helper.h"
#include <boost/algorithm/clamp.hpp>

Seed::Seed(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material, bool magic)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 0.001f}, GrabObject{ name }
{
	isMagicSeed = magic;

	Seed::drawables = &drawables;
}

void Seed::findObjectDependencies(std::map<std::string, BaseObject*>& objects)
{
	if (flowers.size() != 0)
		return;

	for (int i = 1; i <= 16; i++)
		flowers.push_back((Flower*)objects.at(Constant::PS_FLOWER + "_" + std::to_string(i)));
}

void Seed::grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) {
	if (!implanted)
		GrabObject::grab(hand, rigidBodies.at(this->getName()), btTransform(btQuaternion::getIdentity(), { 0.0f, 0.0f, 0.0f }), btTransform(btQuaternion::getIdentity(), { 0.0f, 0.0f, 0.0f }), true);
}

void Seed::implantSeed(btRigidBody* bed, btVector3 implantPoint) {
	if (implanted)
		return;

	GrabObject::ungrab();
	btRigidBody* seed = rigidBodies.at(this->getName());
	seed->setCollisionFlags(4);
	btVector3 anchor = btVector3(implantPoint.getX() , -5.0f, implantPoint.getZ());
	btFixedConstraint *constraint = new btFixedConstraint(
		*bed, *seed,
		btTransform(btQuaternion::getIdentity(), anchor),
		btTransform(btQuaternion::getIdentity(), { 0.0f,  0.0f, 0.0f })
	);
	BaseObject::world->addConstraint(constraint, true);
	constraint->setDbgDrawSize(0.5f);
	
	if (!isMagicSeed)
	{
		int minIndex = -1;
		float minDistance = FLT_MAX;
		for (int i = 0; i < flowers.size(); i++) 
		{
			float distance = (flowers[i]->transformationMatrix().translation() - transformationMatrix().translation()).length();
			if (minDistance > distance) 
			{
				minDistance = distance;
				minIndex = i;
			}
		}
		flowers.at(minIndex)->plant();
		flowers.erase(flowers.begin() + minIndex);
	}

	implanted = true;
}

std::vector<Flower*> Seed::flowers;