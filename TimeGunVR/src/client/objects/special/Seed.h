#pragma once
#include "../../stdafx.h"

#include "Flower.h"
#include "../base/BaseObject.h"
#include "../base/GrabObject.h"

#include "../../drawing/MaterialPBR.h"

// Seed (Magic Bean)
//
// @GrabObject: seed can be grabbed 
//
// seeds should be implanted into the bed/platform
// normal seed grow a flower and the magic bean grows a tree trunk
//
class Seed : public BaseObject, public GrabObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	// @magic: indicates if the seed is the magic bean
	explicit Seed(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material, bool magic);

	void findObjectDependencies(std::map<std::string, BaseObject*> &objects) override;

	virtual void grab(btRigidBody* hand, btRigidBody* object, btTransform transformHand, btTransform transformObj, bool fixed) override;

	// implants the seed into the bed and fixes it (btFixedConstraint)
	void implantSeed(btRigidBody* bed, btVector3 implantPoint);

	bool isImplanted() { return implanted; }

private:
	static std::vector<Flower*> flowers;
	Magnum::SceneGraph::DrawableGroup3D* drawables;

	bool isMagicSeed;
	bool implanted = false;
};