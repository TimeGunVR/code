#include "Teleporter.h"

Teleporter::Teleporter(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
	Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID,
	MaterialPBR material)
	: BaseObject{ name, parent, drawables, importer, objectID, material, 10 }
{
	deactivate();

	btRigidBody* teleporter = rigidBodies.at(Constant::SN_TELEPORTER);
	teleporter->setCollisionFlags(5);
}

void Teleporter::activate()
{
	_isActive = true;
	material.setEmissiveFactor(EMISSION_COLOR_ACTIVATED);
}

void Teleporter::deactivate()
{
	_isActive = false;
	material.setEmissiveFactor(EMISSION_COLOR_DEACTIVATED);
}