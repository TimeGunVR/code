#pragma once

#include "../../stdafx.h"
#include "../base/BaseObject.h"

// Teleporter
//
// teleports user to the next level
//
class Teleporter : public BaseObject
{
public:
	// @name: name of the object
	// @parent: parent of this object
	// @drawables: drawable group where this object will be added
	// @importer: importer with opened scene file
	// @objectID: id of this object in the importer
	// @material: contains textures for PBR shading
	Teleporter(std::string name, Object3D& parent, Magnum::SceneGraph::DrawableGroup3D& drawables,
		Magnum::Trade::AbstractImporter& importer, Magnum::UnsignedInt objectID, MaterialPBR material);

	void activate();
	void deactivate();

	// indicates wether telporter is actived
	bool isActive() { return _isActive; }

private:
	const Magnum::Vector3 EMISSION_COLOR_ACTIVATED{ 0.0f, 0.0f, 1.0f };
	const Magnum::Vector3 EMISSION_COLOR_DEACTIVATED{ 0.0f, 0.0f, 0.0f };

	bool _isActive = false;
};

