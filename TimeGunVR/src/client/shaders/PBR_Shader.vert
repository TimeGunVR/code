// Adapted from KhronosGroup's "glTF-WebGL-PBR" repository and modified
// Source: https://github.com/KhronosGroup/glTF-WebGL-PBR
// License for original file:
// The MIT License
//
//Copyright (c) 2016-2017 Mohamad Moneimne and Contributors
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
//to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
//WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//Third-Party Code
//=============================
//
//## glMatrix
//
//https://github.com/toji/gl-matrix
//
//Matrix calculations are handled using glMatrix.
//
//> Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
//>
//> Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//>
//> The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//>
//> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//
//## dat.gui
//
//https://github.com/dataarts/dat.gui
//
//The GUI is handled by dat.gui.
//
//>Copyright 2014, Google Inc.
//>
//>Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//>     http://www.apache.org/licenses/LICENSE-2.0
//
//>Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

layout(location = 0) in vec4 a_Position;
#ifdef HAS_UV
layout(location = 1) in vec2 a_UV;
#endif
#ifdef HAS_NORMALS
layout(location = 2) in vec3 a_Normal;
#endif
#ifdef HAS_TANGENTS
layout(location = 3) in vec4 a_Tangent;
#endif

layout(location = 0) uniform mat4 u_MVPMatrix = mat4(1.0);
layout(location = 1) uniform mat4 u_ModelMatrix = mat4(1.0);
layout(location = 2) uniform mat3 u_NormalMatrix= mat3(1.0);

layout(location = 15) uniform vec2 u_UVOffset = vec2(0.0);

out vec3 v_Position;
out vec2 v_UV;

#ifdef HAS_NORMALS
#ifdef HAS_TANGENTS
out mat3 v_TBN;
#else
out vec3 v_Normal;
#endif
#endif


void main()
{
  vec4 pos = u_ModelMatrix * a_Position;
  v_Position = pos.xyz / pos.w;

  #ifdef HAS_NORMALS
  #ifdef HAS_TANGENTS
  vec3 normalW = normalize(vec3(u_NormalMatrix * a_Normal));
  vec3 tangentW = normalize(vec3(u_ModelMatrix * vec4(a_Tangent.xyz, 0.0)));
  vec3 bitangentW = cross(normalW, tangentW) * a_Tangent.w;
  v_TBN = mat3(tangentW, bitangentW, normalW);
  #else // HAS_TANGENTS != 1
  v_Normal = normalize(u_NormalMatrix * a_Normal);
//  v_Normal = normalize(vec3(u_ModelMatrix * vec4(a_Normal, 0.0)));
  #endif
  #endif

  #ifdef HAS_UV
  v_UV = a_UV + u_UVOffset;
  #else
  v_UV = vec2(0.,0.) + u_UVOffset;
  #endif

  gl_Position = u_MVPMatrix * a_Position; // needs w for proper perspective correction
}


