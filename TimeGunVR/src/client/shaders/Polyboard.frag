layout(binding = 0) uniform sampler2D u_texture;

in vec2 g_uv;

layout(location = 0)
out vec4 fragmentColor;

void main()
{
	fragmentColor = texture2D(u_texture, g_uv);
	if(fragmentColor.a < 0.5)
		discard;
}