#extension GL_ARB_geometry_shader4 : enable

layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 6) out;

layout(location = 0) uniform mat4 u_MVPMatrix = mat4(1.0);
layout(location = 1) uniform mat4 u_ModelMatrix = mat4(1.0);
layout(location = 2) uniform vec3 u_Camera = vec3(0.0);
layout(location = 3) uniform float u_Radius = 10.0;


in vec4 v_position[];
in vec2 v_UV[];

out vec2 g_uv;

void main()
{
	vec3 p1; // vertex 1
	vec3 c1; // normalized vector from p1 to camera
	vec3 t1; // tanget at p1

	vec3 p2; // vertex 2
	vec3 c2; // normalized vector from p2 to camera
	vec3 t2; // tanget at p2

	vec3 g1; // first new vertex for p1
	vec3 h1; // second new vertex for p1	
	vec3 g2; // first new vertex for p2
	vec3 h2; // second new vertex for p2

	// Calculate new vertices for p1
	//
	p1 = v_position[1].xyz;
	c1 = normalize(u_Camera-p1);
	t1 = normalize(v_position[2].xyz - v_position[0].xyz);

	g1 = p1 + u_Radius * (cross(t1, c1));
	h1 = p1 - u_Radius * (cross(t1, c1));	
	
	// Calculate new vertices for p2
	//
	p2 = v_position[2].xyz;
	c2 = normalize(u_Camera-p2);
	t2 = normalize(v_position[3].xyz - v_position[1].xyz);

	g2 = p2 + u_Radius * (cross(t2, c2));
	h2 = p2 - u_Radius * (cross(t2, c2));

	// output triangle strip
	//

	gl_Position = u_MVPMatrix * vec4(h1.xyz, 1);
	g_uv = vec2(0.0, v_UV[1].y);
	EmitVertex();
	gl_Position = u_MVPMatrix * vec4(g1.xyz, 1);
	g_uv = vec2(1.0, v_UV[1].y);
	EmitVertex();
	gl_Position = u_MVPMatrix * vec4(h2.xyz, 1);
	g_uv = vec2(0.0, v_UV[2].y);
	EmitVertex();
	gl_Position = u_MVPMatrix * vec4(g2.xyz, 1);
	g_uv = vec2(1.0, v_UV[2].y);
	EmitVertex();
	EndPrimitive();
}