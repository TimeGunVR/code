layout(location = 0) in vec4 a_Position;
layout(location = 1) in vec2 a_UV;

out vec4 v_position;
out vec2 v_UV;


void main()
{
//  vec4 pos = u_ModelMatrix * a_Position;
//  v_Position = pos.xyz / pos.w;
//
//  v_UV = a_UV;
//
//  gl_Position = u_MVPMatrix * a_Position;

	// just passing through
	v_position = a_Position;
	v_UV = a_UV;
}