/*******************************************************
*Description: Use w,a,s,d to move camera
*Press 'space' to move the animated sphere to a new random location
*******************************************************/
//cave stuff
//Include this before everything else !
#include <renderServer.h>
#include <synchObject.h>
#include <config.h>

#include <Corrade/Containers/Array.h>
#include <Corrade/PluginManager/Manager.h>
#include <Corrade/Utility/Arguments.h>
#include <Magnum/Mesh.h>
#include <Magnum/PixelFormat.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Magnum/MeshTools/Compile.h>
//#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Platform/GlfwApplication.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Trade/MeshObjectData3D.h>
#include <Magnum/Trade/PhongMaterialData.h>
#include <Magnum/Trade/SceneData.h>
#include <Magnum/Trade/TextureData.h>
//
#include <Magnum/SceneGraph/Animable.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Primitives/UVSphere.h>
#include <Magnum/Primitives/Plane.h>
#include <Magnum/Timeline.h>
#include <Magnum/SceneGraph/AnimableGroup.h>
#include <Magnum/Timeline.h>

#include <Magnum/GL/Buffer.h>

#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Math/Color.h>

//other
#include <functional>
#include <iostream>
#include <thread>
#include <chrono>


using namespace Magnum;
using namespace Math::Literals;

class MyProject: public Platform::GlfwApplication
{
    public:
        explicit MyProject(const Arguments& arguments);
		~MyProject() {
			if (rServer.get()) {
				rServer->stopSynching();
			}
		}
    private: 
        void drawEvent() override;
		void processWandButtonInput();

		std::shared_ptr< synchlib::renderServer > rServer;
		std::shared_ptr< synchlib::udpSynchObject<long long> > timeSyncher;
		std::shared_ptr< synchlib::udpSynchObject<glm::bvec4 > > wandButtonSyncher;

		std::chrono::high_resolution_clock::time_point time_start;

		glm::bvec4 buttonPressed;
};


MyProject::MyProject(const Arguments& arguments) :
	Platform::Application { arguments, Configuration{}
		.setTitle("Time Gun VR Server")
		.setWindowFlags(Configuration::WindowFlag::Resizable)}
{
	rServer = std::make_shared<synchlib::renderServer>(arguments.argv[1], arguments.argc, arguments.argv);

	timeSyncher = synchlib::udpSynchObject<long long>::create();
	wandButtonSyncher = synchlib::udpSynchObject<glm::bvec4 >::create();

	rServer->addSynchObject(timeSyncher, synchlib::renderServer::SENDER, 0,0);
	rServer->addSynchObject(wandButtonSyncher, synchlib::renderServer::SENDER, 0,0);
	rServer->init();
	rServer->startSynching();

}

void MyProject::drawEvent() {
	GL::Renderer::setClearColor(Color4(0, 1, 0, 1.f));
	GL::defaultFramebuffer.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

	auto timeNow = std::chrono::high_resolution_clock::now();
	long long dur = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow - time_start).count();
	timeSyncher->setData(dur);
	timeSyncher->send();

	/*
	glm::mat4 mat;
	rServer->multiplyRightSceneTransform(mat);
	rServer->multiplyLeftSceneTransform(mat);
	*/

	processWandButtonInput();

    swapBuffers();
	redraw();
}

void MyProject::processWandButtonInput()
{
	rServer->useDefaultNavigation(false);

	// get button input
	std::list<glm::ivec2> buttonQueue;
	rServer->getButtonQueue(buttonQueue);
	glm::bvec4 newButtonsState = wandButtonSyncher->getData();

	bool changed = false;

	for (auto it = buttonQueue.begin(); it != buttonQueue.end(); ++it) {
		if (it->x > 3)
			continue;

		newButtonsState[it->x] = (it->y == 1);

		if (newButtonsState[it->x] != buttonPressed[it->x])
			changed = true;
	}

	if (changed)
	{
		wandButtonSyncher->setData(newButtonsState);
		wandButtonSyncher->send();
	}

	buttonPressed = newButtonsState;
}


MAGNUM_APPLICATION_MAIN(MyProject)
