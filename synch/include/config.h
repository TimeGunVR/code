#ifndef CONFIG_H
#define CONFIG_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/version.hpp>

#include <glm/fwd.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <map>
#include <sstream>
#include <string>
#include <iostream>


namespace synchlib {

struct wallConf{

    glm::mat3x3 wall_size;
    glm::mat4x4 wall_trafo;
    glm::vec4 wall_geo;
    bool powerwall;
    unsigned short pipeNumber;
    short number;

};


    class caveConfig {
    public:
        /**
         * @brief caveconfig
         * @param configfile path to configuration file
         */
        caveConfig(const std::string &configfile);

        /**
         * @brief ~caveconfig Destructor
         */
        ~caveConfig();


        bool loadConfig(const std::string& filename);
        bool saveConfig(const std::string& filename);
        void writeDefault(const std::string& filename);


        std::string port;
        std::string multicast_address;
        std::string server_address;
        std::string vrpn_address;
        int vrpn_SensorIDHead = 1;
        int vrpn_SensorIDWand = 0;
        bool stereo;


        std::map<std::string,wallConf> m_wall_conf;  //trafo wall to world cosy

        caveConfig();

        void printConfig();
    private:




    };
}

#endif /* CONFIG_H */
