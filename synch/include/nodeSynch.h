#ifndef NODESYNCH_H
#define NODESYNCH_H

#include <boost/asio.hpp>


#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <memory>

#include <config.h>

#ifndef WIN32

#else
#define __PRETTY_FUNCTION__ __FUNCTION__
#define NOMINMAX
#include <windows.h>
#endif

namespace synchlib {

class nodeSynch {
	typedef  std::shared_ptr<boost::asio::ip::tcp::socket> sock_ptr;

public:
	nodeSynch(const std::string& confFile);
	nodeSynch() = delete;

	~nodeSynch();


	bool synchToServer();


	void startServerSynch();

	bool startNodeSynch(const std::string ownIP);


private:

	void synchFailed();

	static void sendToken(sock_ptr socket);
	static void receiveToken(sock_ptr socket);

	void nodeReceiver(unsigned short number);

	void synchThreads();

	void checkIfWaiting(const boost::system::error_code& e);

	void resetCheckIfWaiting();

	void runIoService();

	caveConfig m_conf;



	std::shared_ptr<boost::asio::io_service> m_pIoService;
	sock_ptr m_socket;
	std::thread m_ioServiceThread;
	std::shared_ptr<boost::asio::deadline_timer> m_pCheckIfWaitingTimer;
	std::mutex m_checkIfWaitingMutex;


    std::vector<std::thread> m_synchThreads;
    std::atomic_bool* m_receivedTokens;
    std::vector<std::string> m_nodehostnames;


    std::atomic_uint m_synchThreadsCounter;
    std::mutex m_syncThreadMutex;
    std::condition_variable m_synchThreadsCondition;

    std::shared_ptr<boost::asio::io_service::work > m_pWork;
    std::string m_ownIP;
    std::string m_lastWritten;
    unsigned int m_lastWrittenCount = 0;



    std::mutex m_synchThreadsMutex;

    bool m_debug = false;
    bool m_stop = false;
    bool m_server = false;


};

}

#endif /*NODESYNCH*/
