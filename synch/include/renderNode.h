#ifndef RENDERNODE_H
#define RENDERNODE_H

#include <glm/fwd.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <config.h>
#include <glm/gtx/transform.hpp>
#include <synchObject.h>
#include <mutex>
#include <thread>
#include <list>
#include <nodeSynch.h>

namespace synchlib {
class renderNode {
public:
    enum connectiontype{
        RECEIVER,
        SENDER,
        NONE
    };

    renderNode(const std::string confFile, int argc, char** argv, bool debug = false);
    ~renderNode();
    /**
     * @brief init Synchronises rendernode to renderserver. Only returns after all rendernodes defined in config are ready.
     * @return False, if something failed.
     */
    bool init();
    /**
     * @brief   startSynching Starts synching of vrpn objects and all synchobjects added via "bool addSynchObject"
     *          adds a new thread for each object
     */
    void startSynching();
    /**
     * @brief stopSynching Stop synchronisation
     */
    void stopSynching();
    /**
     * @brief getUserTrafo get transformation matrix of user (head tracking)
     * @param userTrafo Transformation matrix of user
     */
    void getUserTrafo(glm::mat4x4& userTrafo);
    /**
     * @brief getSceneTrafo get transformation matrix of scene
     * @param sceneTrafo Transformation matrix of scene
     */
    void getSceneTrafo(glm::mat4x4& sceneTrafo);

    /**
     * @brief getProjectionMatrix get Projection matrix for opengl pipeline
     * @param projMatrix projection matrix
     */
    void getProjectionMatrix(glm::mat4x4& projMatrix, const bool rightEye = false);

	glm::mat4 getProjectionMatrix(const bool rightEye = false);

    /**
     * @brief getPMVMatrix Get Projectionviewmatrix(can be directly applied to opengl pipeline)
     * @param pvMatrix transformation matrix for projection+view matrix
     */
    void getPVMatrix(glm::mat4x4 &pvMatrix, const bool rightEye = false);

	/**
	* @brief getProjectionMatrix get Projection matrix for opengl pipeline
	* @param projMatrix projection matrix
	*/
	void getProjectionMatrices(glm::mat4x4& projMatrixLeft, glm::mat4x4& projMatrixRight);

	/**
	* @brief getPMVMatrix Get Projectionviewmatrix(can be directly applied to opengl pipeline)
	* @param pvMatrix transformation matrix for projection+view matrix
	*/
	void getPVMatrices(glm::mat4x4 &pvMatrixLeft, glm::mat4x4 &pvMatrixRight);

    /**
     * @brief addSynchObject Add a synchobject to the rendernode.
     * @param[in] obj synchobject to be added to the synchronising queue
     * @param[in] connectionType Choose if this object is a receiver, a sender olr is already initialised (connected)
     * @return returns true if object could be added (not possible if startSynching is already active)
     */

    bool addSynchObject(std::shared_ptr<SuperSynchObject> obj, connectiontype connectionType, short port = 0);

    void buttonReceiverHandler(std::shared_ptr<SynchObject<glm::ivec2,boost::asio::ip::udp> > classPointer);

    void getButtonQueue(std::list<glm::ivec2>& vec){m_bQueueMutex.lock(); vec = m_buttonQueue; m_buttonQueue.clear(); m_bQueueMutex.unlock();}

    //void getAnalogVec(glm::vec3& vec){m_pAnalogTrackingSyncher->getData(vec);}
    void getWandTrafo(glm::mat4x4& trafo){m_pWandTrackingSyncher->getData(trafo);}

    glm::mat4 getWandTrafo(){glm::mat4 retMat; m_pWandTrackingSyncher->getData(retMat); return retMat;}

    /**
     * @brief synchFrame Blocking call waiting for defined number of render nodes to try synching.
     * @return False if pipe broken (so you can clean up and exit)
     */
    bool synchFrame();

    bool isPowerwall(){return m_powerwall;}

    bool isStereo(){return m_conf.stereo;};

    size_t getWindowWidth(){return m_conf.m_wall_conf[m_ownIp].wall_geo[0];};
    size_t getWindowHeight() {return m_conf.m_wall_conf[m_ownIp].wall_geo[1];};



private:
    std::shared_ptr<boost::asio::io_service> m_pIoService;
    synchlib::nodeSynch m_nodeSynch;

    std::vector<std::shared_ptr<SuperSynchObject> > m_synchList;
    glm::mat4x4 m_userTrafo;
    std::mutex m_userTrafoMutex;
    glm::mat4x4 m_sceneTrafo;
    std::mutex m_sceneTrafoMutex;

    glm::mat4x4 m_wallTrafo; // transform from world CoSy to wall CoSy (in config it's the other way around!)

    std::shared_ptr<udpSynchObject<glm::mat4x4> > m_pHeadTrackingSyncher;
    std::shared_ptr<udpSynchObject<glm::mat4x4> > m_pWandTrackingSyncher;
    std::shared_ptr<udpSynchObject<glm::mat4x4> > m_pAnalogTrackingSyncher;
    std::shared_ptr<udpSynchObject<glm::ivec2> > m_pButtonTrackingSyncher;
    caveConfig m_conf;

    std::string m_ownIp;
    float m_wallRight, m_wallLeft, m_wallTop, m_wallBottom; // in wall CoSy

    bool m_debug;
    bool m_stop;
    int m_currentPort;

    std::list<glm::ivec2> m_buttonQueue;
    std::mutex m_bQueueMutex;
    bool m_initialised = false;


    std::vector<std::thread> m_synchThreads;

    bool m_powerwall;


    void convertToPMat(const glm::mat4 userTrafo, glm::mat4& projMatrix, const bool rightEye);
};

}

#endif /* RENDERNODE_H */

