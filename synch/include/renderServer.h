#ifndef RENDERSERVER_H
#define RENDERSERVER_H
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/norm.hpp>

#include <config.h>
#include <synchObject.h>

#include <boost/asio.hpp>


#include <condition_variable>
#include <mutex>
#include <thread>
#include <set>
#include <string>
#include <chrono>
#include <list>
#include <sstream>

#include <nodeSynch.h>


#include <vrpn_Tracker.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>

#ifdef WIN32
#define getcwd _getcwd
#include<direct.h>
#endif

namespace synchlib {
class renderServer{
public:
    enum connectiontype{
        RECEIVER,
        SENDER,
        NONE
    };

    renderServer(const std::string& confFile, int argc, char** argv);
    ~renderServer();
    /**
     * @brief init Allows all rendernodes an inital synchronisation. Prints after timeout which rendernodes are not ready, if any.
     * @return True if success
     */
    bool init(bool startNodes = true);
    /**
     * @brief   startSynching Starts synching of vrpn objects and all synchobjects added via "bool addSynchObject"
     *          adds a new thread for each object
     */
    void startSynching();

    void stopSynching();
    /**
     * @brief addSynchObject Add a synchobject to the renderserver.
     * @param[in] obj synchobject to be added to the synchronising queue
     * @param[in] connectionType Choose if this object is a receiver, a sender or is already initialised (connected)
     * @param[in] milliseconds Time to wait for publishing update of object
     * @return returns true if object could be added (not possible if startSynching is already active)
     */
     bool addSynchObject(std::shared_ptr<SuperSynchObject> obj, connectiontype connectionType, long int milliseconds, short port = 0);

     bool getButtonQueue(std::list<glm::ivec2> &queue);

     bool getSceneTransform(glm::mat4& sceneTrafo);

     glm::mat4 getSceneTransform();

     glm::mat4 getWandTransform();

     glm::mat4 getHeadTransform();

     void setPredTimes(float times);

     void setAlpha(float alpha);

     void setPrediction(bool active);

     void multiplyRightSceneTransform(const glm::mat4 mat);
     void multiplyLeftSceneTransform(const glm::mat4 mat);

     glm::vec2 getAnalogValue();

	 caveConfig& getConfig() { return m_conf; };

	 void useDefaultNavigation(bool use) { m_useDefaultNavigation = use; };


 private:
     typedef void (renderServer::*CallbackPointer) (void* , const vrpn_TRACKERCB);
     typedef void (renderServer::*CallbackPointerAnalog) (void* , const vrpn_ANALOGCB );
     typedef  boost::asio::ip::tcp::socket* sock_ptr;

     /* Callback fonctions*/
     static void VRPN_CALLBACK callback_receiver1(void* userData, const vrpn_TRACKERCB tracker);
     static void VRPN_CALLBACK callback_receiver2(void* userData, const vrpn_TRACKERCB tracker);
     static void VRPN_CALLBACK callback_analog(void* userData, const vrpn_ANALOGCB analog);
     static void VRPN_CALLBACK callback_button(void* userData, const vrpn_BUTTONCB button);

     void callback_Head_tracker(void* userData, const vrpn_TRACKERCB tracker);
     void callback_Wand_tracker(void* userData, const vrpn_TRACKERCB tracker);

     void callback_analog_tracker(void* userData, const vrpn_ANALOGCB analog);
     void callback_button_tracker(void* userData, const vrpn_BUTTONCB button);


     /* functions */
     void SceneTransformUpdate(const boost::system::error_code &error);
     void TrackerUpdate(const boost::system::error_code &error);

    void startTrackerUpdate();


    void startNodes();



     // Variables
     static CallbackPointer m_cbPtr1;
     static CallbackPointer m_cbPtr2;
     static CallbackPointerAnalog m_cbPtrAnalog;

     synchlib::nodeSynch m_nodeSynch;

     std::shared_ptr<vrpn_Tracker_Remote> m_pTracker;
     std::shared_ptr<vrpn_Analog_Remote> m_pAnalogTracker;
     std::shared_ptr<vrpn_Button_Remote> m_pButtonTracker;
     std::shared_ptr<boost::asio::deadline_timer> m_pTrackingTimer;
     std::shared_ptr<boost::asio::deadline_timer> m_pSceneTransformTimer;
     boost::asio::io_service m_trackingIoService;

     std::shared_ptr<boost::asio::io_service> m_pIoService;

     std::vector<std::thread> m_synchThreads;




     std::vector<std::shared_ptr<SuperSynchObject> > m_synchList;
     std::vector<long int> m_synchListPeriods;
     glm::mat4x4 m_userTransform;


     glm::mat4x4 m_wandTransform;
     glm::mat4x4 m_sceneTrans;
     glm::mat4x4 m_sceneRot;
     glm::mat4x4 m_preMultRightScene;
     glm::mat4x4 m_preMultLeftScene;


     //tracking coordinate system is rotated
     glm::mat4x4 m_correctionTrafo ;
     float m_rotSpeed = 0.f;
     float m_transSpeed = 0.f;
     float m_rotate = 0.f;
     float m_translate = 0.f;

     bool m_sceneTransformUpdate = false;

	 bool m_useDefaultNavigation = true;

     std::string m_exePath;
     std::string m_configPath;

     std::chrono::high_resolution_clock::time_point m_lastTrackingUpdateTime;// = std::chrono::high_resolution_clock::now();
     std::chrono::high_resolution_clock::time_point m_lastSceneTransformUpdateTime;


     std::shared_ptr<SynchObject<glm::mat4,boost::asio::ip::udp> > m_pHeadTrackingSyncher;
     std::shared_ptr<SynchObject<glm::mat4,boost::asio::ip::udp> > m_pWandTrackingSyncher;
     //navigation matrix is transferred
     std::shared_ptr<SynchObject<glm::mat4,boost::asio::ip::udp> > m_pAnalogTrackingSyncher;
     std::shared_ptr<SynchObject<glm::ivec2,boost::asio::ip::udp> >  m_pButtonTrackingSyncher;

     std::mutex m_useLock;


     bool m_stop = false;

     bool m_debug = false;

     bool m_isSynching = false;


     int m_currentPort = 0;

//     std::mutex m_initMutex;


     glm::vec3 m_spt_1, m_sp2_1;
     glm::vec4 m_sqt_1, m_sq2_1;
     long long int m_oldTimeHead;

     float m_alpha = 0.3;
     float m_predTimes = 1.f;
     bool m_active = false;

     std::vector<std::string> m_additionalArguments;

     std::list<glm::ivec2> m_buttonQueue;

     caveConfig m_conf;

};

}
#endif /*RENDERSERVER_H*/
