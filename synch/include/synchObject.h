#ifndef SYNCHOBJECT_H
#define SYNCHOBJECT_H

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>

#include <functional>
#include <mutex>
#include <future>
#include <memory>
#include <iostream>
#include <list>
#include <type_traits>


#ifndef WIN32
#include <unistd.h>
#else
#define __PRETTY_FUNCTION__ __FUNCTION__
#define NOMINMAX
#include <windows.h>
#endif


/**
 * Superclass for providing synchonised udp objects (need that for renderNode/Server)
 */
namespace synchlib {



typedef boost::asio::ip::udp udp_protocol;
typedef  boost::asio::ip::tcp tcp_protocol;




class SuperSynchObject {

public:
		SuperSynchObject();
		~SuperSynchObject();
	   /**
	     * @brief send Send data if changed.  "void handle_send_data()" is called everytime.
	     * @return
	     */
//	    bool send();

	    /**
	     * @brief startReceiving Start loop for receiving. "void handle_parse_data()" is called everytime something could be received. (Stays in function!)
	     */
	    bool startReceiving();

	    /**
	     * @brief stopSynching Stops receiving or sending loop
	     */
	    void stopSynching();

	    /**
	     * @brief startSending Start loop for sending. (Stays in function)
	     * @param
	     */
	    bool startSending();

	    /**
	     * @brief setSendingTimer Sets sending intervall
	     * @param milliseconds[in] milliseconds Intervall in milliseconds
	     */
	    virtual void setSendingTimer(long int milliseconds) {};

	//    SynchObject* getObject();

//	    /**
//	     * @brief Initialises network stuff for sender
//	     * @param[in] multicast_address IP address (ip-version4) of multicast (where the data are sent to)
//	     * @param[in] port multicast port
//	     * @param[in] buffersize size of needed buffer for network transfer (in char)
//	     */
//	    virtual void init(const std::string& multicast_address, const std::string& port, const std::string &local_ip) {};
//

	    /**
	     * @brief Initialises network stuff
	     * @param[in] listen_address IP address (ip-version4) of the network card that should be used (own ip address/ local device)
	     * @param[in] multicast_address IP address (ip-version4) of multicast (where the data are sent to/ received from)
	     * @param[in] port multicast port
	     * @param[in] receiver flag if this device receives or not
	     */
	    virtual void init(const std::string &listen_address, const std::string &multicast_address, const std::string &port, bool receiver) {};

	    virtual void setChanged() {};

	    virtual void receiveLoop(const boost::system::error_code& error, size_t bytes_recvd) {};
	    virtual void sendLoop(const boost::system::error_code& error  ) {};

protected:
	    unsigned int m_sizeBuffer = 0;
	    boost::asio::io_service m_ioService;

	    std::shared_ptr<boost::asio::io_service::work> m_work;

	    char* m_pData = 0;

	    bool m_initialised = false;
	    bool m_stop = false;
	    long int m_sendIntervall;

	    std::shared_ptr<boost::asio::deadline_timer> m_deadlineTimer;

};





template <typename data_type, typename protocol_type>
class SynchObject  : public SuperSynchObject, public std::enable_shared_from_this<SynchObject<data_type, protocol_type> > {

public:

	static std::shared_ptr<SynchObject<data_type, protocol_type> > create();

    ~SynchObject();

    /**
     * @brief send Send data if changed.  "void handle_send_data()" is called everytime.
     * @return
     */
    template<typename... Dummy,typename tmp = protocol_type> // explanation for enable if: see init(...)-function
    typename std::enable_if<std::is_same<tmp, udp_protocol>::value, bool>::type
    send()
    {
    	static_assert(sizeof...(Dummy)==0, "Do not specify template arguments!");
    	if(!m_initialised){
    		std::cerr<<"In function: " <<__PRETTY_FUNCTION__<<"SynchObject not initialised"<<std::endl;
    		return false;
    	}
        if(m_hasChanged){
            handle_send_data();
            try{
            		m_pSocket->send_to(boost::asio::buffer((char*) m_pData, this->m_sizeBuffer), *m_pEndpoint);
            } catch(boost::system::system_error const& e){
                std::cerr<<e.what()<<"Connection lost for SynchObject (sender). Exiting now..."<<std::endl;
                this->m_stop = true;
            } catch(...){
            	std::cout<< "other error"<<std::endl;
            	this->m_stop = true;
            }
            return true;
        }
        return false;

    }

    template<typename... Dummy,typename tmp = protocol_type> // explanation for enable if: see init(...)-function
	typename std::enable_if<std::is_same<tmp, tcp_protocol>::value, bool>::type
        send()
        {
    	static_assert(sizeof...(Dummy)==0, "Do not specify template arguments!");
        	if(!m_initialised){
        		std::cerr<<"In function: " <<__PRETTY_FUNCTION__<<"SynchObject not initialised"<<std::endl;
        		return false;
        	}
            if(m_hasChanged){
                handle_send_data();
                try{
					std::lock_guard<std::mutex> lock(m_socketListMutex);
					int n = 0;
					for(auto it = m_socketList.begin(); it != m_socketList.end(); ++it){
						++n;
						(*it)->send(boost::asio::buffer((char*) m_pData, this->m_sizeBuffer));
						std::cout<<"sending to "<<n<<"th client"<<std::endl;
					}
                } catch(boost::system::system_error const& e){
                    std::cerr<<e.what()<<"Connection lost for SynchObject (sender). Exiting now..."<<std::endl;
                    this->m_stop = true;
                } catch(...){
                	std::cout<< "other error"<<std::endl;
                	this->m_stop = true;
                }
                return true;
            }
            return false;

        }


    data_type getData();

    /*
     * @brief get data from this SynchObject without changing the hasChanged flag (important if you still want to send it)
     * @return data
     */
    data_type getDataUnobservered();


    /**
     * @brief setSendingTimer Sets sending intervall
     * @param milliseconds[in] milliseconds Intervall in milliseconds
     */
    void setSendingTimer(long int milliseconds) {m_sendIntervall = milliseconds;}

    SynchObject<data_type, protocol_type>& operator=(const data_type& data);

    void init(const std::string &listen_address, const std::string &multicast_address, const std::string &port, bool receiver){initInternal(listen_address,multicast_address,port,receiver);} ;

    /**
     * @brief init Initialises network stuff
     * @param[in] listen_address IP address (ip-version4) of the network card that should be used (own ip address/ local device)
     * @param[in] multicast_address IP address (ip-version4) of multicast (where the data are sent to/ received from)
     * @param[in] port multicast port
     * @param[in] receiver flag if this device receives or not
     */

//   We need two specializations for replacing m_pSocket. std::enable_if allows to not test this class fully by the compiler
//	 but only compile the necessary function when needed
//	 typename... Dummy and static_assert(...) is for catching defined function-template-arguments (e.g. obj.init<int, float,...>(arg1,arg2,arg3,arg4)
//

    template<typename... Dummy,typename tmp = protocol_type>
    typename std::enable_if<std::is_same<tmp, udp_protocol>::value, void>::type
	initInternal(const std::string &listen_address, const std::string &sender_address, const std::string &port, bool receiver){
    	static_assert(sizeof...(Dummy)==0, "Do not specify template arguments!");
    	    m_stop = false;
    	    m_sizeBuffer = sizeof(data_type);
    	    m_hasChanged = false;
    	    if(receiver){

    				m_pSocket = std::make_shared<boost::asio::ip::udp::socket>(m_ioService);

    				boost::asio::ip::udp::resolver resolver(m_ioService);
    				boost::asio::ip::udp::resolver::query query(sender_address, port);

    				m_pEndpoint = std::make_shared<boost::asio::ip::udp::endpoint>(
    							 boost::asio::ip::address::from_string(listen_address),
    							 atoi(port.c_str())
    							 );

    				 m_pSocket->open(m_pEndpoint->protocol());
    				 m_pSocket->set_option(boost::asio::ip::udp::socket::reuse_address(true));
    				 m_pSocket->bind(*m_pEndpoint);
    				 // Join the multicast group.
    				 m_pSocket->set_option(boost::asio::ip::multicast::join_group(boost::asio::ip::address::from_string(sender_address).to_v4()) );
    		} else {
    	    		m_pEndpoint = std::make_shared<boost::asio::ip::udp::endpoint>(boost::asio::ip::address::from_string(sender_address), atoi(port.c_str()));
    				m_pSocket = std::make_shared<boost::asio::ip::udp::socket>(m_ioService, m_pEndpoint->protocol());

    				boost::asio::ip::address_v4 local_interface = boost::asio::ip::address_v4::from_string(listen_address);
    				boost::asio::ip::multicast::outbound_interface option(local_interface);
    				m_pSocket->set_option(option);
    	//			m_pSocket->set_option(boost::asio::ip::udp::socket::reuse_address(true));
    	//			m_pSocket->bind(*m_pEndpoint);


    	    }
    	     m_initialised = true;

    };

    template<typename... Dummy,typename tmp = protocol_type>
    typename std::enable_if<std::is_same<tmp, tcp_protocol>::value,void>::type
	initInternal(const std::string &listen_address, const std::string &sender_address, const std::string &port, bool receiver){
    	static_assert(sizeof...(Dummy)==0, "Do not specify template arguments!");
        m_stop = false;
        m_sizeBuffer = sizeof(data_type);
        m_hasChanged = false;
        if(receiver){
        	    m_pSocket = std::make_shared<boost::asio::ip::tcp::socket>(m_ioService);
        	    boost::asio::ip::tcp::resolver resolver(m_ioService);
        	    boost::asio::connect(*m_pSocket, resolver.resolve({sender_address,port}));
        }
        else {
        		m_acceptor = std::make_shared<boost::asio::ip::tcp::acceptor>(m_ioService,
        				boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), std::stoi(port)));
        		acceptIncoming();
        		m_thread = std::thread(std::bind(
        				static_cast<std::size_t (boost::asio::io_service::*)()>(&boost::asio::io_service::run),&m_ioService));

        }
         m_initialised = true;
    }

    /**
     * @brief setData Set data (to send).
     * @param data data to be set
     */
    void setData(const data_type& data);
    /**
     * @brief getData Get data (that was received).
     * @param data
     */
    void getData(data_type& data);

    void addReceiveFunction(std::function<void(std::shared_ptr<SynchObject<data_type, protocol_type> > )> fun){m_receiveFunction = fun;}



    bool hasChanged();

protected:

    void acceptIncoming();
    /**
     * @brief destroy Must be called in deconstructor of inheriting methods. Cleans up all inherited stuff...
     */
    void destroy();
    /**
     * @brief handle_receive_data Called if data is received.
     */
    void handle_receive_data();
    /**
     * @brief handle_send_data Called if data is send.
     */
     void handle_send_data();

     void setChanged();


     /**
      * @brief receiveLoop
      * @param error[in]
      * @param bytes_recvd[in]
      */
     void receiveLoop(const boost::system::error_code& error, size_t bytes_recvd);

     /**
      * @brief sendLoop
      * @param error
      */
     void sendLoop(const boost::system::error_code& error  );


     /**
      * @brief SynchObject Constructor is protected as this class must not be used directly
      */

     SynchObject();

    /**
     * @brief m_hasChanged
     */
    bool m_hasChanged;
    std::mutex m_data_lock;

    data_type m_data;







	std::shared_ptr<typename protocol_type:: endpoint> m_pEndpoint;

    std::function<void(std::shared_ptr<SynchObject<data_type, protocol_type> >)> m_receiveFunction = 0;


	//needed only for tcp
	std::shared_ptr<boost::asio::ip::tcp::acceptor> m_acceptor;
//	std::list<std::future<void> > m_futureList;
	std::list<std::shared_ptr<typename protocol_type:: socket> > m_socketList;
	std::thread m_thread;
	std::mutex m_socketListMutex;

	// needed only for udp
	std::shared_ptr<typename protocol_type:: socket> m_pSocket;


};




template<typename data_type>
using udpSynchObject = SynchObject<data_type, udp_protocol>;

template<typename data_type>
using tcpSynchObject = SynchObject<data_type, tcp_protocol>;

#include "synchObject.hpp"



}
#endif /*SYNCHOBJECT_H*/
