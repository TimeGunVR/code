#ifndef SYNCHOBJECT_H
#error "Please include synchObject.h and not synchObject.hpp!"
#else
#include <synchObject.h>

template <typename data_type, typename protocol_type>
std::shared_ptr<SynchObject<data_type, protocol_type> > SynchObject<data_type, protocol_type>::create() {
	struct make_shared_enabler : public SynchObject<data_type, protocol_type> {};
	return std::make_shared<make_shared_enabler>();
}


template <typename data_type, typename protocol_type>
SynchObject<data_type, protocol_type>::~SynchObject(){
    this->destroy();
}

template <typename data_type, typename protocol_type>
SynchObject<data_type, protocol_type>::SynchObject(){
    m_stop = false;
    m_pData = new char[sizeof(data_type)];
    m_sendIntervall = 0;
    m_hasChanged = false;


}

template <typename data_type, typename protocol_type>
SynchObject<data_type, protocol_type>& SynchObject<data_type, protocol_type>::operator=(const data_type& data){
	std::lock_guard<std::mutex> lock(m_data_lock);
    m_data = data;
    this->setChanged();
}

template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::destroy()
{
    this->stopSynching(); //just to be sure
    delete[] m_pData;

}



template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::receiveLoop(const boost::system::error_code &error, size_t bytes_recvd)
{
    if(m_stop)
        return;
    if (!error)
    {
    	if(bytes_recvd > 0){
    		handle_receive_data();
    	}
        m_pSocket->async_receive(
               boost::asio::buffer((char*) m_pData, m_sizeBuffer),// *m_pEndpoint,
               boost::bind(&SynchObject::receiveLoop, this,
               boost::asio::placeholders::error,
               boost::asio::placeholders::bytes_transferred)
        );

    } else {
        std::cout<<"error"<<std::endl;
    }
}
template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::sendLoop(const boost::system::error_code &error)
{
     if(!error && !m_stop){
         this->send();
         boost::posix_time::millisec interval((int)m_sendIntervall);
         m_deadlineTimer->expires_from_now(interval);
         m_deadlineTimer->async_wait(boost::bind(&SynchObject::sendLoop, this,_1));
     }
}


template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::setData(const data_type& data){
	std::lock_guard<std::mutex> lock(m_data_lock);
    m_data = data;
    this->setChanged();
}

template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::getData(data_type& data){
	std::lock_guard<std::mutex> lock(m_data_lock);
    data = m_data;
    m_hasChanged = false;
}

template <typename data_type, typename protocol_type>
data_type SynchObject<data_type, protocol_type>::getData(){
	std::lock_guard<std::mutex> lock(m_data_lock);
	m_hasChanged = false;
	return m_data;
}


template <typename data_type, typename protocol_type>
data_type SynchObject<data_type, protocol_type>:: getDataUnobservered(){
	std::lock_guard<std::mutex> lock(m_data_lock);
	return m_data;
}

template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::handle_receive_data(){
    m_data_lock.lock();
    memcpy(&m_data,m_pData,m_sizeBuffer);
    this->setChanged();
    m_data_lock.unlock();
    if(m_receiveFunction)
	m_receiveFunction(std::enable_shared_from_this<SynchObject<data_type, protocol_type> >::shared_from_this());
}

template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::handle_send_data(){
	std::lock_guard<std::mutex> lock(m_data_lock);
    memcpy(m_pData,&m_data,m_sizeBuffer);
}


template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::setChanged() {
    this->m_hasChanged=true;
}

template <typename data_type, typename protocol_type>
bool SynchObject<data_type, protocol_type>::hasChanged() {
	std::lock_guard<std::mutex> lock(m_data_lock);
    return this->m_hasChanged;
}

template <typename data_type, typename protocol_type>
void SynchObject<data_type, protocol_type>::acceptIncoming() {

	std::shared_ptr<boost::asio::ip::tcp::socket> socket = std::make_shared<boost::asio::ip::tcp::socket>(m_ioService);
	m_acceptor->async_accept(*socket, [socket, this](boost::system::error_code ec)
	{
		if (!ec) {
			std::lock_guard<std::mutex> lock (m_socketListMutex);
			m_socketList.push_back(socket);
//			acceptIncoming();
		}/* else {
			std::cout<<"error accepting incoming connection "<<ec<<std::endl;
			acceptIncoming();
		}*/
		acceptIncoming();
	});
}


#endif
