#include <config.h>

synchlib::caveConfig::caveConfig()
{
	std::cout << "default constructor" << std::endl;
}

synchlib::caveConfig::caveConfig(const std::string &configfile)
{
  this->loadConfig(configfile);
}

synchlib::caveConfig::~caveConfig()
{

}

bool synchlib::caveConfig::loadConfig(const std::string &filename)
{
    using boost::property_tree::ptree;
    boost::property_tree::ptree pt;

    boost::property_tree::read_xml(filename, pt);

    multicast_address = pt.get<std::string>("broadcastAddress");
    server_address = pt.get<std::string>("serverAddress");
    port = pt.get<std::string>("broadcastPort");
    boost::property_tree::ptree::const_assoc_iterator it = pt.find("stereo");
    if( it != pt.not_found() ){
    	stereo = pt.get<bool>("stereo");
    }else{
    	stereo = true;
    }


    it = pt.find("vrpnAddress");
    if( it != pt.not_found() ){
    	std::string address= pt.get<std::string>("vrpnAddress");
        it = pt.find("vrpnName");
        if( it != pt.not_found() ){
    		std::string name = pt.get<std::string>("vrpnName");
    		std::stringstream tmp;
    		tmp << name<<"@"<<address;
    		vrpn_address = tmp.str();
    	} else {
    		std::cout<<"No name (normally \"DTrack\") found. Using DTrack@192.168.0.44"<<std::endl;
    		vrpn_address = std::string("DTrack@192.168.0.44");
    	}

	} else {
		vrpn_address = std::string("DTrack@192.168.0.44");
	}

    it = pt.find("vrpnSensorIDHead");
    if( it != pt.not_found() ){
		vrpn_SensorIDHead = pt.get<int>("vrpnSensorIDHead");
	}

    it = pt.find("vrpnSensorIDWand");
    if( it != pt.not_found() ){
    	vrpn_SensorIDWand = pt.get<int>("vrpnSensorIDWand");
	}



    //get data for cave
    std::vector<bool> powerwalls;
    std::vector<std::string> walls_addresses;
    std::vector<std::string> tmp_sizes;
    std::vector<std::string> walls_geometries;


   it = pt.find("walls");
    if( it != pt.not_found() ){
    	ptree node_c = pt.get_child("walls");
    	   BOOST_FOREACH(ptree::value_type &v, pt.get_child("walls.addresses")){
    	        walls_addresses.push_back(v.second.data());
    	        powerwalls.push_back(false);
    	   }


       	it = node_c.find("geometries");
   		if( it != node_c.not_found() ){
				BOOST_FOREACH(ptree::value_type &v, pt.get_child("walls.geometries")){
				walls_geometries.push_back(v.second.data());

				}
           }


    	    BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("walls.sizes"))
    	        tmp_sizes.push_back(v.second.data());

    }



    std::vector<std::string> PipeString;
//    boost::property_tree::ptree::assoc_iterator it ;



//    ptree::assoc_iterator it;
    it = pt.find("powerwall");
    if( it != pt.not_found() )
    {
    	ptree node_pw = pt.get_child("powerwall");
        //get data for powerwall
        BOOST_FOREACH(ptree::value_type &v, pt.get_child("powerwall.addresses")){
            walls_addresses.push_back(v.second.data());
        	powerwalls.push_back(true);
        }

//        int c = pt.get_optional()count("powerwall.geometries");
//
//        	std::cout<<"geom not found"<<std::endl;
//        }else{
//
//        	std::cout<<"geom found "<<std::endl;
//        }

    	it = node_pw.find("geometries");
		if( it != node_pw.not_found() ){
			BOOST_FOREACH(ptree::value_type &v, pt.get_child("powerwall.geometries")){
				walls_geometries.push_back(v.second.data());
				std::cout<<"got geometry"<<std::endl;

			}
        }


        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("powerwall.sizes"))
            tmp_sizes.push_back(v.second.data());


        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("powerwall.displaypipes"))
            PipeString.push_back(v.second.data());

    }


    std::vector<std::string>::iterator sizeit = tmp_sizes.begin();
    std::vector<std::string>::iterator addrIt = walls_addresses.begin();
    std::vector<std::string>::iterator geomIt = walls_geometries.begin();
    std::vector<bool >::iterator pwIt = powerwalls.begin();
    std::vector<std::string>::iterator pipeIt = PipeString.begin();

    short number = 0;
    for( ;sizeit != tmp_sizes.end();++sizeit){
        if(addrIt == walls_addresses.end()){
            std::cout<<"not enough addresses given"<<std::endl;
             exit(0);
        }
//        if(geomIt == walls_geometries.end()){
//            std::cout<<"not enough geometries given"<<std::endl;
//             exit(0);
//        }
        if(pwIt == powerwalls.end()){
            std::cout<<"not enough powerwall attributes given (for walls or pws a attribute is missing)"<<std::endl;
             exit(0);
        }
        std::stringstream curAddress; curAddress << *addrIt;
        //if(*pwIt){
        //    curAddress <<"_"<<*pipeIt;
        //}else {
        //	curAddress <<"_"<<0;
        //}

        glm::dmat3x3 mat;

        std::stringstream ss(*sizeit);

        glm::dvec3 pos1;
        glm::dvec3 pos2;
        glm::dvec3 pos3;
        //retrive given positions (origin, size in x and size in y direction)
        ss >>pos1[0] >>pos1[1]>>pos1[2];
        mat[0]=pos1;
        ss >>pos2[0] >>pos2[1]>>pos2[2];
        mat[1]=pos2;
        ss >>pos3[0] >>pos3[1]>>pos3[2];
        mat[2]=pos3;

        //standard normal
        glm::dvec3 standardNormal(0.,0.,1.);
        //normal of current wall
        glm::dvec3 normalWall = glm::normalize(glm::cross((pos3-pos1),(pos2-pos1)));

        //angle between normals
        double rotAngle = glm::acos(glm::dot(normalWall,standardNormal));
        //rotation axis between normals
        glm::dvec3 rotAxis = -glm::cross(normalWall,standardNormal);

        //Trafo from normal rendering to wall rendering
        glm::dmat4 trafo;

        //rotate if necessary
        if(fabs(glm::degrees(rotAngle)) > 0.5 && fabs(glm::degrees(rotAngle)) < 179.95)
            trafo = glm::rotate(trafo,rotAngle,glm::normalize(rotAxis));
        //translate to position
        trafo[3] = glm::vec4(pos1.x,pos1.y,pos1.z,1.);// = glm::translate(trafo,-pos1);

        //translate geometry string
        glm::vec4 tmpGeometry;
        if(geomIt != walls_geometries.end()){
			std::stringstream ss2(*geomIt);
			ss2>>tmpGeometry[0]>>tmpGeometry[1]>>tmpGeometry[2]>>tmpGeometry[3];
        } else {
        	tmpGeometry[0] = 0;
        	tmpGeometry[1] = 0;
        	tmpGeometry[2] = 0;
        	tmpGeometry[3] = 0;
        }

        //and save in configuration
        m_wall_conf[curAddress.str()].wall_size = glm::mat3(mat);
        m_wall_conf[curAddress.str()].wall_trafo = glm::mat4(trafo);
        m_wall_conf[curAddress.str()].number = number;
        m_wall_conf[curAddress.str()].wall_geo = tmpGeometry;
        m_wall_conf[curAddress.str()].powerwall = *pwIt;
        if((*pwIt)){
            if(pipeIt == PipeString.end()){
                std::cerr<<"not enough pipe numbers given for powerwall"<<std::endl;
                exit(0);
            }
            m_wall_conf[curAddress.str()].pipeNumber = (unsigned short ) (atoi((*pipeIt).c_str()));
            ++pipeIt;

        }


        ++number;
        ++addrIt;
        if(geomIt != walls_geometries.end()){
        	++geomIt;
        }
        ++pwIt;
    }

    return true;

}

bool synchlib::caveConfig::saveConfig(const std::string &filename)
{
    using boost::property_tree::ptree;
    boost::property_tree::ptree pt;




//     // Put log filename in property tree
//     pt.put("debug.filename", m_file);

//     // Put debug level in property tree
//     pt.put("debug.level", m_level);

//     // Iterate over the modules in the set and put them in the
//     // property tree. Note that the put function places the new
//     // key at the end of the list of keys. This is fine most of
//     // the time. If you want to place an item at some other place
//     // (i.e. at the front or somewhere in the middle), this can
//     // be achieved using a combination of the insert and put_own
//     // functions.
//     BOOST_FOREACH(const std::string &name, m_modules)
//        pt.put("debug.modules.module", name, true);

#if BOOST_VERSION >= 105800
    boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
#else
    boost::property_tree::xml_writer_settings<char> settings('\t', 1);
#endif
     boost::property_tree::write_xml(filename, pt, std::locale(), settings);

	 return true;
}

void synchlib::caveConfig::writeDefault(const std::string &filename)
{
    using boost::property_tree::ptree;

    boost::property_tree::ptree pt;

    pt.put<std::string>("broadcastAddress", "10.156.209.255");
    pt.put<std::string>("serverAddress","10.156.209.131");
    pt.put<std::string>("broadcastPort","30001");
    pt.put<std::string>("vrpnAddress","192.168.0.44");
    pt.put<std::string>("vrpnName","DTrack");
    pt.put<int>("vrpnSensorIDHead",1);
    pt.put<int>("vrpnSensorIDWand",0);
    pt.put<bool>("stereo",true);

    std::vector<std::string> wallsAddresses;
    wallsAddresses.push_back("10.156.209.133");
    wallsAddresses.push_back("10.156.209.134");
    wallsAddresses.push_back("10.156.209.135");
    wallsAddresses.push_back("10.156.209.136");
    wallsAddresses.push_back("10.156.209.137");
    wallsAddresses.push_back("10.156.209.138");
    wallsAddresses.push_back("10.156.209.139");
    wallsAddresses.push_back("10.156.209.140");
    wallsAddresses.push_back("10.156.209.141");
    wallsAddresses.push_back("10.156.209.142");

    std::vector<std::string> tmpSizes;
    tmpSizes.push_back("-135  101.25   135     -135  270      135     -135  101.25 -135");
    tmpSizes.push_back(" -135    0      135     -135  168.75   135     -135    0    -135");
    tmpSizes.push_back("-135  101.25  -135     -135  270     -135      135  101.25 -135");
    tmpSizes.push_back("-135    0     -135     -135  168.75  -135      135    0    -135");
    tmpSizes.push_back("135  101.25  -135      135  270     -135      135  101.25  135");
    tmpSizes.push_back("135    0     -135      135  168.75  -135      135    0     135");
    tmpSizes.push_back("-135    0      33.75   -135    0     -135      135    0      33.75");
    tmpSizes.push_back("-135    0     135      -135    0      -33.75   135    0     135");
    tmpSizes.push_back("-135   270   -135      -135  270       33.75   135  270    -135");
    tmpSizes.push_back(" -135   270    -33.75   -135  270      135      135  270     -33.75");

    std::vector<std::string> tmpGeometries;
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");
    tmpGeometries.push_back("1920 1200 0 0");

     BOOST_FOREACH(const std::string &name, wallsAddresses)
        pt.add("walls.addresses.address", name);

     BOOST_FOREACH(const std::string &name, tmpSizes)
        pt.add("walls.sizes.size", name);

    BOOST_FOREACH(const std::string &name, tmpGeometries)
        pt.add("walls.geometries.cavewall", name);


    std::vector<std::string> pwAddresses;
    pwAddresses.push_back("10.156.209.130");
    pwAddresses.push_back("10.156.209.130");
    pwAddresses.push_back("10.156.209.130");
    pwAddresses.push_back("10.156.209.130");


    std::vector<std::string> pwSizes;
    pwSizes.push_back("300  107.5  -150          -300  265    -150           0  107.5 -150");
    pwSizes.push_back("0   107.5  -150          0  265    -150           300  107.5 -150");
    pwSizes.push_back("300  -50  -150          -300  107.5    -150           0  -50 -150");
    pwSizes.push_back("0   -50  -150          0  107.5    -150           300  -50 -150");


    std::vector<std::string> pwGeometries;
    pwGeometries.push_back("2048 1080 0 0");
    pwGeometries.push_back("2048 1080 0 0");
    pwGeometries.push_back("2048 1080 0 0");
    pwGeometries.push_back("2048 1080 0 0");

    std::vector<std::string> pwDispPipe;
    pwDispPipe.push_back("0");
    pwDispPipe.push_back("1");
    pwDispPipe.push_back("2");
    pwDispPipe.push_back("3");


    BOOST_FOREACH(const std::string &name, pwAddresses)
       pt.add("powerwall.addresses.address", name);

    BOOST_FOREACH(const std::string &name, pwSizes)
       pt.add("powerwall.sizes.size", name);

   BOOST_FOREACH(const std::string &name, pwGeometries)
       pt.add("powerwall.geometries.geometry", name);
   BOOST_FOREACH(const std::string &name, pwDispPipe)
       pt.add("powerwall.displaypipes.pipe", name);







#if BOOST_VERSION >= 105800
    boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
#else
    boost::property_tree::xml_writer_settings<char> settings('\t', 1);
#endif
     boost::property_tree::write_xml(filename, pt, std::locale(), settings);
}

void synchlib::caveConfig::printConfig(){
    for(std::map<std::string, synchlib::wallConf>::iterator it = m_wall_conf.begin(); it != m_wall_conf.end(); ++it){
        std::cout<<"Config for: "<<it->first<<std::endl;
        std::cout<<"pos: ";
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                std::cout<<it->second.wall_size[i][j]<<", ";
            }
            std::cout<<std::endl;
        }
        std::cout<<std::endl;
        std::cout<<"wall_trafo: ";
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                std::cout<<it->second.wall_trafo[j][i]<<", ";
            }
            std::cout<<std::endl;
        }
        std::cout<<std::endl;
    }


}

