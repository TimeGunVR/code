#include <nodeSynch.h>

using namespace synchlib;



synchlib::nodeSynch::nodeSynch(const std::string& confFile) : m_conf(confFile) {
		m_pIoService = std::make_shared<boost::asio::io_service>();
		m_pCheckIfWaitingTimer = std::make_shared<boost::asio::deadline_timer>(*m_pIoService);


}

synchlib::nodeSynch::~nodeSynch() {
//	m_pWork->reset();
	m_stop = true;
	synchFailed();
	if(m_server)
		delete[] m_receivedTokens;
	if(m_ioServiceThread.joinable())
		m_ioServiceThread.join();

}



void synchlib::nodeSynch::synchFailed() {
	std::cout<<"synchFailed"<<std::endl;
	m_stop = true;
	m_synchThreadsCondition.notify_all();
	m_pIoService->stop();
}

void synchlib::nodeSynch::sendToken(sock_ptr socket) {
}

void synchlib::nodeSynch::receiveToken(sock_ptr socket) {
}

void synchlib::nodeSynch::synchThreads(){
	m_syncThreadMutex.lock();
	++m_synchThreadsCounter;
    if(m_synchThreadsCounter  == m_synchThreads.size()){
        //all needed synchs are received, notify waiting threads
    	m_synchThreadsCounter = 0;
        {
            //needed for verifying that all threads (except this) are at m_condition.wait(*lock);
            std::lock_guard<std::mutex> lockGuard(this->m_synchThreadsMutex);
//            std::cout<<"got all threads"<<std::endl;

        }
        resetCheckIfWaiting();
        m_synchThreadsCondition.notify_all();
        m_syncThreadMutex.unlock();
    } else {
        //wait for reception of remaining syncs
        std::unique_lock<std::mutex> lock(this->m_synchThreadsMutex);
        m_syncThreadMutex.unlock();
        m_synchThreadsCondition.wait(lock);
        lock.unlock();
    }
    return;
}

bool synchlib::nodeSynch::synchToServer() {
	if(m_server){
		std::cerr<<" I am the server! I do not synchronise to myself!"<<std::endl;
		return false;
	}

	char data[1];
    try{
        boost::asio::write(*m_socket, boost::asio::buffer(data,1));
    } catch(boost::system::system_error const& e){
        std::cerr<<e.what()<<"    "<<__PRETTY_FUNCTION__<<"  sending error - stopping instance"<<std::endl;
        this->synchFailed();
        return false;
    }



	size_t reply_length = 0;
	boost::system::error_code error;
	try{
		 reply_length = m_socket->read_some(boost::asio::buffer(data),error);
	} catch(boost::system::system_error const& e){
		std::cerr<<e.what()<<"    "<<__PRETTY_FUNCTION__<<"  receiving error - stopping instance"<<std::endl;
		return false;
	}
    if (error == boost::asio::error::eof)
   	        NULL;
   	    else if(error){

   	    	std::cout<<"Synch failed at "<<m_ownIP<<std::endl;
   	    	synchFailed();
   	        return false;
   	    }
    return true;
}



void synchlib::nodeSynch::startServerSynch() {
	m_server = true;
	m_synchThreadsCounter = 0;
	int numberOfNodes = m_conf.m_wall_conf.size();
	std::map<std::string,wallConf>::iterator it = m_conf.m_wall_conf.begin();

	m_synchThreads.resize(numberOfNodes);
	std::cout<<" i want "<<numberOfNodes<<" and size: "<<m_synchThreads.size()<<std::endl;
	m_receivedTokens = new std::atomic_bool[numberOfNodes];
	m_nodehostnames.resize(numberOfNodes);
	for(unsigned short i = 0; i < numberOfNodes; ++i){
		m_synchThreads[i] = std::thread(std::bind(&nodeSynch::nodeReceiver,this, it->second.number));
		m_nodehostnames[it->second.number] = it->first;
		++it;
	}

	m_checkIfWaitingMutex.lock();
    boost::posix_time::milliseconds interval(5000);
    m_pCheckIfWaitingTimer->expires_from_now(interval);
    m_pCheckIfWaitingTimer->async_wait(boost::bind( &nodeSynch::checkIfWaiting, this, _1));
    m_checkIfWaitingMutex.unlock();

	m_ioServiceThread = std::thread(std::bind(&nodeSynch::runIoService,this));

//	//wait for first sync
//	bool notAllSynched = true;
//	do{
//		for(size_t i = 0; i < m_synchThreads.size(); ++i){
//			notAllSynched &= m_receivedTokens[i];
//		}
//
//	}while(!notAllSynched);


//	m_checkIfWaitingThread = std::thread(std::bind(&nodeSynch::checkIfWaiting,this));
	return;

}

bool synchlib::nodeSynch::startNodeSynch(const std::string ownIP) {
	m_server = false;
	m_ownIP = ownIP;
	char data[1];

	boost::asio::ip::tcp::resolver resolver(*m_pIoService);
	int port = (atoi(m_conf.port.c_str()) + ((int)m_conf.m_wall_conf[ownIP].number));
	boost::asio::ip::tcp::resolver::query query =  boost::asio::ip::tcp::resolver::query( boost::asio::ip::tcp::v4(), m_conf.server_address.c_str(), std::to_string(port));
	boost::asio::ip::tcp::resolver::iterator iterator = boost::asio::ip::tcp::resolver::iterator();
	iterator = resolver.resolve(query);


	m_socket = std::make_shared<boost::asio::ip::tcp::socket>(*m_pIoService);
	try{
		boost::asio::connect(*m_socket, iterator);
	} catch(boost::system::system_error const& e){
		std::cerr<<"Could not connect to server at port "<<port<<std::endl;
		return false;
	}

	boost::asio::socket_base::keep_alive option(true);
	try{
		m_socket->set_option(option);
	} catch(boost::system::system_error const& e){
		std::cerr<<"Cannot set option keep_alive"<<std::endl;
		return false;
	}

	try{
		boost::asio::write(*m_socket, boost::asio::buffer(data, 1));
	} catch(boost::system::system_error const& e){
		std::cerr<<"Connection lost. Cannot send sync request: |"<<ownIP<<"|"<<std::endl;
		return false;
	}


	boost::system::error_code error;
	size_t reply_length = 0;
	try{
		 reply_length = m_socket->read_some(boost::asio::buffer(data),error);
	} catch(boost::system::system_error const& e){
		std::cerr<<e.what()<<" \nFrame sync connection lost. Cannot read sync signal. "<<ownIP<<std::endl;
		return false;
	}

	return true;
}


void synchlib::nodeSynch::nodeReceiver(unsigned short number) {
	unsigned short port = (unsigned short)atoi(m_conf.port.c_str()) + (number);
	sock_ptr ownSocket = std::make_shared<boost::asio::ip::tcp::socket>(*m_pIoService);;
	std::atomic_bool* ownToken = &(m_receivedTokens[number]);
	*ownToken = false;

	boost::asio::ip::tcp::acceptor acceptor(*m_pIoService,boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port));
    acceptor.accept(*ownSocket);
    char data[1];
    boost::system::error_code ownError;
    boost::posix_time::milliseconds interval(5000);
	while(!m_stop){
		try{
			size_t length = ownSocket->read_some(boost::asio::buffer(data), ownError);
		} catch(boost::system::system_error const& e){
            std::cerr<<e.what()<<"\nConnection lost. Cannot reply to init. Exiting server..."<<std::endl;
            synchFailed();
		}

	    if (ownError == boost::asio::error::eof)
	        NULL;
	    else if(ownError){

	    	std::cout<<"Synch failed with "<<m_nodehostnames[number]<<std::endl;
	    	synchFailed();
	        return;
	    }
	    *ownToken = true;
	    //Received sync token, now wait if all displays sent one
	    this->synchThreads();
	    *ownToken = false;

        try{
            boost::asio::write(*ownSocket, boost::asio::buffer(data, 1));
        } catch(boost::system::system_error const& e){
            std::cerr<<e.what()<<"\nConnection lost. Cannot reply to init. Exitting server..."<<std::endl;
            synchFailed();
        }
        resetCheckIfWaiting();
	}
}

void synchlib::nodeSynch::checkIfWaiting(const boost::system::error_code& e) {
	m_checkIfWaitingMutex.lock();
    if (e != boost::asio::error::operation_aborted) // here is the important part
	{

    	boost::posix_time::milliseconds interval(3000);
    	m_pCheckIfWaitingTimer->expires_from_now(interval);
    	m_pCheckIfWaitingTimer->async_wait(boost::bind( &nodeSynch::checkIfWaiting, this, _1));
    	m_checkIfWaitingMutex.unlock();
		bool found = false;
		std::stringstream tmp;
		for(size_t i = 0; i < m_synchThreads.size(); ++i){
			if(!m_receivedTokens[i]){
				if(!found){
					found = true;
					tmp<<"Waiting for: \n";
				}
				tmp<<m_nodehostnames[i]<<"\n";
			}
		}
		if(found){
			++m_lastWrittenCount;
			tmp << std::string(m_lastWrittenCount, '.');
			if(m_lastWrittenCount > 10){
				m_lastWrittenCount = 0;
			}
//			std::cout<<"\r"<<std::string(m_lastWritten.size() + 10, ' ')<<std::flush;
//			std::cout<<"\033c"<<std::endl;
			std::cout<<tmp.str()<<std::flush;
//			m_lastWritten.clear();
//			m_lastWritten = tmp.str();
		} else {
			std::cout<<std::endl;
		}
		return;
	}
    m_checkIfWaitingMutex.unlock();
}

void synchlib::nodeSynch::resetCheckIfWaiting() {
	m_checkIfWaitingMutex.lock();
    boost::posix_time::milliseconds interval(3000);
    m_pCheckIfWaitingTimer->expires_from_now(interval);
    m_pCheckIfWaitingTimer->async_wait(boost::bind( &nodeSynch::checkIfWaiting, this,_1));
    m_checkIfWaitingMutex.unlock();
}

void synchlib::nodeSynch::runIoService() {
//	m_pWork = std::make_shared<boost::asio::io_service::work >(*m_pIoService);
	m_pIoService->run();
	std::cout<<"run done"<<std::endl;
	m_stop = true;
}
