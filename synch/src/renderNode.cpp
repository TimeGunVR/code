#include <renderNode.h>



synchlib::renderNode::renderNode(const std::string confFile, int argc, char** argv, bool debug) : m_conf(confFile), m_debug(debug), m_nodeSynch(confFile)
{

	//char buffer[MAX_COMPUTERNAME_LENGTH + 1];
	//DWORD len = MAX_COMPUTERNAME_LENGTH + 1;
	//if (GetComputerName(buffer, &len))
	//{
	//	std::cout << std::string(buffer, len) << std::endl;
	//}

	if(argc < 3){
		std::cout << "Not enough arguments! Start with " << argv[0] << " <own hostname/IP> <path to configfile>" << std::endl;// <Display pipe>"<<std::endl;
		m_stop = true;
		return;
	}
    m_stop = false;

    m_currentPort = atoi(m_conf.port.c_str())+m_conf.m_wall_conf.size()+1;
    std::stringstream ss; ss<<argv[1]<</*"_"<<argv[3]<<*/"\0";
    m_ownIp = ss.str();
    if(m_debug)
    	std::cout<<"own Ip:"<<m_ownIp<<std::endl;

    m_powerwall = m_conf.m_wall_conf[m_ownIp].powerwall;
//    std::cout<<"trafo: "<<m_conf.m_wall_conf[m_ownIp].wall_trafo<<std::endl;


    std::string ip2 = "0.0.0.0";


    m_pAnalogTrackingSyncher = SynchObject<glm::mat4x4, boost::asio::ip::udp>::create();
    m_pAnalogTrackingSyncher->init(ip2,m_conf.multicast_address,std::to_string(m_currentPort), true);
    ++m_currentPort;
    m_pHeadTrackingSyncher = SynchObject<glm::mat4x4, boost::asio::ip::udp>::create();
    m_pHeadTrackingSyncher->init(ip2,m_conf.multicast_address,std::to_string(m_currentPort), true);
    ++m_currentPort;
    m_pWandTrackingSyncher = SynchObject<glm::mat4x4, boost::asio::ip::udp>::create();
    m_pWandTrackingSyncher->init(ip2,m_conf.multicast_address,std::to_string(m_currentPort), true);
    ++m_currentPort;


    glm::mat4 identMat;
    glm::mat4 startPosMat = glm::translate(glm::vec3(0.,150.,0.f));
    m_pAnalogTrackingSyncher->setData(identMat);

    m_pHeadTrackingSyncher->setData(startPosMat);

    m_pWandTrackingSyncher->setData(identMat);


    glm::vec4 wallSize;

    wallSize[1] = glm::length(m_conf.m_wall_conf[m_ownIp].wall_size[0] -  m_conf.m_wall_conf[m_ownIp].wall_size[1]); //as config prefers as first corner the corner in y-direction
    wallSize[0] = glm::length(m_conf.m_wall_conf[m_ownIp].wall_size[0] -  m_conf.m_wall_conf[m_ownIp].wall_size[2]);

    //std::cout<<"wallsize: "<<wallSize[0]<<"    "<<wallSize[1]<<std::endl;
    m_wallLeft = -wallSize[0]/2.;
    m_wallRight = wallSize[0]/2.;
    m_wallBottom = -wallSize[1]/2.;
    m_wallTop = wallSize[1]/2.;

    //std::cout<<"wall points: "<<m_wallLeft<<"  |   "<<m_wallRight<<"  |   "<<m_wallTop<<"  |   "<<m_wallBottom<<"  |   "<<std::endl;

    wallSize[2] = 0.; wallSize[3] = 0;//(it is only a vector, no need for translation)



    m_wallTrafo = m_conf.m_wall_conf[m_ownIp].wall_trafo;
    m_wallTrafo[3] = glm::vec4(m_wallTrafo[3].x/1., m_wallTrafo[3].y/1.,m_wallTrafo[3].z/1.,1.);

    wallSize = m_wallTrafo * wallSize;

    //transform trafo to middle of wall
    m_wallTrafo[3] = m_wallTrafo[3] + glm::vec4((wallSize[0]/2.), (wallSize[1]/2.),(wallSize[2]/2.),0.);

    // as we only use the inverse matrix here
    m_wallTrafo = glm::inverse(m_wallTrafo);

}

synchlib::renderNode::~renderNode(){
    this->stopSynching();
    for(std::vector<std::thread>::iterator it = m_synchThreads.begin(); it != m_synchThreads.end(); ++it){
        (it)->join();

    }
//    delete m_pIoService;
//    delete m_pResolver;
//    delete m_pQuery;
//    delete m_pIterator;
//    delete m_pSocket;

}


bool synchlib::renderNode::init(){
	m_initialised = true;
	return m_nodeSynch.startNodeSynch(m_ownIp);
}

bool synchlib::renderNode::synchFrame(){
	if(!m_initialised){
		if(m_debug)
			std::cout<<"rendernode not initialised!"<<std::endl;
		return false;
	}

	return m_nodeSynch.synchToServer();
}

void synchlib::renderNode::startSynching()
{
    m_synchThreads.push_back(std::thread( std::bind( &SuperSynchObject::startReceiving, m_pAnalogTrackingSyncher) ));
    m_synchThreads.push_back(std::thread( std::bind( &SuperSynchObject::startReceiving, m_pHeadTrackingSyncher) ));
    m_synchThreads.push_back(std::thread( std::bind( &SuperSynchObject::startReceiving, m_pWandTrackingSyncher) ));
//    m_synchThreads.push_back(new std::thread( std::bind( &SynchObject::startReceiving, m_pButtonTrackingSyncher) ));

    for(std::vector<std::shared_ptr<SuperSynchObject> >::iterator it = m_synchList.begin(); it != m_synchList.end(); ++it){
        m_synchThreads.push_back(std::thread( std::bind(&SuperSynchObject::startReceiving, *it) ));
    }

}

void synchlib::renderNode::stopSynching()
{
    m_stop = true;
//    m_pButtonTrackingSyncher->stopSynching();
    m_pAnalogTrackingSyncher->stopSynching();
    m_pHeadTrackingSyncher->stopSynching();
    m_pWandTrackingSyncher->stopSynching();
    for(std::vector<std::shared_ptr<SuperSynchObject> >::iterator it = m_synchList.begin(); it != m_synchList.end(); ++it){
        (*it)->stopSynching();
    }
}

void synchlib::renderNode::getUserTrafo(glm::mat4x4 &userTrafo){
    if(!m_powerwall)
        m_pHeadTrackingSyncher->getData(userTrafo);
    else{
        userTrafo = glm::mat4(1.);
        userTrafo[3] = glm::vec4(0.f,100.,400.,1.); // 1m hoch, 3m weg von PW;
    }
}

void synchlib::renderNode::getSceneTrafo(glm::mat4x4 &sceneTrafo)
{
    m_pAnalogTrackingSyncher->getData(sceneTrafo);
}

void synchlib::renderNode::getProjectionMatrix(glm::mat4x4 &projMatrix, const bool rightEye)
{
    glm::mat4x4 userTrafo;
    this->getUserTrafo(userTrafo); //where are the eyes
    this->convertToPMat(userTrafo,projMatrix,rightEye);


}

glm::mat4 synchlib::renderNode::getProjectionMatrix(const bool rightEye) {
	glm::mat4x4 userTrafo;
	glm::mat4x4 projMatrix;
	this->getUserTrafo(userTrafo); //where are the eyes
	this->convertToPMat(userTrafo, projMatrix, rightEye);
	return projMatrix;
}


void synchlib::renderNode::getProjectionMatrices(glm::mat4x4& projMatrixLeft, glm::mat4x4& projMatrixRight){
    glm::mat4 userTrafo;
    this->getUserTrafo(userTrafo);
    this->convertToPMat(userTrafo,projMatrixRight, true);
    this->convertToPMat(userTrafo,projMatrixLeft, false);

}

void synchlib::renderNode::getPVMatrices(glm::mat4x4 &pvMatrixLeft, glm::mat4x4 &pvMatrixRight){

    this->getProjectionMatrices(pvMatrixLeft,pvMatrixRight);
    glm::mat4x4 viewmat;
    m_pAnalogTrackingSyncher->getData(viewmat);

    pvMatrixLeft =  (pvMatrixLeft * viewmat);
    pvMatrixRight =  (pvMatrixRight * viewmat);
}




void synchlib::renderNode::getPVMatrix(glm::mat4x4 &pvMatrix, const bool rightEye)
{
    glm::mat4x4 projMatrix;
    this->getProjectionMatrix(projMatrix, rightEye);


    glm::mat4x4 viewmat;
    m_pAnalogTrackingSyncher->getData(viewmat);
    pvMatrix =  (projMatrix * viewmat);

}

//template <class T>
bool synchlib::renderNode::addSynchObject(std::shared_ptr<SuperSynchObject> obj, connectiontype connectionType, short port)
{
    std::string ip2 = "0.0.0.0";

    if(connectionType == RECEIVER){
        if(port == 0){
            port = m_currentPort;
            ++m_currentPort;
        }
        obj->init(ip2,m_conf.multicast_address,std::to_string(port), true);

    }
    else if(connectionType == SENDER){
        if(port == 0){
            port = m_currentPort;
            ++m_currentPort;
        }
        obj->init(m_ownIp, m_conf.multicast_address,std::to_string(port), true);

    }
    else
        ; // do nothing

    this->m_synchList.push_back(obj);
    return true;
}



void synchlib::renderNode::convertToPMat(const glm::mat4 userTrafo, glm::mat4& projMatrix, const bool rightEye){

    glm::dvec4 eye = glm::dvec4(userTrafo[3].x,userTrafo[3].y ,userTrafo[3].z,1.f);

    glm::dvec4 eyeSep = glm::dvec4(0.,0.,0.,0.);

    // TODO: dynamic eye position
    if(rightEye){
        eyeSep += glm::dvec4(3.0,0.0,0.0,0.0);
    } else{
        eyeSep += glm::dvec4(-3.0,0.0,0.0,0.0);
    }
    eyeSep = (userTrafo) * eyeSep;
    eye = eye + eyeSep;
    eye = m_wallTrafo * eye;


    double d_near;
    double d_far;
    if(m_powerwall){
        d_near = 9.;
        d_far = fabs(eye.z) + fabs(100000.);
    } else {
        d_near = 0.3; //5cm before eye!//eye.z;
        d_far = fabs(eye.z) + 30000.;
    }



    // change values for projection matrix. Position of eyes (+5cm) is origin from which everything else is calculated
    // m_wall* is designed to be symmetric. Origin is at first centered in the middle of the screen (x-y-plane).

    //factor is used for actual wall size
    double factor = d_near / fabs(eye.z);
	//std::cout << "factor: " << factor << "," << d_near << "," << eye.z << std::endl;
    double top = (m_wallTop- eye.y) ;
    double bottom = (m_wallBottom - eye.y);//top - 2*m_wallTop/100.);
    double right = (m_wallRight - eye.x);
    double left = (m_wallLeft - eye.x);//(right - 2*m_wallRight/100.);
	//std::cout << "top: " << top << "," << bottom << "," << right << "," << left << "," << d_near << std::endl;

    top *= factor;
    bottom*= factor;
    left *= factor;
    right *= factor;


    //adapting Trafo for something TODO find out why
    glm::dmat4 view1;
    glm::dvec3 eye3 (eye.x,eye.y,eye.z);
    glm::dvec3 view_dir = glm::dvec3(0.0, 0.0, -1.0);  //in screen CoSy
    glm::dvec3 up = glm::dvec3(0.0, 1.0, 0.0);
    glm::dvec3 n = glm::normalize(-view_dir);
    glm::dvec3 u = glm::normalize(cross(up, n));
    glm::dvec3 v = glm::normalize(cross(n, u));

    view1[3] = glm::dvec4(-glm::dot(eye3,u),-glm::dot(eye3,v),-glm::dot(eye3,n),1.);
    view1[0] = glm::dvec4(u.x,v.x,n.x,0.);
    view1[1] = glm::dvec4(u.y,v.y,n.y,0.);
    view1[2] = glm::dvec4(u.z,v.z,n.z,0.);


    projMatrix = glm::transpose(glm::mat4(
                (2.0 * d_near)/(right - left ), 0.0,                            (right + left)/(right - left),          0.0,
                 0.0,                           (2.0 * d_near)/(top - bottom),  (top + bottom)/(top - bottom),          0.0,
                 0.0,                           0.0,                            -(d_far + d_near)/(d_far - d_near),     -(2.0 * d_far * d_near)/(d_far - d_near),
                 0.0,                           0.0,                            -1.0,                                   0.0));

	//std::cout << "proj: " << std::endl;
	//for (size_t y = 0; y < 4; ++y) {
	//	for (size_t x = 0; x < 4; ++x) {
	//		std::cout << projMatrix[x][y] << ",";
	//	}
	//	std::cout << std::endl;
	//}
	//std::cout << "view1: " << std::endl;
	//for (size_t y = 0; y < 4; ++y) {
	//	for (size_t x = 0; x < 4; ++x) {
	//		std::cout << view1[x][y] << ",";
	//	}
	//	std::cout << std::endl;
	//}
	//std::cout << "wall: " << std::endl;
	//for (size_t y = 0; y < 4; ++y) {
	//	for (size_t x = 0; x < 4; ++x) {
	//		std::cout << m_wallTrafo[x][y] << ",";
	//	}
	//	std::cout << std::endl;
	//}

    projMatrix = projMatrix * glm::mat4(view1) * m_wallTrafo;
}



