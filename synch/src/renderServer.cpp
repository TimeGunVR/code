#include "renderServer.h"
#include <time.h>


using namespace synchlib;

renderServer::CallbackPointer renderServer::m_cbPtr1;
renderServer::CallbackPointer renderServer::m_cbPtr2;
renderServer::CallbackPointerAnalog renderServer::m_cbPtrAnalog;



renderServer::renderServer(const std::string& confFile, int argc, char** argv) : m_conf(confFile), m_debug(true), m_nodeSynch(confFile){

	m_configPath = confFile;
    std::string argvStr(argv[0]);
    std::stringstream tmpStream;
    if(argv[0][0] == '.'){
        char cCurrentPath[FILENAME_MAX];
        getcwd(cCurrentPath, sizeof(cCurrentPath));
         tmpStream << cCurrentPath;
         std::size_t found = argvStr.find_last_of("_");
         tmpStream << argvStr.substr(1,found-1);
    } else {
        std::size_t found = argvStr.find_last_of("_");
        tmpStream << argvStr.substr(0,found);
    }
    if(argc > 2){
    	m_additionalArguments.resize(argc-2);
    	for(int t = 2; t < argc; ++t){
    		std::cout<<"adding: "<<argv[t]<<" at "<<t<<std::endl;
    		m_additionalArguments[t-2] = argv[t];
    	}
    }

    m_exePath = tmpStream.str();



    m_lastTrackingUpdateTime = std::chrono::high_resolution_clock::now();
    m_rotSpeed 		= 0.0000005;
    m_transSpeed 	= 0.0000005;
    m_oldTimeHead = 0;

    m_currentPort = atoi(m_conf.port.c_str())+m_conf.m_wall_conf.size()+1;
    m_stop = false;

    m_pAnalogTrackingSyncher = SynchObject<glm::mat4x4,boost::asio::ip::udp>::create();
    m_pAnalogTrackingSyncher->init(m_conf.server_address, m_conf.multicast_address,std::to_string(m_currentPort), false);
    ++m_currentPort;
    m_pHeadTrackingSyncher = SynchObject<glm::mat4x4,boost::asio::ip::udp>::create();
    m_pHeadTrackingSyncher->init(m_conf.server_address, m_conf.multicast_address,std::to_string(m_currentPort), false);
    ++m_currentPort;
    m_pWandTrackingSyncher = SynchObject<glm::mat4x4,boost::asio::ip::udp>::create();
    m_pWandTrackingSyncher->init(m_conf.server_address, m_conf.multicast_address,std::to_string(m_currentPort), false);
    ++m_currentPort;

    glm::mat4 identMat, startPosMat;
    startPosMat = glm::translate(glm::vec3(0.,150.,0.f));
    m_pAnalogTrackingSyncher->setData(identMat);
    m_pAnalogTrackingSyncher->send();

    m_pHeadTrackingSyncher->setData(startPosMat);
    m_pHeadTrackingSyncher->send();

    m_pWandTrackingSyncher->setData(identMat);
    m_pWandTrackingSyncher->send();

    m_cbPtr1 = &renderServer::callback_Head_tracker;
    m_cbPtr2 = &renderServer::callback_Wand_tracker;
    m_cbPtrAnalog = &renderServer::callback_analog_tracker;


}


renderServer::~renderServer()
{

    this->stopSynching();

    for(std::vector<std::thread>::iterator it = m_synchThreads.begin(); it != m_synchThreads.end(); ++it){
        (it)->join();
    }


}


bool renderServer::init(bool startNodes)
{
	m_nodeSynch.startServerSynch();
#ifndef WIN32
    if(startNodes)
    	this->startNodes();
#endif
    return true;

}



void renderServer::startSynching()
{
	   try
	    {

	        const char* const vrpn_name = m_conf.vrpn_address.c_str();//"DTrack@192.168.0.44";
	        m_pTracker = std::make_shared<vrpn_Tracker_Remote>(vrpn_name);
	        m_pTracker->shutup = true;

	        m_pTracker->register_change_handler(this, callback_receiver1, m_conf.vrpn_SensorIDHead);
	        m_pTracker->register_change_handler(this, callback_receiver2, m_conf.vrpn_SensorIDWand);
	        m_pAnalogTracker = std::make_shared<vrpn_Analog_Remote>(vrpn_name);
	        m_pAnalogTracker->shutup = false;
	        m_pAnalogTracker->register_change_handler(this, callback_analog);
	        m_pButtonTracker = std::make_shared<vrpn_Button_Remote>(vrpn_name);
	        m_pButtonTracker->register_change_handler(this, callback_button);

			std::this_thread::sleep_for(std::chrono::milliseconds(100));
	    }
	    catch(const std::exception& e)
	    {
	        std::cout << "ERROR: " << e.what() << '\n';
	        return;
	    }
	    catch(...){
	        std::cout<<"other error #############################################################"<<std::endl;
	        exit(0);
	    }


	    m_pTrackingTimer = std::make_shared<boost::asio::deadline_timer>(m_trackingIoService);
	    m_pSceneTransformTimer = std::make_shared<boost::asio::deadline_timer>(m_trackingIoService);
    //start vrpn synch
    std::cout<<"start synching"<<std::endl;
    m_synchThreads.push_back(std::thread( boost::bind( &renderServer::startTrackerUpdate, this) ));
    //start custom objects synch
    for(std::vector<std::shared_ptr<SuperSynchObject> >::iterator it = m_synchList.begin(); it != m_synchList.end(); ++it){
        m_synchThreads.push_back(std::thread( std::bind(&SuperSynchObject::startSending, *it) ));

    }
    m_isSynching = true;


}

void renderServer::stopSynching()
{

    m_stop = true;
    m_trackingIoService.stop();
    m_pAnalogTrackingSyncher->stopSynching();
    m_pHeadTrackingSyncher->stopSynching();
    m_pWandTrackingSyncher->stopSynching();

    for(std::vector<std::shared_ptr<SuperSynchObject> >::iterator it = m_synchList.begin(); it != m_synchList.end(); ++it){
        (*it)->stopSynching();
    }
    m_isSynching = false;
}


bool renderServer::addSynchObject(std::shared_ptr<SuperSynchObject> obj, renderServer::connectiontype connectionType, long int milliseconds, short port)
{
	if(m_isSynching)
		return false;

    m_synchList.push_back(obj);
    if(connectionType == SENDER){
        if(port == 0){
            port = m_currentPort;
            ++m_currentPort;
        }
        std::cout<<"add synch object"<<std::endl;
        obj->init(m_conf.server_address, m_conf.multicast_address,std::to_string(port), false);
        obj->setSendingTimer(milliseconds);
        return true;
    }
    else if (connectionType == RECEIVER){
        std::cerr<<"receiver in server not supported yet"<<std::endl;
        m_synchList.pop_back();
        return false;
    }
    return false;



}

bool renderServer::getButtonQueue(std::list<glm::ivec2> &queue){
    std::lock_guard<std::mutex> lock(m_useLock);
    if(!m_stop){
		for(std::list<glm::ivec2>::iterator it = m_buttonQueue.begin(); it != m_buttonQueue.end(); ++it){
			queue.push_back(*it);
		}
		this->m_buttonQueue.clear();
		return true;
    }
    return false;
}


void renderServer::startTrackerUpdate(){
      boost::posix_time::milliseconds interval(1);
        m_pTrackingTimer->expires_from_now(interval);
        m_lastTrackingUpdateTime = std::chrono::high_resolution_clock::now();
        m_pTrackingTimer->async_wait(boost::bind( &renderServer::TrackerUpdate, this,_1));

        m_pSceneTransformTimer->expires_from_now(interval);
        m_lastSceneTransformUpdateTime = std::chrono::high_resolution_clock::now();
        m_pSceneTransformTimer->async_wait(boost::bind( &renderServer::SceneTransformUpdate, this,_1));

        //block so that this thread doesnt die
        m_trackingIoService.run();

}


void renderServer::startNodes(){
//    start nodes
    for(std::map<std::string, synchlib::wallConf>::iterator it = m_conf.m_wall_conf.begin(); it != m_conf.m_wall_conf.end(); ++it){
        std::stringstream command;
        command << "/sw/config/mlib/startXterm "<<it->first.substr(0,it->first.find_last_of("_"))<<" "<<m_exePath.c_str()<<" "<<m_configPath;
        if(it->second.powerwall){
            command<<" "<<it->second.pipeNumber;
        }else{
        	command<<" "<<0;
        }
        for(size_t t = 0; t < m_additionalArguments.size(); ++t){
        	command<<" "<<m_additionalArguments[t];
        }
        command<<" & ";


        std::cout<<"command: "<<command.str()<<std::endl;
        system(command.str().c_str()); //per hand starten mit MALLOC_CHECK_=3 um fehler zu finden
    }
}


void renderServer::TrackerUpdate( const boost::system::error_code& error  )
{

    if(!error && !m_stop){
        m_pTracker->mainloop();
        m_pAnalogTracker->mainloop();
        m_pButtonTracker->mainloop();
        boost::posix_time::milliseconds interval(1);
        m_pTrackingTimer->expires_from_now(interval);
        m_lastTrackingUpdateTime = std::chrono::high_resolution_clock::now();
        m_pTrackingTimer->async_wait(boost::bind( &renderServer::TrackerUpdate, this,_1));

    }


}

glm::mat4 renderServer::getWandTransform(){
	std::lock_guard<std::mutex> lock (m_useLock);
	return m_wandTransform;
}

glm::mat4 renderServer::getHeadTransform(){
	std::lock_guard<std::mutex> lock (m_useLock);
	return m_userTransform;
}

void renderServer::multiplyRightSceneTransform(const glm::mat4 mat){
	std::lock_guard<std::mutex> lock (m_useLock);
	m_preMultRightScene = mat * m_preMultRightScene;
//	m_sceneTrans = m_sceneTrans * mat;
	m_sceneTransformUpdate = true;
}

void renderServer::multiplyLeftSceneTransform(const glm::mat4 mat){
	std::lock_guard<std::mutex> lock (m_useLock);
	m_preMultLeftScene = mat * m_preMultLeftScene;
	m_sceneTransformUpdate = true;
}

void renderServer::SceneTransformUpdate(const boost::system::error_code& error){
    if(!error && !m_stop){
        boost::posix_time::milliseconds interval(1);
        m_pSceneTransformTimer->expires_from_now(interval);

        m_pSceneTransformTimer->async_wait(boost::bind( &renderServer::SceneTransformUpdate, this,_1));


    if(m_sceneTransformUpdate  || fabs(m_rotate) > 0 || fabs(m_translate) > 0){

        std::chrono::high_resolution_clock::time_point curTime = std::chrono::high_resolution_clock::now();
        long long duration = std::chrono::duration_cast<std::chrono::microseconds>(curTime - m_lastSceneTransformUpdateTime).count();

    //calculate scene rotation matrix
		if (m_useDefaultNavigation) {
			glm::mat4x4 mat;
			//rotate around y by phi
			double angle = m_rotate * m_rotSpeed * double(duration) / 2.;
			double trans = -m_translate * m_transSpeed * double(duration) / 2.;
			glm::quat q = glm::quat(1. * cos(angle), 0., 1. * sin(angle), 0.); //quat constructor =: (w,wx,wy,wz)
			glm::normalize(q);

			//calculate scene translation matrix;
			std::lock_guard<std::mutex> lock(m_useLock);
			m_sceneTrans[3] = glm::inverse(m_sceneRot) * m_wandTransform * glm::vec4(0., 0., trans, 0.)* float(duration) + m_sceneTrans[3];
			m_sceneRot = m_sceneRot * glm::mat4_cast(q);
		}
		m_pAnalogTrackingSyncher->setData(m_preMultLeftScene * m_sceneRot * m_sceneTrans * m_preMultRightScene);
		

		
//        m_sceneMatMutex.unlock();
        m_pAnalogTrackingSyncher->send();
        m_sceneTransformUpdate = false;


    }
    m_lastSceneTransformUpdateTime = std::chrono::high_resolution_clock::now();


    }
}




void VRPN_CALLBACK renderServer::callback_receiver1(void* userData, const vrpn_TRACKERCB tracker){

    renderServer* instance = (renderServer*)(userData);
    (instance->*m_cbPtr1)(userData,tracker);
}

void VRPN_CALLBACK renderServer::callback_receiver2(void* userData, const vrpn_TRACKERCB tracker){

    renderServer* instance =  (renderServer*)(userData);
    (instance->*m_cbPtr2)(userData,tracker);
}

void VRPN_CALLBACK renderServer::callback_analog(void* userData, const vrpn_ANALOGCB analog)
{

    renderServer* instance =  (renderServer*)(userData);
    (instance->*m_cbPtrAnalog)(userData,analog);
}

void VRPN_CALLBACK renderServer::callback_button(void* userData, const vrpn_BUTTONCB button){
    renderServer* instance = (renderServer*) (userData);
    instance->callback_button_tracker(userData,button);
}




void renderServer::callback_Head_tracker(void* userData, const vrpn_TRACKERCB tracker){


    glm::quat ori;
    glm::vec4 pos;
//	if(m_active){
//		glm::vec4 oriVec(tracker.quat[3], tracker.quat[0], tracker.quat[1], tracker.quat[2]);
//		long long int curTime = tracker.msg_time.tv_sec * 1000000 + tracker.msg_time.tv_usec;
//		glm::vec3 posVec3(tracker.pos[0]*100.,tracker.pos[1]*100.,tracker.pos[2]*100.);
//
//
//		glm::vec3 spt, sp2;
//		glm::vec4 sqt, sq2;
//		if(abs(curTime - m_oldTimeHead) < 40000){
//				spt = m_alpha * posVec3 + (1.f-m_alpha ) * m_spt_1;
//				sp2 = m_alpha  * spt + (1.f-m_alpha ) * m_sp2_1;
//				glm::vec3 tmp = (2.f + (m_predTimes*m_alpha )/(1.f-m_alpha )) * spt - (1.f+(m_predTimes*m_alpha )/(1.f-m_alpha )) * sp2;
//				pos = glm::vec4(tmp[0],tmp[1],tmp[2],1.);
//
//
//				m_spt_1 = spt;
//				m_sp2_1 = sp2;
//				sqt =  (m_alpha  * oriVec + (1.f-m_alpha ) * m_sqt_1);;
//				sq2 = m_alpha  * sqt + (1.f-m_alpha ) * m_sq2_1;
//				glm::vec4 tmp1 = (2.f + (m_alpha )/(1.f-m_alpha )) * sqt - (1.f+(m_alpha )/(1.f-m_alpha )) * sq2;
//	//			glm::quat tmpQuat = glm::quat(tmp1[0],tmp1[1],tmp1[2],tmp1[3]);
//				m_sqt_1 = sqt;
//				m_sq2_1 = sq2;
//				ori = glm::normalize(glm::quat(tmp1[0],tmp1[1],tmp1[2],tmp1[3]));
//
//
//
//
//			}else {
//				std::cout<<"too old "<<curTime - m_oldTimeHead<<std::endl;
//				m_spt_1 = posVec3;
//				m_sp2_1 = posVec3;
//				m_sqt_1 = oriVec;
//				m_sq2_1 = m_sqt_1;
//				glm::quat(oriVec[0],oriVec[1],oriVec[2],oriVec[3]);
//				pos = glm::vec4(posVec3[0],posVec3[1],posVec3[2],1.);
//
//			}
//		m_oldTimeHead = curTime;
//	}else {
			ori = glm::quat(tracker.quat[3], tracker.quat[0], tracker.quat[1], tracker.quat[2]);
			pos = glm::vec4 (tracker.pos[0]*100.,tracker.pos[1]*100.,tracker.pos[2]*100., 1.);
//	}

	std::lock_guard<std::mutex> lock (m_useLock);
	m_userTransform = glm::mat4_cast(ori);
	m_userTransform[3] = pos;
    m_pHeadTrackingSyncher->setData(m_userTransform );
    m_pHeadTrackingSyncher->send();

}
void renderServer::callback_Wand_tracker(void* userData, const vrpn_TRACKERCB tracker){

    glm::quat ori(tracker.quat[3], tracker.quat[0], tracker.quat[1], tracker.quat[2]);
    //convert to cm (*100.)
    glm::vec4 pos(tracker.pos[0]*100.,tracker.pos[1]*100.,tracker.pos[2]*100., 1.);

    std::lock_guard<std::mutex> lock (m_useLock);
    m_wandTransform = glm::mat4_cast(ori);
    m_wandTransform[3] = pos;
    m_pWandTrackingSyncher->setData(m_wandTransform);
    m_pWandTrackingSyncher->send();
}

void renderServer::callback_analog_tracker(void* userData, const vrpn_ANALOGCB analog){
    if(analog.num_channel >= 2){
    	std::lock_guard<std::mutex> lock (m_useLock);
        m_rotate = analog.channel[0];
        m_translate = -analog.channel[1];
    }

}


void renderServer::callback_button_tracker(void* userData, const vrpn_BUTTONCB button){
    glm::ivec2 vec;
    vec[0] = button.button;
    vec[1] = button.state;
    std::lock_guard<std::mutex> lock (m_useLock);
    this->m_buttonQueue.push_back(vec);

}

bool synchlib::renderServer::getSceneTransform(glm::mat4& sceneTrafo) {
	std::lock_guard<std::mutex> lock(m_useLock);
	if(!m_stop){
		sceneTrafo =m_preMultLeftScene * m_sceneRot * m_sceneTrans * m_preMultRightScene;
		return true;
	}
	return false;
}

glm::mat4 synchlib::renderServer::getSceneTransform(){
	glm::mat4 retMat;
	std::lock_guard<std::mutex> lock(m_useLock);
	retMat = m_preMultLeftScene * m_sceneRot * m_sceneTrans * m_preMultRightScene;
	return retMat;
}

void synchlib::renderServer::setPredTimes(float times) {
	std::lock_guard<std::mutex> lock (m_useLock);
	m_predTimes = times;
}

void synchlib::renderServer::setAlpha(float alpha) {
	std::lock_guard<std::mutex> lock (m_useLock);
	m_alpha = alpha;
}

void synchlib::renderServer::setPrediction(bool active){
	std::lock_guard<std::mutex> lock (m_useLock);
	m_active = active;
}

glm::vec2 synchlib::renderServer::getAnalogValue() {
	std::lock_guard<std::mutex> lock(m_useLock);
	glm::vec2 retVal(m_rotate,-m_translate);
	return retVal;
}
