#include <synchObject.h>

using namespace synchlib;

bool SuperSynchObject::startReceiving()
{
	if(!m_initialised){
		std::cerr<<"In function: " <<__PRETTY_FUNCTION__<<" SynchObject not initialised"<<std::endl;
		return false;
	}
	m_work = std::make_shared<boost::asio::io_service::work>(m_ioService);
	boost::system::error_code er;
	this->receiveLoop(er,0);
//    m_pSocket->async_receive(
//           boost::asio::buffer((char*)m_pData, m_sizeBuffer),// *m_pEndpoint,
//           boost::bind(&SuperSynchObject::receiveLoop, this,
//           boost::asio::placeholders::error,
//           boost::asio::placeholders::bytes_transferred)
//    );
    m_ioService.run();
    return true;
}


void SuperSynchObject::stopSynching()
{
    m_stop = true;
    m_ioService.stop();
}


bool  SuperSynchObject::startSending()
{
	if(!m_initialised){
		std::cerr<<"In function: " <<__PRETTY_FUNCTION__<<"SynchObject not initialised"<<std::endl;
		return false;
	}
    if(m_sendIntervall > 0){
        m_deadlineTimer = std::make_shared<boost::asio::deadline_timer>(m_ioService);
        boost::posix_time::millisec interval((int)m_sendIntervall);
        m_deadlineTimer->expires_from_now(interval);
        m_deadlineTimer->async_wait(boost::bind(&SuperSynchObject::sendLoop, this,_1));

    m_ioService.run();
    }
    return true;
}

synchlib::SuperSynchObject::SuperSynchObject() {
	m_sendIntervall = 0;

}

synchlib::SuperSynchObject::~SuperSynchObject() {
}

//bool synchlib::SuperSynchObject::send() {
//}
