# Rename this file to, e.g. linux_64.cmake or vs11_32.cmake, and configure your default settings below.
# In your build-tree-directory call 'cmake -C this-file-with-path path-to-source-code'.
set(USER_CMAKE_TEXT "initialized by '${CMAKE_CURRENT_LIST_FILE}'")

#################################################################################
##### 					WHAT YOU CAN EASILY CHANGE 							#####


if(WIN32)	
	set(CMAKE_GENERATOR "Visual Studio 15 2017 Win64" CACHE INTERNAL ${USER_CMAKE_TEXT})
	set(GLM_ROOT_DIR "${CMAKE_CURRENT_LIST_DIR}/../libs/glm" CACHE PATH "Installation directory of glm library")
	set(BOOST_ROOT "${CMAKE_CURRENT_LIST_DIR}/../libs/boost_1_64_0/" CACHE PATH "Installation directory of boost library")
endif()

set(VRPN_ROOT_DIR "${CMAKE_CURRENT_LIST_DIR}/../libs/vrpn/latest" CACHE PATH "Installation directory of vrpn library")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}" "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules")
set(Boost_NO_SYSTEM_PATHS ON CACHE BOOL ${USER_CMAKE_TEXT})
set(Boost_USE_STATIC_LIBS ON CACHE BOOL ${USER_CMAKE_TEXT})



set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}/../libs/synchInstalled")




